package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import java.rmi.RemoteException;

/**
 * Interface das acções do bagageiro na zona de desembarque, onde os passageiros
 * saem do avião.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Porter_DZ {

    /**
     * O bagageiro espera até que outro passageiro de outro voo o acorde.
     * 
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return takeARest(VectorClock extClk) throws RemoteException;

    /**
     * O bagageiro tenta recolher uma mala do porão do avião, caso ela exista.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return Mala recolhida ou null (caso não exista) e o relógio vectorial
     * do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return tryToCollectABag(VectorClock extClk) throws RemoteException;

}
