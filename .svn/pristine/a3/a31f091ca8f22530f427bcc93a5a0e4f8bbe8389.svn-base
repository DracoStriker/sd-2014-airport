package airportRhapsody.run;

import airportRhapsody.registry.MutexRegister;
import airportRhapsody.registry.Register;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Servidor de registos RMI.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public class RegistryServer {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    /**
     * Endereço IP do servidor de registos.
     */
    private String registryServerHostName;
    
    /**
     * Porto TCP do servidor de registos.
     */
    private int registryServerPort;
    
    /**
     * Porto TCP do servidor.
     */
    private int port;

    private RegistryServer() {
    }

    /**
     * @param args Argumentos de execução (endereço IP do servidor de registos, porto TCP do servidor de registos, porto TCP do servidor)
     */
    public static void main(String[] args) {
        RegistryServer registryServer = new RegistryServer();
        registryServer.init(args);
        registryServer.serveForever();
    }

    /**
     * Corre o servidor.
     */
    private void serveForever() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.createRegistry(registryServerPort);
            MutexRegister entry = new MutexRegister(registryServerHostName, registryServerPort);
            Register register = (Register) UnicastRemoteObject.exportObject(entry, port);
            registry.rebind("Register", register);
        } catch (RemoteException e) {
            System.err.println(e);
        }
        System.out.println("registry server is running...");
    }

    /**
     * Inicialização deste processo.
     *
     * @param args argumentos da linha de comandos.
     */
    private void init(String[] args) {
        if (args.length < 3) {
            System.out.println("java -cp Airport_Rhapsody_RMI.jar airportRhapsody.run.RegistryServer <registry server host name> <registry server port> <registry server local port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
            port = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("arguments 2 and 3 must be a integer between [4000, 65535]");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nserver is closing...");
            }
        });

    }
}
