package airportRhapsody.mutex;

import airportRhapsody.logging.DTE_Logger;
import airportRhapsody.protocol.Return;
import airportRhapsody.registry.Register;
import airportRhapsody.struct.Client;
import airportRhapsody.struct.Passenger_State;
import airportRhapsody.synchronization.VectorClock;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Terminal de embarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Departure_Terminal_Entrance implements Departure_Terminal_Entrance_Interface {

    /**
     * O repositório geral de informação.
     */
    private final DTE_Logger logger;

    /**
     * Indica se ainda há passageiros em execução.
     */
    private boolean passengerKeepAlive;

    /**
     * Indica se o bagageiro ainda está em execução.
     */
    private boolean porterKeepAlive;

    /**
     * Indica se o motorista ainda está em execução.
     */
    private boolean driverKeepAlive;

    /**
     * Proxy do servidor de rregistos.
     */
    private final Register register;

    /**
     * Relógio vectorial do servidor.
     */
    private final VectorClock clk;

    /**
     * Inicializa este monitor.
     *
     * @param logger o repositório geral de informação.
     * @param register proxy do servidor de rregistos.
     * @param nPassengers número de passageiros por voo.
     */
    public Departure_Terminal_Entrance(DTE_Logger logger, Register register, int nPassengers) {
        this.logger = logger;
        this.clk = new VectorClock(nPassengers);
        this.register = register;
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
    }

    /**
     * O passageiro embarca no avião.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * avião.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized Return prepareNextLeg(Integer id, VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        System.out.println("Passenger " + id + " boarded the plane.");
        try {
            logger.setPassengerState(
                    Passenger_State.ENTERING_THE_DEPARTURE_TERMINAL, id, clk);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        return new Return(new VectorClock(clk));
    }

    /**
     * Retira o registo deste objecto remoto do servidor de registos e termina
     * a execução do processo que instanciou este objecto.
     *
     * @param client Tipo de cliente que está a terminar o servidor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void remoteClose(Client client) throws RemoteException {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            try {
                register.unbind("DTE");
            } catch (NotBoundException ex) {
                System.err.println(ex);
            }
            UnicastRemoteObject.unexportObject(this, true);
            System.out.println("\nserver is closing...");
        }
    }

}
