package airportRhapsody.struct;

import java.io.Serializable;

/**
 * Estados possíveis do bagageiro.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public enum Porter_State implements Serializable {

    WAITING_FOR_A_PLANE_TO_LAND("WPTL"), AT_THE_PLANES_HOLD("APHL"),
    AT_THE_LUGGAGE_BELT_CONVEYOR("ALBC"), AT_THE_STOREROOM("ASTR");

    private static final long serialVersionUID = 1L;

    /**
     * String de representação deste tipo de dados enumerado.
     */
    private final String state;

    /**
     * Construtor deste tipo enumerado fornecendo a sua representação em String.
     * 
     * @param state representação em String deste tipo de dados enumerado.
     */
    private Porter_State(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return state;
    }
}
