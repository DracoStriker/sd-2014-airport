package airportRhapsody.config;

import airportRhapsody.registry.Register;
import airportRhapsody.struct.Client;
import airportRhapsody.struct.Simulation;
import java.net.InetSocketAddress;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 * Configuração para uma simulação da Rapsódia no Aeroporto.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @author Pedro Piçarra <simao.paulo@ua.pt>
 * @version 3.0
 * @since 22-04-2014
 */
public class SimulationSetup implements InitialSetup {

    private static final long serialVersionUID = 1L;

    /**
     * Lista de simulações, contendo toda a informação necessária para cada
     * simulação.
     */
    private final ArrayList<Simulation> simulations;

    /**
     * Número de voos.
     */
    private final int flights;

    /**
     * Número de passageiros por cada voo.
     */
    private final int passengersPerFlight;

    /**
     * Número de lugares do autocarro de transferência.
     */
    private final int busSeats;

    /**
     * Número máximo de peças de bagagem.
     */
    private final int luggage;

    /**
     * Condição que indica se ainda há passageiros em execução.
     */
    private boolean passengerKeepAlive;

    /**
     * Condição que indica se o bagageiro ainda está em execução.
     */
    private boolean porterKeepAlive;

    /**
     * Condição que indica se o motorista ainda está em execução.
     */
    private boolean driverKeepAlive;

    /**
     * Proxy do servidor de registos.
     */
    private final Register register;

    /**
     * Lista com pares (endereço IP Unicast, porto UDP) de um grupo
     * que quer aceder a uma região crítica segundo o algoritmo distribuído de
     * Ricart e Agrawala.
     */
    private final ArrayList<InetSocketAddress> pAddress;

    /**
     * Inicializa a mensagem recebendo todos os endereços dos monitores, as
     * listas e as variáveis de simulação necessárias a todos os monitores e
     * entidades.
     *
     * @param simulations Lista de simulações, contendo toda a informação
     * necessária para cada simulação.
     * @param flights Número de voos.
     * @param passengersPerFlight Número de passageiros por cada voo.
     * @param busSeats Número de lugares do autocarro de transferência.
     * @param luggage Número máximo de peças de bagagem.
     * @param register Proxy do servidor de registos.
     * @param pAddress
     */
    public SimulationSetup(ArrayList<Simulation> simulations, int flights,
            int passengersPerFlight, int busSeats, int luggage, Register register,
            ArrayList<InetSocketAddress> pAddress) {
        this.simulations = simulations;
        this.flights = flights;
        this.register = register;
        this.passengersPerFlight = passengersPerFlight;
        this.busSeats = busSeats;
        this.luggage = luggage;
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
        this.pAddress = pAddress;
    }

    /**
     * Obtém a lista de simulações, contendo toda a informação necessária para
     * cada simulação.
     *
     * @return Lista de simulações, contendo toda a informação necessária para
     * cada simulação.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public ArrayList<Simulation> getSimulations() throws RemoteException {
        return simulations;
    }

    /**
     * Obtém o número de voos.
     *
     * @return Número de voos.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public int getFlights() throws RemoteException {
        return flights;
    }

    /**
     * Obtém o número de passageiros por voo.
     *
     * @return Número de passageiros por voo.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public int getPassengersPerFlight() throws RemoteException {
        return passengersPerFlight;
    }

    /**
     * Obtém o número de lugares do autocarro de transferência.
     *
     * @return Número de lugares do autocarro de transferência.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public int getBusSeats() throws RemoteException {
        return busSeats;
    }

    /**
     * Obtém o número máximo de malas de cada passageiro.
     *
     * @return Número máximo de malas de cada passageiro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public int getLuggage() throws RemoteException {
        return luggage;
    }

    /**
     * Obtém uma lista com pares (endereço IP Unicast, porto UDP) de um grupo
     * que quer aceder a uma região crítica segundo o algoritmo distribuído de
     * Ricart e Agrawala.
     *
     * @return uma lista com pares (endereço IP Municast, porto UDP) de um grupo
     * que quer aceder a uma região crítica segundo o algoritmo distribuído de
     * Ricart e Agrawala.
     * @throws RemoteException quando ocorre algum erro na invocação deste
     * método remoto.
     */
    @Override
    public ArrayList<InetSocketAddress> getProcessAddress() throws RemoteException {
        return pAddress;
    }

    /**
     * Retira o registo deste objecto remoto do servidor de registos e termina a
     * execução do processo que instanciou este objecto.
     *
     * @param client Tipo de cliente que está a terminar o servidor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void remoteClose(Client client) throws RemoteException {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            try {
                register.unbind("InitialSetup");
            } catch (NotBoundException ex) {
                System.err.println(ex);
            }
            UnicastRemoteObject.unexportObject(this, true);
            System.out.println("\nserver is closing...");
        }
    }

    @Override
    public String toString() {
        return "Simulation Setup:\n"
                + "Simulations = " + simulations + "\nFlights = " + flights
                + "\nPassengers per Flight = " + passengersPerFlight
                + "\nBus Seats = " + busSeats + "\nLuggage = " + luggage;
    }
}
