package airportRhapsody.logging;

import airportRhapsody.struct.Passenger_Status;
import airportRhapsody.struct.Porter_State;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Interface de controlo dos relatórios provenientes dos vários monitores.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 2.0
 * @since 14-05-2014
 */
public interface Control_Logger extends RemotelyCloseable {

    /**
     * Começa um novo voo limpando toda a estrutura de dados.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void startFlight() throws RemoteException;

    /**
     * Grava o estado actual no ficheiro de log caso este seja diferente do
     * anterior.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema ao escrever no ficheiro.
     */
    public void save() throws RemoteException, IOException;

    /**
     * Regista o número do voo.
     *
     * @param fn número do voo.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void setFlightNumber(Integer fn) throws RemoteException;

    /**
     * Regista a quantidade de bagagem no voo.
     *
     * @param bn quantidade de bagagem de todos os passageiros.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void setLuggageOnThePlane(Integer bn) throws RemoteException;

    /**
     * O bagageiro retira uma peça de bagagem do avião.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void restoreAPieceOfLuggageFromThePlane() throws RemoteException;

    /**
     * Muda o estado do ciclo de vida do bagageiro, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do bagageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setPorterState(Porter_State stat, VectorClock extClk) throws RemoteException,
            IOException;

    /**
     * Retira toda a bagagem da correia de transporte.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void emptyConveyorBelt() throws RemoteException;

    /**
     * Retira toda a bagagem do armazém.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void emptyStoreroom() throws RemoteException;

    /**
     * Atribui uma situação a um passageiro, se está em trânsito ou este
     * aeroporto é o seu voo final.
     *
     * @param si situação do passageiro.
     * @param id número de identificação do passageiro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void setPassengerStatus(Passenger_Status si, Integer id) throws RemoteException;

    /**
     * Define a quantidade de bagagem de um passageiro no início do voo.
     *
     * @param nr quantidade de bagagem.
     * @param id número de identificação do passageiro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void setLuggageCarriedAtTheStartOfHerJourney(Integer nr, Integer id) throws RemoteException;
}
