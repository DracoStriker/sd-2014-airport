package airportRhapsody.logging;

import airportRhapsody.struct.Passenger_State;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Acções de relatório provenientes da saída do terminal de embarque.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 2.0
 * @since 17-03-2014
 */
public interface DTE_Logger extends RemotelyCloseable {

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro
     * de logging.
     */
    public void setPassengerState(Passenger_State st, Integer id, VectorClock extClk) throws RemoteException, IOException;
}
