package airportRhapsody.logging;

import airportRhapsody.struct.Passenger_State;
import airportRhapsody.struct.Porter_State;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Acções de relatório provenientes da zona de desembarque.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 2.0
 * @since 17-03-2014
 */
public interface DZ_Logger extends RemotelyCloseable {

    /**
     * O bagageiro retira uma peça de bagagem do avião.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void restoreAPieceOfLuggageFromThePlane() throws RemoteException;

    /**
     * Muda o estado do ciclo de vida do bagageiro, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do bagageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro
     * de logging.
     */
    public void setPorterState(Porter_State stat, VectorClock extClk) throws RemoteException, IOException;

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro
     * de logging.
     */
    public void setPassengerState(Passenger_State st, Integer id, VectorClock extClk) throws RemoteException, IOException;
}
