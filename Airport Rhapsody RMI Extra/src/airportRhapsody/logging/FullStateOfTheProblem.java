package airportRhapsody.logging;

import airportRhapsody.struct.Driver_State;
import airportRhapsody.struct.Passenger_State;
import airportRhapsody.struct.Passenger_Status;
import airportRhapsody.struct.Porter_State;

/**
 * Descrição do estado actual da simulação da rapsódia no aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 2.0
 * @since 16-03-2014
 */
class FullStateOfTheProblem {

    /**
     * Estado do avião.
     */
    final Plane plane;

    /**
     * Estado do bagageiro.
     */
    final Porter porter;

    /**
     * Estado do motorista.
     */
    final Driver driver;

    /**
     * Estado dos passageiros.
     */
    final Passenger[] passengers;

    /**
     * Inicializa o estado do voo com o número de passageiros e número de
     * lugares no autocarro.
     *
     * @param N número de passageiros.
     * @param T número de lugares no autocarro.
     */
    FullStateOfTheProblem(int N, int T) {
        plane = new Plane();
        porter = new Porter();
        driver = new Driver();
        driver.q = new int[N];
        driver.s = new int[T];
        passengers = new Passenger[N];
        for (int i = 0; i < N; i++) {
            passengers[i] = new Passenger();
        }
    }

    /**
     * Instancia uma nova descrição do estado actual da simulação copiada de uma
     * outra.
     *
     * @param fullStat descrição do estado actual da simulação da rapsódia no
     * aeroporto.
     */
    FullStateOfTheProblem(FullStateOfTheProblem fullStat) {
        plane = new Plane(fullStat.plane);
        porter = new Porter(fullStat.porter);
        driver = new Driver(fullStat.driver);
        passengers = new Passenger[fullStat.passengers.length];
        for (int i = 0; i < fullStat.passengers.length; i++) {
            passengers[i] = new Passenger(fullStat.passengers[i]);
        }
    }

    @Override
    public String toString() {
        String str = plane.toString() + porter.toString() + driver.toString()
                + "\n";
        for (Passenger p : passengers) {
            str += p.toString();
        }
        return str + "\n";
    }

    /**
     * Todo o estado do avião durante a simulação de um voo.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 16-03-2014
     */
    class Plane {

        /**
         * Instancia uma descrição do estado do avião sem definição de nenhum
         * dos atributos.
         */
        Plane() {
        }

        /**
         * Instancia uma descrição do estado do avião copiada de outra
         * descrição.
         *
         * @param plane descrição do estado actual do avião.
         */
        Plane(Plane plane) {
            fn = plane.fn;
            bn = plane.bn;
        }

        /**
         * Número do voo.
         */
        int fn;

        /**
         * Quantidade de bagagem na aterragem do avião.
         */
        int bn;

        @Override
        public String toString() {
            return " " + fn + "  " + bn + " ";
        }
    }

    /**
     * Todo o estado do bagageiro durante a simulação de um voo.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 16-03-2014
     */
    class Porter {

        /**
         * Instancia uma descrição do estado do bagageiro sem definição de
         * nenhum dos atributos.
         */
        Porter() {
        }

        /**
         * Instancia uma descrição do estado do bagageiro copiada de outra
         * descrição.
         *
         * @param porter descrição do estado actual do bagageiro.
         */
        Porter(Porter porter) {
            stat = porter.stat;
            cb = porter.cb;
            sr = porter.sr;
        }

        /**
         * Estado do bagageiro no seu ciclo de vida.
         */
        Porter_State stat;

        /**
         * Quantidade de bagagens na correia transportadora.
         */
        int cb;

        /**
         * Quantidade de bagagem dos passageiros em trânsito armazenada no
         * armazem.
         */
        int sr;

        @Override
        public String toString() {
            return " " + stat + "  " + cb + "  " + sr + " ";
        }
    }

    /**
     * Todo o estado do motorista durante a simulação de um voo.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 16-03-2014
     */
    class Driver {

        /**
         * Instancia uma descrição do estado do condutor sem definição de nenhum
         * dos atributos.
         */
        Driver() {
        }

        /**
         * Instancia uma descrição do estado do condutor copiada de outra
         * descrição.
         *
         * @param driver descrição do estado actual do condutor.
         */
        Driver(Driver driver) {
            stat = driver.stat;
            q = new int[driver.q.length];
            System.arraycopy(driver.q, 0, q, 0, driver.q.length);
            s = new int[driver.s.length];
            System.arraycopy(driver.s, 0, s, 0, driver.s.length);
        }

        /**
         * Estado do motorista no seu ciclo de vida.
         */
        Driver_State stat;

        /**
         * Estado da fila de espera do autocarro.
         */
        int[] q;

        /**
         * Estado dos lugares do autocarro.
         */
        int[] s;

        @Override
        public String toString() {
            String str = "   " + stat + "    ";
            for (int i : q) {
                if (i == -1) {
                    str += "-  ";
                } else {
                    str += i + "  ";
                }
            }
            for (int i : s) {
                if (i == -1) {
                    str += " - ";
                } else {
                    str += " " + i + " ";
                }
            }
            return str;
        }
    }

    /**
     * Todo o estado de um passageiro durante a simulação de um voo.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 16-03-2014
     */
    class Passenger {

        /**
         * Instancia uma descrição do estado de um passangeiro sem definição de
         * nenhum dos seus atributos.
         */
        Passenger() {
        }

        /**
         * Instancia uma descrição do estado de um passageiro copiada de outra
         * descrição.
         *
         * @param passenger descrição do estado actual do passageiro.
         */
        Passenger(Passenger passenger) {
            st = passenger.st;
            si = passenger.si;
            nr = passenger.nr;
            na = passenger.na;
        }

        /**
         * Estado do passageiro no seu ciclo de vida.
         */
        Passenger_State st;

        /**
         * Condição do passageiro, se está em trânsito ou se este aeroporto é o seu
         * destino final.
         */
        Passenger_Status si;

        /**
         * Quantidade de bagagem que o passageiro transportava ao início da
         * viagem.
         */
        int nr;

        /**
         * Quantidade de bagagem que o passageiro recolheu da correia de
         * transporte.
         */
        int na;

        @Override
        public String toString() {
            return st + " " + si + "  " + nr + "   " + na + "  ";
        }
    }
}
