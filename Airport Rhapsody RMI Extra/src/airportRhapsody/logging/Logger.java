package airportRhapsody.logging;

import airportRhapsody.registry.Register;
import airportRhapsody.struct.Client;
import airportRhapsody.struct.Driver_State;
import airportRhapsody.struct.Passenger_State;
import airportRhapsody.struct.Passenger_Status;
import airportRhapsody.struct.Porter_State;
import airportRhapsody.synchronization.VectorClock;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Monitor de reportagem dos eventos dos voos durante um dia de trabalho.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 16-03-2014
 */
public class Logger implements Logger_Interface {

    /**
     * Número total de voos.
     */
    public final int K;

    /**
     * Número total de passageiro em cada voo.
     */
    public final int N;

    /**
     * Número de lugares no autocarro de transferência.
     */
    public final int T;

    /**
     * Número máximo de malas para o passageiro.
     */
    public final int M;

    /**
     * Todo o estado de um voo.
     */
    private final FullStateOfTheProblem fullStat;

    /**
     * Impressora do ficheiro de log.
     */
    private BufferedWriter bufferWritter;

    /**
     * Texto da reportagem anterior.
     */
    private String previousLog;

    /**
     * Indica se ainda há passageiros em execução.
     */
    private boolean passengerKeepAlive;

    /**
     * Indica se o bagageiro ainda está em execução.
     */
    private boolean porterKeepAlive;

    /**
     * Indica se o motorista ainda está em execução.
     */
    private boolean driverKeepAlive;

    /**
     * Proxy do servidor de registos.
     */
    private final Register register;

    /**
     * Lista dos relatórios provenientes dos monitores.
     */
    private final List<Log> logging;

    /**
     * Inicializa o monitor de log.
     *
     * @param K número total de voos.
     * @param N número total de passageiro em cada voo.
     * @param T número de lugares no autocarro de transferência.
     * @param M número máximo de malas para o passageiro.
     * @param register
     */
    public Logger(int K, int N, int T, int M, Register register) {
        this.register = register;
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
        this.K = K;
        this.N = N;
        this.T = T;
        this.M = M;
        fullStat = new FullStateOfTheProblem(N, T);
        previousLog = "";
        logging = new ArrayList<>();
    }

    /**
     * Abre um novo ficheiro de log e caso um com o mesmo nome exista o anterior
     * é apagado.
     *
     * @param fileName nome do ficheiro de log.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a abrir o ficheiro de
     * logging.
     */
    public synchronized void open(String fileName) throws RemoteException,
            IOException {
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        } else {
            file.delete();
            file.createNewFile();
        }
        bufferWritter = new BufferedWriter(new FileWriter(file, true));
        String header = "               AIRPORT RHAPSODY - Description of the "
                + "internal state of the problem\nPLANE    PORTER              "
                + "    DRIVER\nFN BN  Stat CB SR   Stat  ";
        for (int i = 0; i < N; i++) {
            header += "Q" + i + " ";
        }
        for (int i = 0; i < T; i++) {
            header += " S" + i;
        }
        header += "\n                                                          "
                + "   PASSENGERS\n";
        for (int i = 0; i < N; i++) {
            header += "St" + i + " Si" + i + " NR" + i + " NA" + i + " ";
        }
        header += "\n";
        bufferWritter.write(header);
    }

    /**
     * Começa um novo voo limpando toda a estrutura de dados.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void startFlight() throws RemoteException {
//        fullStat.plane.fn = 0;
//        fullStat.plane.bn = 0;
        fullStat.porter.stat = Porter_State.WAITING_FOR_A_PLANE_TO_LAND;
        fullStat.porter.cb = 0;
        fullStat.porter.sr = 0;
        fullStat.driver.stat = Driver_State.PARKING_AT_THE_ARRIVAL_TERMINAL;
        for (int i = 0; i < N; i++) {
            fullStat.driver.q[i] = -1;
        }
        for (int i = 0; i < T; i++) {
            fullStat.driver.s[i] = -1;
        }
        for (int i = 0; i < N; i++) {
            fullStat.passengers[i].st = Passenger_State.AT_THE_DISEMBARKING_ZONE;
//            fullStat.passengers[i].si = Passenger_Status.IN_TRANSIT;
//            fullStat.passengers[i].nr = 0;
            fullStat.passengers[i].na = 0;
        }
    }

    /**
     * Grava o estado actual no ficheiro de log caso este seja diferente do
     * anterior.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro de
     * logging.
     */
    @Override
    public synchronized void save() throws RemoteException, IOException {
        String currentLog = fullStat.toString();
        if (currentLog.compareTo(previousLog) != 0) {
            previousLog = currentLog;
        }
    }
    
    /**
     * Grava o estado actual no ficheiro de log caso este seja diferente do
     * anterior.
     *
     * @param clk relógio vectorial do monitor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro de
     * logging.
     */
    private synchronized void save(VectorClock clk) throws RemoteException, IOException {
        String currentLog = fullStat.toString();
        if (currentLog.compareTo(previousLog) != 0) {
            logging.add(new Log(new FullStateOfTheProblem(fullStat), new VectorClock(clk)));
            previousLog = currentLog;
        }
    }

    /**
     * Fecha o ficheiro de log.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro de
     * logging.
     */
    public synchronized void close() throws RemoteException, IOException {
        airportRhapsody.util.Sort.bubble(logging);
        for (Log log : logging) {
            bufferWritter.write(log.toString());
        }
        bufferWritter.close();
    }

    /**
     * Regista o número do voo.
     *
     * @param fn número do voo.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void setFlightNumber(Integer fn) throws RemoteException {
        fullStat.plane.fn = fn;
    }

    /**
     * Regista a quantidade de bagagem no voo.
     *
     * @param bn quantidade de bagagem de todos os passageiros.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void setLuggageOnThePlane(Integer bn) throws RemoteException {
        fullStat.plane.bn = bn;
    }

    /**
     * O bagageiro retira uma peça de bagagem do avião.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void restoreAPieceOfLuggageFromThePlane() throws RemoteException {
        fullStat.plane.bn--;
    }

    /**
     * Muda o estado do ciclo de vida do bagageiro, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do bagageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro de
     * logging.
     */
    @Override
    public synchronized void setPorterState(Porter_State stat, VectorClock extClk)
            throws RemoteException, IOException {
        fullStat.porter.stat = stat;
        save(extClk);
    }

    /**
     * O bagageiro adiciona uma peça de bagagem à correia de transporte.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void addLuggageToTheConveyorBelt() throws RemoteException {
        fullStat.porter.cb++;
    }

    /**
     * O passageiro recolhe uma peça de bagagem da correia de transporte.
     *
     * @param id número de identificação do passageiro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void restoreLugageFromTheConveyorBelt(Integer id) throws RemoteException {
        fullStat.passengers[id].na++;
        fullStat.porter.cb--;
    }

    /**
     * Retira toda a bagagem da correia de transporte.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void emptyConveyorBelt() throws RemoteException {
        fullStat.porter.cb = 0;
    }

    /**
     * O bagageiro adiciona uma peça de bagagem no armazém.
     */
    @Override
    public synchronized void addLuggageToTheStoreroom() throws RemoteException {
        fullStat.porter.sr++;
    }

    /**
     * Retira toda a bagagem do armazém.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void emptyStoreroom() throws RemoteException {
        fullStat.porter.sr = 0;
    }

    /**
     * Muda o estado do ciclo de vida do motorista, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do motorista.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro de
     * logging.
     */
    @Override
    public synchronized void setDriverState(Driver_State stat, VectorClock extClk) throws RemoteException, IOException {
        fullStat.driver.stat = stat;
        save(extClk);
    }

    /**
     * Um passageiro entra para a última posição livre da fila de espera do
     * autocarro.
     *
     * @param id número de identificação do passageiro que entrou na fila de
     * espera.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void addPassengerToTheWaitingQueue(Integer id) throws RemoteException {
        for (int i = 0; i < N; i++) {
            if (fullStat.driver.q[i] == -1) {
                fullStat.driver.q[i] = id;
                break;
            }
        }
    }

    /**
     * Um passageiro embarca no autocarro.
     *
     * @param id número de identificação do passageiro que entrou no autocarro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void attributePassengerHerBusSeat(Integer id) throws RemoteException {
        for (int i = 0; i < T; i++) {
            if (fullStat.driver.s[i] == -1) {
                fullStat.driver.s[i] = id;
                break;
            }
        }
    }

    /**
     * O primeiro passageiro na fila de espera dai desta para entrar no
     * autocarro.
     *
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void removePassengerFromTheWaitingQueue() throws RemoteException {
        for (int i = 0; i < N - 1; i++) {
            fullStat.driver.q[i] = fullStat.driver.q[i + 1];
        }
        fullStat.driver.q[N - 1] = -1;
    }

    /**
     * Um passageiro sai do autocarro, libertando o seu lugar.
     *
     * @param id número de identificação do passageiro que saíu no autocarro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void revokePassengerHerBusSeat(Integer id) throws RemoteException {
        for (int i = 0; i < T; i++) {
            if (fullStat.driver.s[i] == id) {
                fullStat.driver.s[i] = -1;
                break;
            }
        }
    }

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro de
     * logging.
     */
    @Override
    public synchronized void setPassengerState(Passenger_State st, Integer id, VectorClock extClk)
            throws RemoteException, IOException {
        fullStat.passengers[id].st = st;
        save(extClk);
    }

    /**
     * Atribui uma situação a um passageiro, se está em trânsito ou este
     * aeroporto é o seu voo final.
     *
     * @param si situação do passageiro.
     * @param id número de identificação do passageiro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void setPassengerStatus(Passenger_Status si, Integer id) throws RemoteException {
        fullStat.passengers[id].si = si;
    }

    /**
     * Define a quantidade de bagagem de um passageiro no início do voo.
     *
     * @param nr quantidade de bagagem.
     * @param id número de identificação do passageiro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void setLuggageCarriedAtTheStartOfHerJourney(Integer nr, Integer id) throws RemoteException {
        fullStat.passengers[id].nr = nr;
    }

    /**
     * Retira o registo deste objecto remoto do servidor de registos e termina
     * a execução do processo que instanciou este objecto.
     *
     * @param client Tipo de cliente que está a terminar o servidor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void remoteClose(Client client) throws RemoteException {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            try {
                close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
            try {
                register.unbind("Logger");
            } catch (NotBoundException ex) {
                System.err.println(ex);
            }
            UnicastRemoteObject.unexportObject(this, true);
            System.out.println("\nserver is closing...");
        }
    }

    /**
     * Relatório proveniente de um monitor, que inclui o relógio vectorial
     * de quando foi reportado.
     * 
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 27-05-2014
     */
    private class Log implements Comparable<Log> {

        /**
         * Descrição da simulação.
         */
        private final FullStateOfTheProblem fullStat;

        /**
         * Relógio vectorial do momento em que o relatório foi feito.
         */
        private final VectorClock clk;

        /**
         * Constrói uma nova mensagem de log baseada numa descrição da
         * simulação mais o relógio vectorial de quando esta foi relatada.
         * 
         * @param fullStat descrição da simulação.
         * @param clk relógio vectorial do momento em que o relatório foi feito.
         */
        public Log(FullStateOfTheProblem fullStat, VectorClock clk) {
            this.fullStat = fullStat;
            this.clk = clk;
        }

        /**
         * Devolve a descrição da simulação.
         * 
         * @return descrição da simulação.
         */
        public FullStateOfTheProblem getFullStat() {
            return fullStat;
        }

        /**
         * Devolve o relógio vectorial do momento em que o relatório foi feito.
         * 
         * @return relógio vectorial do momento em que o relatório foi feito.
         */
        public VectorClock getClk() {
            return clk;
        }

        @Override
        public int compareTo(Log o) {
            return clk.compareTo(o.clk);
        }

        @Override
        public String toString() {
            return clk + "\n" + fullStat;
        }
    }

}
