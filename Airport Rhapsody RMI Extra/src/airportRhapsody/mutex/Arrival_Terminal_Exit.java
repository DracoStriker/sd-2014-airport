package airportRhapsody.mutex;

import airportRhapsody.logging.ATE_Logger;
import airportRhapsody.protocol.Return;
import airportRhapsody.registry.Register;
import airportRhapsody.struct.Client;
import airportRhapsody.struct.Passenger_State;
import airportRhapsody.synchronization.VectorClock;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Monitor que implementa as acções do passageiro no terminal de desembarque,
 * antes de sair do aeroporto.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Arrival_Terminal_Exit implements Arrival_Terminal_Exit_Interface {

    /**
     * O repositório geral de informação.
     */
    private final ATE_Logger logger;

    /**
     * Indica se ainda há passageiros em execução.
     */
    private boolean passengerKeepAlive;

    /**
     * Indica se o bagageiro ainda está em execução.
     */
    private boolean porterKeepAlive;

    /**
     * Indica se o motorista ainda está em execução.
     */
    private boolean driverKeepAlive;
    
    /**
     * Proxy do servidor de registos.
     */
    private final Register register;

    /**
     * Relógio vectorial do servidor.
     */
    private final VectorClock clk;
    
    /**
     * Inicializa o monitor.
     *
     * @param logger Repositório geral de informação.
     * @param register Proxy do servidor de registos.
     * @param nPassengers Número de passageiros por voo.
     */
    public Arrival_Terminal_Exit(ATE_Logger logger, Register register, int nPassengers) {
        this.logger = logger;
        this.register = register;
        this.clk = new VectorClock(nPassengers);
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
    }

    /**
     * Se o passageiro perder malas, reporta o sucedido.
     *
     * @param id Identificação do passageiro que vai reportar malas perdidas.
     * @param extClk Relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return Relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized Return reportMissingBags(Integer id, VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        try {
            logger.setPassengerState(Passenger_State.AT_THE_BAGGAGE_RECLAIM_OFFICE, id, clk);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("Passenger " + id + " is reporting missing bags. HELP!"); //apenas para debug
        return new Return(new VectorClock(clk));
    }

    /**
     * O passageiro vai para casa e abandona o aeroporto, se for o seu destino
     * final e se se encontrar na situação de possuir malas e já ter recolhido
     * todas, possuir malas e ter reclamado de malas perdidas ou não possuir
     * malas.
     *
     * @param id Identificação do passageiro a abandonar o aeroporto.
     * @param extClk Relógio de sincronização vectorial do cliente para 
     * actualizar o Relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized Return goHome(Integer id, VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        try {
            logger.setPassengerState(Passenger_State.EXITING_THE_ARRIVAL_TERMINAL, id, clk);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("Passenger " + id + " is leaving the airport and going home."); //apenas para debug
        return new Return(new VectorClock(clk));
    }

    /**
     * Retira o registo deste objecto remoto do servidor de registos e termina
     * a execução do processo que instanciou este objecto.
     *
     * @param client Tipo de cliente que está a terminar o servidor.
     * @throws java.rmi.RemoteException Quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void remoteClose(Client client) throws RemoteException {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            try {
                register.unbind("ATE");
            } catch (NotBoundException ex) {
                System.err.println(ex);
            }
            UnicastRemoteObject.unexportObject(this, true);
            System.out.println("\nserver is closing...");
        }
    }

}
