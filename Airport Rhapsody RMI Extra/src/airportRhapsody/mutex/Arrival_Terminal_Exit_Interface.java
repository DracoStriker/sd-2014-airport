package airportRhapsody.mutex;

/**
 * Interface do monitor que implementa as acções do passageiro no terminal de
 * desembarque, antes de sair do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface Arrival_Terminal_Exit_Interface extends Passenger_ATE {
}
