package airportRhapsody.mutex;

import airportRhapsody.logging.ATT_Logger;
import airportRhapsody.protocol.Return;
import airportRhapsody.registry.Register;
import airportRhapsody.struct.Client;
import airportRhapsody.struct.Driver_State;
import airportRhapsody.struct.Passenger_State;
import airportRhapsody.synchronization.VectorClock;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Zona de transferência para o terminal de desembarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 28-03-2014
 */
public class Arrival_Transfer_Terminal implements
        Arrival_Transfer_Terminal_Interface {

    /**
     * Fila de espera do autocarro.
     */
    private final LinkedList<Integer> busWaitingQueue;

    /**
     * Condição que indica que a hora de partida foi atingida.
     */
    private boolean timeToGo;

    /**
     * Condição que indica que um passageiro entrou no autocarro e o motorista
     * pode convidar o próximo a entrar.
     */
    private boolean passengerEntered;

    /**
     * Condição que indica que o próximo passageiro foi chamado pelo motorista.
     */
    private boolean passengerCalled;

    /**
     * Número de lugares do autocarro.
     */
    private final int nTotalSeats;

    /**
     * Número de passageiros em trânsito para todos os voos de hoje.
     */
    private final int nFlightsForToday;

    /**
     * Número de passageiros que entraram no autocarro.
     */
    private int nPassengersOnTheBus;

    /**
     * Gerador de tempos aleatórios.
     */
    private final Random r;

    /**
     * O repositório geral de informação.
     */
    private final ATT_Logger logger;

    /**
     * Número do voo actual.
     */
    private int nFlight;

    /**
     * Número de passageiros que estão à espera de apanhar o autocarro.
     */
    private int nPassengersWaitingForADrive;

    /**
     * Região crítica para o acesso dos métodos deste monitor.
     */
    private final ReentrantLock lock;

    /**
     * Condição de acessos para o motorista à região crítica deste monitor.
     */
    private final Condition driver;

    /**
     * Lista condições de acesso para os passageiros à região crítica deste
     * monitor.
     */
    private final HashMap<Integer, Condition> passengers;

    /**
     * Condição que indica se ainda há passageiros em execução.
     */
    private boolean passengerKeepAlive;

    /**
     * Condição que indica se o bagageiro ainda está em execução.
     */
    private boolean porterKeepAlive;

    /**
     * Condição que indica se o motorista ainda está em execução.
     */
    private boolean driverKeepAlive;

    /**
     * Proxy do servidor de registos.
     */
    private final Register register;

    /**
     * Relógio vectorial do servidor.
     */
    private final VectorClock clk;
    
    /**
     * A lista de passageiros que embarcaram no autocarro.
     */
    private ArrayList<Integer> passengersOnTheBus;

    /**
     * Inicializa este monitor passando as variáveis de simulação e referências de objectos necessárias.
     *
     * @param nTotalSeats Número de lugares do autocarro.
     * @param nFlightsForToday Número de voos hoje.
     * @param logger Repositório geral de informação.
     * @param register Proxy do servidor de registos.
     * @param nPassengers Número de passageiros por voo.
     */
    public Arrival_Transfer_Terminal(Integer nTotalSeats, Integer nFlightsForToday,
            ATT_Logger logger, Register register, int nPassengers) {
        this.nTotalSeats = nTotalSeats;
        this.nFlightsForToday = nFlightsForToday;
        this.logger = logger;
        this.register = register;
        this.clk = new VectorClock(nPassengers);
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
        busWaitingQueue = new LinkedList<>();
        timeToGo = false;
        passengerEntered = false;
        passengerCalled = false;
        r = new Random();
        nPassengersOnTheBus = 0;
        nFlight = 1;
        lock = new ReentrantLock(true);
        driver = lock.newCondition();
        passengers = new HashMap<>();
    }

    /**
     * Inicializa o estado do voo actual.
     *
     * @param nFlight número do voo actual.
     * @param nPassengersWaitingForADrive número de passageiros em trânsito do
     * voo actual.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public void startFlight(Integer nFlight,
            Integer nPassengersWaitingForADrive) throws RemoteException {
        lock.lock();
        try {
            this.nFlight = nFlight;
            this.nPassengersWaitingForADrive += nPassengersWaitingForADrive;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Os passageiros colocam-se na fila de espera do autocarro pela sua ordem
     * de chegada e caso o número de pessageiros na fila de espera chegue para
     * lotar o autocarro ou tenha chegada a hora de partida o passageiro avisa o
     * motorista que pode mandar chamar os passageiros.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return takeABus(Integer id, VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        lock.lock();
        try {
            busWaitingQueue.offer(id); // o passageiro coloca-se na primeira posição livre da fila de espera
            passengers.put(id, lock.newCondition());
            System.out.println("Passenger " + id + " arrived at the bus "
                    + "waiting queue.");
            if (nTotalSeats == busWaitingQueue.size() || timeToGo) { // caso o número de passageiros à espera seja igual ao núemro de lugares do autocarro ou está na hroa de partir
                driver.signal(); // o passageiro avisa o motorista que pode chamar os passageiros para entrarem
            }
            try {
                logger.addPassengerToTheWaitingQueue(id);
                logger.setPassengerState(
                        Passenger_State.AT_THE_ARRIVAL_TRANSFER_TERMINAL, id, clk);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            return new Return(new VectorClock(clk));
        } finally {
            lock.unlock();
        }
    }

    /**
     * Após o motorista anunciar o embarque dos passageiros, cada passageiro
     * embarca no autocarro conforme a ordem na fila de espera.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return enterTheBus(Integer id, VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        lock.lock();
        try {
            while (!(Objects.equals(busWaitingQueue.peek(), id) && passengerCalled)) { // o passageiro espera que seja a sua vez de entrar no autocarro
                try {
                    passengers.get(id).await();
                } catch (InterruptedException ex) {
                }
            }
            busWaitingQueue.remove(); // o passageiro sai da fila de espera
            passengers.remove(id);
            passengerCalled = false;
            passengerEntered = true;
            System.out.println("Passenger " + id + " entered the bus.");
            try {
                logger.removePassengerFromTheWaitingQueue();
                logger.attributePassengerHerBusSeat(id);
                logger.setPassengerState(Passenger_State.TERMINAL_TRANSFER, id, clk);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            passengersOnTheBus.add(id);
            driver.signal(); // o passageiro avisa o motorista que pode chamar o próximo passageiro
            return new Return(new VectorClock(clk));
        } finally {
            lock.unlock();
        }
    }

    /**
     * O motorista espera até que haja passageiros suficientes para encher o
     * autocarro na fila de espera ou chegue a hora de partida e há pelo menos
     * um passageiro para entrar ou se já transportou todos os passageiros em
     * trânsito e caso sim termina o seu dia de trabalho.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return se todos os passageiros em trânsito já foram transportados para o
     * terminal de embarque e o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return hasDaysWorkEnded(VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        lock.lock();
        try {
            System.out.println("Driver is ready and waiting for enough passengers to fill the bus.");
            try {
                logger.setDriverState(Driver_State.PARKING_AT_THE_ARRIVAL_TERMINAL, clk);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            boolean daysWorkEnded = false;
            while (!(nTotalSeats == busWaitingQueue.size()) && !(timeToGo && !busWaitingQueue.isEmpty()) && !(daysWorkEnded = (nFlightsForToday == nFlight) && (nPassengersWaitingForADrive == 0))) { // o motorista espera que a fila de espera encha ou que chegue a hora de partida e há pelo menos um passageiro à espera
                Timer timer = new Timer();
                DriverTimerTask task = new DriverTimerTask();
                timer.schedule(task, r.nextInt(100)); // a hora de partida é anunciada
                try {
                    driver.await();
                } catch (InterruptedException ex) {
                }
                task.cancel();
                timer.cancel();
            }
            timeToGo = false;
            if (daysWorkEnded) {
                System.out.println("Has driver work ended? Yes there are no more "
                        + "passenger to transport.");
            } else {
                System.out.println("Has driver work ended? No, this is the flight "
                        + nFlight + " and I'm still waiting for more "
                        + nPassengersWaitingForADrive + " passengers.");
            }
            return new Return(daysWorkEnded, new VectorClock(clk));
        } finally {
            lock.unlock();
        }
    }

    /**
     * O motorista anuncia que os passageiros podem entrar no autocarro
     * controlando a entrada dos passageiros um a um até que não existam mais
     * lugares livres ou enquanto ainda há passageiros em fila desde que existam
     * lugares livres.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return announcingBusBoarding(VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        lock.lock();
        try {
            System.out.println("Announcing bus boarding!");
            passengersOnTheBus = new ArrayList<>();
            while (nPassengersOnTheBus < nTotalSeats && !busWaitingQueue.isEmpty()) { // o motorista verifica se ainda há lugares livres e se ainda há passageiros à espera
                passengerCalled = true;
                passengers.get(busWaitingQueue.peek()).signal(); // o motorista chama o primeiro passageiro da fila
                while (!passengerEntered) { // e espera que entre
                    try {
                        driver.await();
                    } catch (InterruptedException ex) {
                    }
                }
                passengerEntered = false;
                nPassengersOnTheBus++;
            }
            passengerCalled = false; // o motorista para de chamar passageiros pois está na hora de partir
            System.out.println("The bus left to the Departure Transfer Terminal.");
            return new Return(new VectorClock(clk));
        } finally {
            lock.unlock();
        }
    }

    /**
     * Após os passageiros terem embarcado no autocarro, o motorista parte para
     * o terminal de embarque.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return a lista de passageiros que embarcaram no autocarro e o relógio
     * vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return goToDepartureTerminal(VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        lock.lock();
        try {
            nPassengersWaitingForADrive -= nPassengersOnTheBus;
            System.out.println("The bus is travelling to the Departure Transfer Terminal.");
            try {
                logger.setDriverState(Driver_State.DRIVING_FORWARD, clk);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            return new Return(passengersOnTheBus, new VectorClock(clk));
        } finally {
            lock.unlock();
        }
    }

    /**
     * O motorista estaciona o autocarro na zona de transferência do terminal de
     * desembarque após ter regressado do terminal de embarque.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return parkTheBus(VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        lock.lock();
        try {
            nPassengersOnTheBus = 0;
            System.out.println("The bus parked at the Arrival Transfer Terminal.");
            try {
                logger.setDriverState(Driver_State.PARKING_AT_THE_ARRIVAL_TERMINAL, clk);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            return new Return(new VectorClock(clk));
        } finally {
            lock.unlock();
        }
    }

    /**
     * Retira o registo deste objecto remoto do servidor de registos e termina
     * a execução do processo que instanciou este objecto.
     *
     * @param client Tipo de cliente que está a terminar o servidor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public void remoteClose(Client client) throws RemoteException {
        lock.lock();
        try {
            switch (client) {
                case PASSENGER:
                    passengerKeepAlive = false;
                    break;
                case PORTER:
                    porterKeepAlive = false;
                    break;
                case DRIVER:
                    driverKeepAlive = false;
                    break;
            }
            if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
                try {
                    register.unbind("ATT");
                } catch (NotBoundException ex) {
                    System.err.println(ex);
                }
                UnicastRemoteObject.unexportObject(this, true);
                System.out.println("\nserver is closing...");
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Horário de partida do autocarro.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 15-03-2014
     */
    private class DriverTimerTask extends TimerTask {

        @Override
        public void run() {
            lock.lock();
            try {
                System.out.println("The driver checked that it's time to leave!");
                timeToGo = true;
                driver.signal(); // chegou a hora de partida
            } finally {
                lock.unlock();
            }
        }
    }
}
