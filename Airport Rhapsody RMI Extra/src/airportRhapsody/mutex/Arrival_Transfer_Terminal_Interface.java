package airportRhapsody.mutex;

/**
 * Interface da Zona de transferência para o terminal de desembarque do 
 * aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface Arrival_Transfer_Terminal_Interface extends Passenger_ATT, Driver_ATT {
}
