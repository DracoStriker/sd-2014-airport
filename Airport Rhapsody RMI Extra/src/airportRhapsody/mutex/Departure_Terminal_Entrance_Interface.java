package airportRhapsody.mutex;

/**
 * Interface do Terminal de embarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface Departure_Terminal_Entrance_Interface extends Passenger_DTE {
}
