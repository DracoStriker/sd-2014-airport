package airportRhapsody.mutex;

/**
 * Interface da Zona de transferência para o terminal de embarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface Departure_Transfer_Terminal_Interface extends Passenger_DTT, Driver_DTT {
}
