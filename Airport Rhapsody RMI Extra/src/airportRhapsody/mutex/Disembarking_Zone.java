package airportRhapsody.mutex;

import airportRhapsody.logging.DZ_Logger;
import airportRhapsody.protocol.Return;
import airportRhapsody.registry.Register;
import airportRhapsody.struct.Bag;
import airportRhapsody.struct.Client;
import airportRhapsody.struct.Passenger_State;
import airportRhapsody.struct.Passenger_Status;
import airportRhapsody.struct.Porter_State;
import airportRhapsody.synchronization.VectorClock;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 * Monitor que implementa as acções do passageiro e do bagageiro na zona de
 * desembarque, onde os passageiros saem do avião.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Disembarking_Zone implements Disembarking_Zone_Interface {

    /**
     * Lista de estados internos do passageiro.
     */
    private ArrayList<Passenger_Status> passengerStatus;

    /**
     * Lista de malas no avião.
     */
    private ArrayList<Bag> planeBags;

    /**
     * Número de pessoas que já sairam do avião.
     */
    private int counter = 0;

    /**
     * Condição para acordar o bagageiro.
     */
    private boolean wakePorter = false;

    /**
     * Número total de passageiros de cada voo.
     */
    private final int N;

    /**
     * Repositório geral de informação.
     */
    private final DZ_Logger logger;

    /**
     * Indica se ainda há passageiros em execução.
     */
    private boolean passengerKeepAlive;

    /**
     * Indica se o bagageiro ainda está em execução.
     */
    private boolean porterKeepAlive;

    /**
     * Indica se o motorista ainda está em execução.
     */
    private boolean driverKeepAlive;

    /**
     * Proxy do servidor de registos.
     */
    private final Register register;

    /**
     * Relógio vectorial do servidor.
     */
    private final VectorClock clk;

    /**
     * Inicializa o monitor.
     *
     * @param N Número total de passageiros de cada voo.
     * @param logger Repositório geral de informação.
     * @param register Proxy do servidor de registos.
     */
    public Disembarking_Zone(Integer N, DZ_Logger logger, Register register) {
        this.N = N;
        this.logger = logger;
        this.register = register;
        this.clk = new VectorClock(N);
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
    }

    /**
     * Actualiza a informação das listas a utilizar durante a simulação.
     *
     * @param passengerStatus Lista de estados dos passageiros.
     * @param planeBags Lista de malas do avião.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void setLists(
            ArrayList<Passenger_Status> passengerStatus,
            ArrayList<Bag> planeBags) throws RemoteException {
        this.passengerStatus = passengerStatus;
        this.planeBags = planeBags;
    }

    /**
     * Indica qual a próxima acção do passageiro com base no seu estado interno.
     * Acordará o bagageiro caso seja o último a sair do avião.
     *
     * @param id Identificação do passageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return Estado interno do passageiro e o relógio vectorial do objecto
     * remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized Return whatShouldIDo(Integer id, VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        try {
            logger.setPassengerState(Passenger_State.AT_THE_DISEMBARKING_ZONE, id, clk); //escrever no log
        } catch (IOException ex) {
            System.err.println(ex);
        }
        counter++; //incrementa o número de passageiros que saiu do avião
        System.out.println("Passenger " + id + " left the plane."); //print apenas para debug
        if (counter == N) { //se o contador == número total de passageiros, então este é o último a sair do avião
            System.out.println("Passenger " + id + " was the last exiting the airplane. Let's go and wake the porter :D"); //print apenas para debug
            wakePorter = true; //condição que permite acordar o bagageiro sem que ele volte a esperar no ciclo
            counter = 0; //reset do contador para o próximo voo
            notifyAll(); //acorda o bagageiro
        }
        return new Return(passengerStatus.get(id), new VectorClock(clk));
    }

    /**
     * O bagageiro espera até que outro passageiro de outro voo o acorde.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized Return takeARest(VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        try {
            logger.setPorterState(Porter_State.WAITING_FOR_A_PLANE_TO_LAND, clk); //escreve no log a mudança de estado do passageiro
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("Porter is resting."); //print apenas para debug
        while (!wakePorter) { //enquanto o último passageiro não saiu do avião e não activou a condição, o bagageiro não pode prosseguir
            try {
                wait();
            } catch (InterruptedException ex) {
            }
        }
        wakePorter = false; //o bagageiro já acordou, reset da condição para o próximo voo
        System.out.println("Porter is awake!"); //print apenas para debug
        return new Return(new VectorClock(clk));
    }

    /**
     * O bagageiro tenta recolher uma mala do porão do avião, caso ela exista.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return Mala recolhida ou null (caso não exista) e o relógio vectorial
     * do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized Return tryToCollectABag(VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        if (planeBags.isEmpty()) { //se não existirem malas no avião, apenas muda de estado
            try {
                logger.setPorterState(Porter_State.AT_THE_PLANES_HOLD, clk); //escreve no log a mudança de estado do bagageiro
            } catch (IOException ex) {
                System.err.println(ex);
            }
            System.out.println("Unable to retrieve bags from plane"); //print apenas para debug
            return new Return(new VectorClock(clk));
        } else { //se existirem malas no avião, muda de estado e retira a mala
            logger.restoreAPieceOfLuggageFromThePlane(); //escreve no log o número de malas no avião
            try {
                logger.setPorterState(Porter_State.AT_THE_PLANES_HOLD, clk); //escreve no log a mudança de estado do bagageiro
            } catch (IOException ex) {
                System.err.println(ex);
            }
            return new Return(planeBags.remove(0), new VectorClock(clk));
        }
    }

    /**
     * Retira o registo deste objecto remoto do servidor de registos e termina
     * a execução do processo que instanciou este objecto.
     *
     * @param client Tipo de cliente que está a terminar o servidor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void remoteClose(Client client) throws RemoteException {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            try {
                register.unbind("DZ");
            } catch (NotBoundException ex) {
                System.err.println(ex);
            }
            UnicastRemoteObject.unexportObject(this, true);
            System.out.println("\nserver is closing...");
        }
    }
}
