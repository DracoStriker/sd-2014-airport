package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;

/**
 * Interface das acções do passageiro no terminal de desembarque, antes de sair
 * do aeroporto.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_ATE extends RemotelyCloseable {

    /**
     * Se o passageiro perder malas, reporta o sucedido.
     *
     * @param id Identificação do passageiro que vai reportar malas perdidas.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return reportMissingBags(Integer id, VectorClock extClk) throws RemoteException;

    /**
     * Se for o destino final do passageiro, o passageiro vai para casa e
     * abandona o aeroporto, se se encontrar nas seguintes situações:
     * <li>Possui malas e recolheu-as todas;
     * <li>Possui malas e reclamou de malas perdidas;
     * <li>Não possui malas.
     *
     * @param id Identificação do passageiro a abandonar o aeroporto.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return goHome(Integer id, VectorClock extClk) throws RemoteException;

}
