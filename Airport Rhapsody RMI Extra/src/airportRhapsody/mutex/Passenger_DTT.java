package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;

/**
 * Acções do passageiro na zona de transferência para o terminal de desembarque
 * do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_DTT extends RemotelyCloseable {

    /**
     * O passageiro sai do autocarro.
     *
     * @param id número de identificação do passageiro que vai sair do
     * autocarro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return leaveTheBus(Integer id, VectorClock extClk) throws RemoteException;

}
