package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.struct.Bag;
import airportRhapsody.struct.Passenger_Status;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Interface das acções do passageiro na zona de desembarque, onde o mesmo sai
 * do avião.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_DZ extends RemotelyCloseable {

    /**
     * Actualiza a informação das listas a utilizar durante a simulação.
     *
     * @param passengerStatus Lista de estados dos passageiros.
     * @param planeBags Lista de malas do avião.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void setLists(ArrayList<Passenger_Status> passengerStatus,
            ArrayList<Bag> planeBags) throws RemoteException;

    /**
     * Indica qual a próxima acção do passageiro com base no seu estado interno.
     * Acordará o bagageiro caso seja o último a sair do avião.
     *
     * @param id Identificação do passageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return Estado interno do passageiro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return whatShouldIDo(Integer id, VectorClock extClk) throws RemoteException;

}
