package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.struct.Bag;
import airportRhapsody.synchronization.VectorClock;
import java.rmi.RemoteException;

/**
 * Interface das acções do bagageiro na zona de recolha de bagagens.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Porter_LCP {

    /**
     * O bagageiro desloca a mala recolhida do porão do avião para o destino
     * apropriado conforme o destino do passageiro seja o destino final ou não.
     *
     * @param b Mala recolhida do porão do avião.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return carryItToAppropriateStore(Bag b, VectorClock extClk) throws RemoteException;

    /**
     * O bagageiro não tem mais malas para recolher e acorda todos os
     * passageiros que estão ainda à espera de alguma mala.
     * 
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return noMoreBagsToCollect(VectorClock extClk) throws RemoteException;

}
