package airportRhapsody.parser;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Interpretador para obter informações específicas a partir do ficheiro de
 * configurações no formato XML.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class XMLReader {

    /**
     * Ficheiro para leitura.
     */
    private File file;

    /**
     * Gerador de produtores de documentos.
     */
    private DocumentBuilderFactory dbf;

    /**
     * Produtor de documentos.
     */
    private DocumentBuilder db;

    /**
     * Documento XML a ser interpretado.
     */
    private Document doc;

    /**
     * Inicializa o interpretador, recebendo o nome do ficheiro XML.
     *
     * @param filename Nome do ficheiro XML.
     */
    public XMLReader(String filename) {
        this.file = new File(filename);
        dbf = DocumentBuilderFactory.newInstance();
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            throw new RuntimeException(ex);
        }
        try {
            doc = db.parse(file);
        } catch (SAXException | IOException ex) {
            throw new RuntimeException(ex);
        }
        doc.getDocumentElement().normalize();
    }

    /**
     * Obtém o número de voos a utilizar na simulação.
     *
     * @return Número de voos da simulação.
     */
    public int getFlights() {
        NodeList monitorConfigs = doc.getDocumentElement().getElementsByTagName("Simulation");
        Element monitorConfig = (Element) monitorConfigs.item(0);
        NodeList address = monitorConfig.getElementsByTagName("Flights");
        return Integer.parseInt(address.item(0).getTextContent().replaceAll(" ", "").replaceAll("\n", ""));
    }

    /**
     * Obtém o número de passageiros por voo a utilizar na simulação.
     *
     * @return Passageiros por voo da simulação.
     */
    public int getPassengersPerFlight() {
        NodeList monitorConfigs = doc.getDocumentElement().getElementsByTagName("Simulation");
        Element monitorConfig = (Element) monitorConfigs.item(0);
        NodeList address = monitorConfig.getElementsByTagName("PassengersPerFlight");
        return Integer.parseInt(address.item(0).getTextContent().replaceAll(" ", "").replaceAll("\n", ""));
    }

    /**
     * Obtém o número de lugares do autocarro de transferência a utilizar na
     * simulação.
     *
     * @return Número de lugares do autocarro da simulação.
     */
    public int getBusSeats() {
        NodeList monitorConfigs = doc.getDocumentElement().getElementsByTagName("Simulation");
        Element monitorConfig = (Element) monitorConfigs.item(0);
        NodeList address = monitorConfig.getElementsByTagName("BusSeats");
        return Integer.parseInt(address.item(0).getTextContent().replaceAll(" ", "").replaceAll("\n", ""));
    }

    /**
     * Obtém o número máximo de malas dos passageiros a utilizar na simulação.
     *
     * @return Número máximo de malas dos passageiros da simulação.
     */
    public int getBags() {
        NodeList monitorConfigs = doc.getDocumentElement().getElementsByTagName("Simulation");
        Element monitorConfig = (Element) monitorConfigs.item(0);
        NodeList address = monitorConfig.getElementsByTagName("Bags");
        return Integer.parseInt(address.item(0).getTextContent().replaceAll(" ", "").replaceAll("\n", ""));
    }

    /**
     * Obtém o endereço de um monitor específico.
     *
     * @param monitor Nome do monitor, como consta no nó do ficheiro XML.
     * @return Endereço do monitor requisitado.
     * @throws java.net.UnknownHostException Quando o endereço não pode ser
     * descodificado ou quando o host não é encontrado.
     */
    private InetAddress getAddress(String monitor) throws UnknownHostException {
        NodeList monitorConfigs = doc.getDocumentElement().getElementsByTagName(monitor);
        Element monitorConfig = (Element) monitorConfigs.item(0);
        NodeList address = monitorConfig.getElementsByTagName("Address");
        return InetAddress.getByName(address.item(0).getTextContent().replaceAll(" ", "").replaceAll("\n", ""));
    }

    /**
     * Obtém a porta de um monitor específico.
     *
     * @param monitor Nome do monitor, como consta no nó do ficheiro XML.
     * @return Porta do monitor requisitado.
     */
    private int getPort(String monitor) {
        NodeList monitorConfigs = doc.getDocumentElement().getElementsByTagName(monitor);
        Element monitorConfig = (Element) monitorConfigs.item(0);
        NodeList address = monitorConfig.getElementsByTagName("Port");
        return Integer.parseInt(address.item(0).getTextContent().replaceAll(" ", "").replaceAll("\n", ""));
    }

    /**
     * Obtém o endereço completo (endereço e porta) de um monitor específico.
     *
     * @param monitor Nome do monitor, como consta no nó do ficheiro XML.
     * @return Endereço completo do monitor requisitado.
     * @throws java.net.UnknownHostException Quando o endereço não pode ser
     * descodificado ou quando o host não é encontrado.
     */
    public InetSocketAddress getSocketAddress(String monitor) throws UnknownHostException {
        return new InetSocketAddress(getAddress(monitor), getPort(monitor));
    }
}
