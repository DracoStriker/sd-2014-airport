package airportRhapsody.protocol;

import airportRhapsody.synchronization.ExtendedTimestamp;
import java.sql.Timestamp;

/**
 * Mensagem de pedido de entrada da região crítica segundo o algoritmo de Ricart
 * e Agrawala.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 03-07-2014
 */
public class RequestAccess extends ProtocolMessage implements Comparable<RequestAccess> {
    
    private static final long serialVersionUID = 1L;

    /**
     * Marca temporal extendida.
     */
    private final ExtendedTimestamp ets;

    /**
     * Constrói uma mensagem de pedido de entrada na região crítica por parte de
     * um processo.
     *
     * @param pid o número de identificação do processo que originou esta
     * mensagem.
     */
    public RequestAccess(int pid) {
        ets = new ExtendedTimestamp(pid);
    }

    /**
     * Devolve a marca temporal do instante em que a classe é instânciada.
     *
     * @return a marca temporal do instante em que a classe é instânciada.
     */
    public Timestamp getTimestamp() {
        return ets.getTimestamp();
    }

    /**
     * Devolve o número de identificação do processo que originou esta mensagem.
     *
     * @return o número de identificação do processo que originou esta mensagem.
     */
    public int getPID() {
        return ets.getPID();
    }

    @Override
    public int compareTo(RequestAccess request) {
        return ets.compareTo(request.ets);
    }

    @Override
    public String toString() {
        return "RequestAccess { Timestamp = " + ets.getTimestamp() + ", PID = " + ets.getPID() + " }";
    }
}
