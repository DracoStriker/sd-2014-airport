package airportRhapsody.protocol;

import airportRhapsody.synchronization.VectorClock;
import java.io.Serializable;

/**
 * Retorno genérico de um objecto remoto, que contém o seu relógio vectorial.
 * 
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 * @param <T> o tipo de retorno.
 */
public class Return<T> implements Serializable {
    
    /**
     * Objecto de retorno.
     */
    private final T obj;
    
    /**
     * Relógio vectorial do objecto remoto.
     */
    private final VectorClock vc;

    /**
     * Constrói uma mensagem de retorno genérica com o relógio vectorial
     * do objecto remoto.
     * 
     * @param obj o objecto de retorno.
     * @param vc o relógio vectorial do objecto remoto.
     */
    public Return(T obj, VectorClock vc) {
        this.obj = obj;
        this.vc = vc;
    }
    
    /**
     * Constrói uma mensagem de retorno void com o relógio vectorial
     * do objecto remoto.
     * 
     * @param vc o relógio vectorial do objecto remoto.
     */
    public Return(VectorClock vc) {
        this.obj = null;
        this.vc = vc;
    }
    
    /**
     * Obter o retorno genérico.
     * 
     * @return o valor genérico, retorna null caso o tipo de retorno seja void.
     */
    public T getObj() {
        return obj;
    }

    /**
     * Obter o relógio vectorial do objecto remoto.
     * 
     * @return o relógio vectorial do objecto remoto.
     */
    public VectorClock getVc() {
        return vc;
    }
    
}
