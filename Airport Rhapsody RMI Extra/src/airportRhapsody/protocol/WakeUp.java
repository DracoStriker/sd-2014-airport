package airportRhapsody.protocol;

/**
 * Mensagem de ordem de desbloqueio por parte de um processo.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 03-07-2014
 */
public class WakeUp extends ProtocolMessage {
    
    private static final long serialVersionUID = 1L;

    /**
     * Número de identificação do processo que originou esta mensagem.
     */
    private final int pid;

    /**
     * Constrói uma mensagem de ordem de desbloqueio por parte de um processo.
     *
     * @param pid o número de identificação do processo que originou esta
     * mensagem.
     */
    public WakeUp(int pid) {
        this.pid = pid;
    }

    /**
     * Devolve o número de identificação do processo que originou esta mensagem.
     *
     * @return o número de identificação do processo que originou esta mensagem.
     */
    public int getPID() {
        return pid;
    }

    @Override
    public String toString() {
        return "WakeUp { PID = " + pid + " }";
    }
}
