package airportRhapsody.registry;

import airportRhapsody.struct.Client;
import static airportRhapsody.struct.Client.DRIVER;
import static airportRhapsody.struct.Client.PASSENGER;
import static airportRhapsody.struct.Client.PORTER;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Proxy de um servidor de registos RMI.
 * 
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public class MutexRegister implements Register {

    private static final long serialVersionUID = 1L;

    /**
     * Endereço ou URI do servidor de registos.
     */
    private String hostName;

    /**
     * Porto do servidor de registos.
     */
    private int portNumb = 1099;

    /**
     * Indica se ainda há passageiros em execução.
     */
    private boolean passengerKeepAlive;

    /**
     * Indica se o bagageiro ainda está em execução.
     */
    private boolean porterKeepAlive;

    /**
     * Indica se o motorista ainda está em execução.
     */
    private boolean driverKeepAlive;

    /**
     * Constrói um proxy de um servidor de registos.
     * 
     * @param hostName Endereço ou URI do servidor de registos.
     * @param portNumb Porto do servidor de registos.
     */
    public MutexRegister(String hostName, int portNumb) {
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
        if ((hostName == null) || ("".equals(hostName))) {
            throw new NullPointerException();
        }
        this.hostName = hostName;
        if ((portNumb >= 4000) && (portNumb <= 65535)) {
            this.portNumb = portNumb;
        }
    }

    /**
     * Regista um objecto com o nome fornecido.
     * 
     * @param name nome do registo.
     * @param obj objecto a registar.
     * @throws RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws AlreadyBoundException se o objecto com o nome definido já se 
     * encontra registado.
     */
    @Override
    public void bind(String name, Remote obj) throws RemoteException, AlreadyBoundException {
        Registry registry;
        if ((name == null) || (obj == null)) {
            throw new NullPointerException();
        }
        registry = LocateRegistry.getRegistry(hostName, portNumb);
        registry.bind(name, obj);
        System.out.println("binding object " + obj.getClass().getCanonicalName() + " as " + name);
    }

    /**
     * Retira o registo de um objecto remoto com o nome fornecido.
     * 
     * @param name nome do registo.
     * @throws RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws NotBoundException caso um objecto com o noem fornecido não
     * se encontre registado.
     */
    @Override
    public void unbind(String name) throws RemoteException, NotBoundException {
        Registry registry;
        if ((name == null)) {
            throw new NullPointerException();
        }
        registry = LocateRegistry.getRegistry(hostName, portNumb);
        registry.unbind(name);
        System.out.println("unbinding " + name);
    }

    /**
     * Regista um objecto com o nome fornecido.
     * 
     * @param name nome do registo.
     * @param obj objecto a registar.
     * @throws RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public void rebind(String name, Remote obj) throws RemoteException {
        Registry registry;
        if ((name == null) || (obj == null)) {
            throw new NullPointerException();
        }
        registry = LocateRegistry.getRegistry(hostName, portNumb);
        registry.rebind(name, obj);
        System.out.println("rebinding object " + obj.getClass().getCanonicalName() + " as " + name);
    }

    /**
     * Retira o registo deste objecto remoto do servidor de registos e termina
     * a execução do processo que instanciou este objecto, para tal é necessário
     * também destruir o servidor de registos.
     *
     * @param client Tipo de cliente que está a terminar o servidor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void remoteClose(Client client) throws RemoteException {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            final MutexRegister register = this;
            new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(1000);
                        Registry registry;
                        registry = LocateRegistry.getRegistry(hostName, portNumb);
                        try {
                            registry.unbind("Register");
                            System.out.println("unbinding Register");
                        } catch (NotBoundException | AccessException ex) {
                            System.err.println(ex);
                        }
                        UnicastRemoteObject.unexportObject(register, true);
                        UnicastRemoteObject.unexportObject(registry, true);
                    } catch (RemoteException | InterruptedException ex) {
                    }
                }
            }.start();
        }
    }
}
