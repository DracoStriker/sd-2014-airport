package airportRhapsody.run;

import airportRhapsody.config.InitialSetup;
import airportRhapsody.logging.ATE_Logger;
import airportRhapsody.mutex.Arrival_Terminal_Exit;
import airportRhapsody.mutex.Arrival_Terminal_Exit_Interface;
import airportRhapsody.registry.Register;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Processo de simulação do monitor do terminal de desembarque, antes dos passageiros sairem do aeroporto.
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 */
public class ATE_Process {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    /**
     * Endereço IP do servidor de registos.
     */
    private String registryServerHostName;

    /**
     * Porto TCP do servidor de registos.
     */
    private int registryServerPort;

    /**
     * Porto TCP do servidor.
     */
    private int port;

    private ATE_Process() {
    }

    /**
     * @param args Argumentos de execução (endereço IP do servidor de registos, porto TCP do servidor de registos, porto TCP do servidor)
     */
    public static void main(String[] args) {
        ATE_Process ateProcess = new ATE_Process();
        ateProcess.init(args);
        ateProcess.runProcess();
    }

    /**
     * Corre o servidor.
     */
    private void runProcess() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(registryServerHostName, registryServerPort);
            Register register = (Register) registry.lookup("Register");
            ATE_Logger logger = (ATE_Logger) registry.lookup("Logger");
            InitialSetup setup = (InitialSetup) registry.lookup("InitialSetup");
            Arrival_Terminal_Exit ate = new Arrival_Terminal_Exit(logger, register, setup.getPassengersPerFlight());
            Arrival_Terminal_Exit_Interface ateProxy = (Arrival_Terminal_Exit_Interface) UnicastRemoteObject.exportObject(ate, port);
            register.rebind("ATE", ateProxy);
        } catch (RemoteException | NotBoundException ex) {
            System.err.println(ex);
        }
        System.out.println("ATE Process is running...");
    }

    /**
     * Inicialização deste processo.
     *
     * @param args argumentos da linha de comandos.
     */
    private void init(String[] args) {
        if (args.length < 3) {
            System.out.println("java -cp Airport_Rhapsody_RMI.jar airportRhapsody.run.ATE_Process <registry server host name> <registry server port> <port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
            port = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("arguments 2 and 3 must be a integer between [4000, 65535]");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nserver is closing...");
            }
        });
    }

}
