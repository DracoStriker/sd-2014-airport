package airportRhapsody.struct;

import java.io.Serializable;

/**
 * Possíveis entidades cliente.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public enum Client implements Serializable {

    PASSENGER("Passenger"), PORTER("Porter"), DRIVER("Driver");
    
    private static final long serialVersionUID = 1L;

    /**
     * Nome do tipo de cliente.
     */
    private final String client;

    /**
     * Inicializa esta notação de cliente, recebendo o seu nome.
     * @param client Nome do tipo de cliente.
     */
    private Client(String client) {
        this.client = client;
    }


    @Override
    public String toString() {
        return client;
    }
    
}
