package airportRhapsody.struct;

import java.io.Serializable;

/**
 * Estados possíveis do passageiro.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public enum Passenger_State implements Serializable {

    AT_THE_DISEMBARKING_ZONE(" DZ"), AT_THE_LUGGAGE_COLLECTION_POINT("LCP"),
    AT_THE_BAGGAGE_RECLAIM_OFFICE("BRO"), EXITING_THE_ARRIVAL_TERMINAL("ATE"),
    AT_THE_ARRIVAL_TRANSFER_TERMINAL("ATT"), TERMINAL_TRANSFER(" TT"),
    AT_THE_DEPARTURE_TRANSFER_TERMINAL("DTT"),
    ENTERING_THE_DEPARTURE_TERMINAL("DTE");

    private static final long serialVersionUID = 1L;

    /**
     * String de representação deste tipo de dados enumerado.
     */
    private final String state;

    /**
     * Construtor deste tipo enuemrado fornecendo a sua representação em String.
     * 
     * @param state representação em String deste tipo de dados enumerado.
     */
    private Passenger_State(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return state;
    }
}
