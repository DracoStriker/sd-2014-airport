package airportRhapsody.synchronization;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Implementação de uma marca temporal extendida.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 02-07-2014
 */
public class ExtendedTimestamp implements Serializable, Comparable<ExtendedTimestamp> {
    
    private static final long serialVersionUID = 1L;

    /**
     * Marca temporal do instante em que a classe é instânciada.
     */
    private final Timestamp ts;
    
    /**
     * Número de identificação do processo que originou esta mensagem.
     */
    private final int pid;

    /**
     * Constrói uma marca temporal extendida que usa como segundo critério
     * de ordenação o número de identificação do processo que a originou.
     * 
     * @param pid Número de identificação do processo que originou esta mensagem.
     */
    public ExtendedTimestamp(int pid) {
        ts = new Timestamp(new Date().getTime());
        this.pid = pid;
    }

    /**
     * Devolve a marca temporal.
     * 
     * @return a marca temporal.
     */
    public Timestamp getTimestamp() {
        return ts;
    }

    /**
     * Devolve o número de identificação do processo que originou esta marca 
     * temporal.
     * 
     * @return o número de identificação do processo que originou esta marca 
     * temporal.
     */
    public int getPID() {
        return pid;
    }

    @Override
    public int compareTo(ExtendedTimestamp ets) {
        int cmp;
        if ((cmp = ts.compareTo(ets.ts)) == 0) {
            if (pid == ets.pid) {
                return 0;
            } else if (pid < ets.pid) {
                return -1;
            } else {
                return 1;
            }
        }
        return cmp;
    }

    @Override
    public String toString() {
        return "ExtendedTimestamp { timestamp = " + ts.toString() + ", pid = " + pid + "}";
    }
}
