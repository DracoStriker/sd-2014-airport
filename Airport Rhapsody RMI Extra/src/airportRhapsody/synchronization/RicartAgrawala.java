package airportRhapsody.synchronization;

import airportRhapsody.protocol.AccessGranted;
import airportRhapsody.protocol.ProtocolMessage;
import airportRhapsody.protocol.RequestAccess;
import airportRhapsody.protocol.WakeUp;
import airportRhapsody.util.Serialization;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Mecânismo de sincronização distribuído que usa o algoritmo de Ricart e
 * Agrawala para entrar coordenadamente numa região crítica, negociando com
 * outros processos com o mesmo interesse.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 03-07-2014
 */
public class RicartAgrawala {

    /**
     * Condição que indica que o pedido multicast de acesso à região crítica foi
     * realizado.
     */
    private boolean requestMessageReady;

    /**
     * Número de pedidos aceites para entrar na região crítica.
     */
    private int numberOfRequestsGranted;

    /**
     * Último pedido enviado por este processo para entrar na região crítica.
     */
    private RequestAccess myRequest;

    /**
     * Fila de pedidos pendentes de acesso à região crítica por parte de outros
     * processos.
     */
    private final ArrayList<RequestAccess> requestQueue;

    /**
     * Número de processos envolvidos neste grupo de sincronização.
     */
    private final int numOfProcess;

    /**
     * Canal de comunicação unicast com o grupo de sincronização.
     */
    private final DatagramSocket dSocket;

    /**
     * Estado do algoritmo de Ricart e Agrawala para a entrada na região
     * crítica.
     */
    private State state;

    /**
     * Número de identificação do processo que usa este mecânismo de
     * sincronização.
     */
    private final int pid;

    /**
     * Thread processadora de pedidos unicast.
     */
    private final RequestHandler handler;

    /**
     * Lista dos pares (Porto UDP, IP Unicast) dos processos envolvidos neste
     * grupo de sincronização.
     */
    private final List<InetSocketAddress> pAddress;

    /**
     * Mecânismo de sincronização interno.
     */
    private final ReentrantLock lock;

    /**
     * Condição que indica que o pedido multicast de acesso à região crítica foi
     * realizado.
     */
    private final Condition requestMessageReadyCondition;

    /**
     * Condição que indica que o número de pedidos aceites para entrar na região
     * crítica é igual ao número de processos do grupo menos 1.
     */
    private final Condition numberOfRequestsGrantedCondition;

    /**
     * Condição que indica que o processo se enontra bloqueado neste mecânismo
     * de sincronização.
     */
    private final Condition blockCondition;

    /**
     * Condição que indica que o processo se enontra bloqueado neste mecânismo
     * de sincronização.
     */
    private boolean block;

    /**
     * Condição de encerramento do mecânismo de sincronização.
     */
    private volatile boolean close;

    /**
     * Constrói um mecânismo de sincronização distribuído.
     *
     * @param pid Número de identificação do processo que usa este mecânismo de
     * sincronização.
     * @param pAddress Lista dos pares (Porto UDP, IP Unicast) dos processos
     * envolvidos neste grupo de sincronização.
     * @throws UnknownHostException quando algum dos endereços IP é
     * desconhecido.
     * @throws IOException Quando ocorre algum erro na inicialização dos canais
     * de comunicação (unicast ou multicast).
     */
    @SuppressWarnings("CallToThreadStartDuringObjectConstruction")
    public RicartAgrawala(int pid, List<InetSocketAddress> pAddress) throws UnknownHostException, IOException {
        this.pid = pid;
        state = State.outsideCR;
        requestMessageReady = false;
        numOfProcess = pAddress.size();
        requestQueue = new ArrayList<>();
        this.pAddress = pAddress;
        InetSocketAddress addr = pAddress.get(pid);
        dSocket = new DatagramSocket(addr.getPort(), addr.getAddress());
        lock = new ReentrantLock(true);
        requestMessageReadyCondition = lock.newCondition();
        numberOfRequestsGrantedCondition = lock.newCondition();
        block = true;
        blockCondition = lock.newCondition();
        close = false;
        handler = new RequestHandler();
        handler.setDaemon(true);
        handler.start();
    }

    /**
     * O processo entra na região crítica quando tal for permitido por todos os
     * processos de grupo.
     *
     * @throws IOException quando ocorre algum erro no envio do pedido de
     * entrada multicast.
     */
    public void enterCR() throws IOException {
        lock.lock();
        try {
            System.out.println("Process " + pid + " wants to enter in the Critical Region");
            // I want to enter in the critical region
            state = State.wantIn;
            // reset request grant count
            numberOfRequestsGranted = 0;
            // multicast request access
            myRequest = new RequestAccess(pid);
            multicast(myRequest);
            System.out.println("Process " + pid + " Multicast " + myRequest);
            // multicast done
            requestMessageReady = true;
            requestMessageReadyCondition.signal();
            // wait for all access grant
            while (numberOfRequestsGranted != numOfProcess - 1) {
                try {
                    numberOfRequestsGrantedCondition.await();
                } catch (InterruptedException ex) {
                }
            }
            // enter in the critical region
            state = State.insideCR;
            System.out.println("Process " + pid + " Enter the Critical Region");
        } finally {
            lock.unlock();
        }
    }

    /**
     * O processo saí da região crítica e avisa os restates processos que querem
     * entrar entrar na região crítica.
     *
     * @throws IOException quando ocorre algum erro no envio das permissões
     * unicast.
     */
    public void exitCR() throws IOException {
        lock.lock();
        try {
            // exiting the critical region
            state = State.outsideCR;
            System.out.println("Process " + pid + " Exit the Critical Region");
            // multicast is not done yet
            requestMessageReady = false;
            // send all pending access granted
            AccessGranted grant = new AccessGranted(pid);
            while (!requestQueue.isEmpty()) {
                RequestAccess request = requestQueue.remove(0);
                unicast(grant, request.getPID());
                System.out.println("Unicast " + grant + " to Process " + request.getPID());
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * O processo bloqueia até receber uma mensagem wakeUp de outro processo.
     */
    public void block() {
        lock.lock();
        try {
            // wait until wakeup message
            System.out.println("Process " + pid + " Blocked");
            while (block) {
                try {
                    blockCondition.await();
                } catch (InterruptedException ex) {
                }
            }
            block = true;
            System.out.println("Process " + pid + " Unblocked");
        } finally {
            lock.unlock();
        }
    }

    /**
     * Envia uma mensagem wakeUp para um processo.
     *
     * @param pid Número de identificação do processo bloqueado.
     * @throws IOException quando ocorre algum erro no envio das permissões
     * unicast.
     */
    public void unblock(int pid) throws IOException {
        // unicast wakeup message
        WakeUp wakeup = new WakeUp(this.pid);
        unicast(wakeup, pid);
        System.out.println("Process " + this.pid + " Unicast " + wakeup + " to Process " + pid);
    }

    /**
     * Envia uma mensagem wakeUp para todos os processos.
     *
     * @throws IOException quando ocorre algum erro no envio da mensagem wakeUP
     * multicast.
     */
    public void unblockAll() throws IOException {
        // multicast wakeup message
        WakeUp wakeup = new WakeUp(pid);
        multicast(wakeup);
        System.out.println("Process " + pid + " Multicast " + wakeup);
    }

    /**
     * O processo abandona este grupo de sincronização.
     */
    public void leave() {
        close = true;
        dSocket.close();
    }

    /**
     * 
     * @param protoMsg
     * @param recvPid
     * @throws IOException 
     */
    private void unicast(ProtocolMessage protoMsg, int pid) throws IOException {
        byte[] msg = Serialization.serializeObject(protoMsg);
        InetSocketAddress address = pAddress.get(pid);
        DatagramPacket packet = new DatagramPacket(msg, msg.length, address.getAddress(), address.getPort());
        dSocket.send(packet);
    }

    /**
     * 
     * @param protoMsg
     * @throws IOException 
     */
    private void multicast(ProtocolMessage protoMsg) throws IOException {
        byte[] msg = Serialization.serializeObject(protoMsg);
        for (int i = 0; i < numOfProcess; i++) {
            if (i != pid) {
                InetSocketAddress address = pAddress.get(i);
                DatagramPacket packet = new DatagramPacket(msg, msg.length, address.getAddress(), address.getPort());
                dSocket.send(packet);

            }
        }
    }

    /**
     * Estados do algoritmo de Ricart e Agrawala.
     */
    private enum State {

        outsideCR, wantIn, insideCR
    }

    /**
     * Thread processadora de pedidos unicast.
     */
    private class RequestHandler extends Thread {

        @Override
        public void run() {
            byte[] buf = new byte[1000];
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            AccessGranted grant;
            Object recv;
            while (!close) {
                try {
                    dSocket.receive(packet);
                } catch (IOException ex) {
                    if (!close) {
                        System.err.println(ex);
                    }
                    continue;
                }
                try {
                    recv = Serialization.deserializeObject(packet.getData());
                } catch (IOException | ClassNotFoundException ex) {
                    if (!close) {
                        System.err.println(ex);
                    }
                    continue;
                }
                if (recv instanceof RequestAccess) {
                    RequestAccess request = (RequestAccess) recv;
                    lock.lock();
                    try {
                        System.out.println("Process " + pid + " Recived " + request);
                        switch (state) {
                            case outsideCR:
                                System.out.println("Process " + pid + " State is outsideCR");
                                grant = new AccessGranted(pid);
                                try {
                                    unicast(grant, request.getPID());
                                    System.out.println("Process " + pid + " Unicast " + grant + " to Process " + request.getPID());
                                } catch (IOException ex) {
                                    if (!close) {
                                        System.err.println(ex);
                                    }
                                }
                                break;
                            case wantIn:
                                System.out.println("Process " + pid + " State is wantIn");
                                while (!requestMessageReady) {
                                    try {
                                        requestMessageReadyCondition.await();
                                    } catch (InterruptedException ex) {
                                    }
                                }
                                if (myRequest.compareTo(request) > 0) {
                                    System.out.println("Process " + pid + " Timestamp is less recent than Process " + request.getPID());
                                    requestQueue.add(request);
                                    System.out.println("Process " + pid + " Pending " + request);
                                } else {
                                    System.out.println("Process " + pid + " Timestamp is more recent than Process " + request.getPID());
                                    grant = new AccessGranted(pid);
                                    try {
                                        unicast(grant, request.getPID());
                                        System.out.println("Process " + pid + " Unicast " + grant + " to Process " + request.getPID());
                                    } catch (IOException ex) {
                                        if (!close) {
                                            System.err.println(ex);
                                        }
                                    }
                                }
                                break;
                            case insideCR:
                                System.out.println("Process " + pid + " State is insideCR");
                                requestQueue.add(request);
                                System.out.println("Process " + pid + " Pending " + request);
                                break;
                        }
                    } finally {
                        lock.unlock();
                    }
                } else if (recv instanceof AccessGranted) {
                    AccessGranted accessGranted = (AccessGranted) recv;
                    lock.lock();
                    try {
                        System.out.println("Process " + pid + " Recived " + accessGranted);
                        // processa autorizacao de acesso
                        numberOfRequestsGranted++;
                        if (numberOfRequestsGranted == numOfProcess - 1) {
                            numberOfRequestsGrantedCondition.signal();
                        }
                    } finally {
                        lock.unlock();
                    }
                } else if (recv instanceof WakeUp) {
                    WakeUp wakeUp = (WakeUp) recv;
                    lock.lock();
                    try {
                        System.out.println("Process " + pid + " Recived " + wakeUp);
                        block = false;
                        blockCondition.signal();
                    } finally {
                        lock.unlock();
                    }
                }
            }
        }
    }
}
