package airportRhapsody.synchronization;

import java.io.Serializable;

/**
 * Implementação de um relógio vectorial.
 * 
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 20-05-2014
 */
public class VectorClock implements Serializable, Comparable<VectorClock> {

    /**
     * Relógio vectorial, contém um elemento por cada entidade activa da
     * simulação da rapsódia do aeroporto, número de passageiros mais 2.
     */
    private final int[] clock;

    /**
     * Contrói um relógio vectorial com base do número de passageiros por voo.
     * 
     * @param numOfPassengers número de passageiros por voo.
     */
    public VectorClock(int numOfPassengers) {
        clock = new int[numOfPassengers + 2];
    }
    
    /**
     * Constrói um relógio vectorial, cópia de um outro.
     * 
     * @param vectorClock relógio vectorial original.
     */
    public VectorClock(VectorClock vectorClock) {
        clock = new int[vectorClock.clock.length];
        System.arraycopy(vectorClock.clock, 0, clock, 0, vectorClock.clock.length);
    }

    /**
     * Incrementa o relógio de um passageiro.
     * 
     * @param pid número de identificação do passageiro.
     */
    public void incrPassengerClk(int pid) {
        clock[pid]++;
    }

    /**
     * Incrementa o relógio do bagageiro.
     */
    public void incrPorterClk() {
        clock[clock.length - 2]++;
    }

    /**
     * Incrementa o relógio do motorista.
     */
    public void incrDriverClk() {
        clock[clock.length - 1]++;
    }

    /**
     * Actualiza este relógio vectorial com base num outro (operação de
     * merge).
     * 
     * @param externalClk o relógio vectorial de referência.
     */
    public void update(VectorClock externalClk) {
        for (int i = 0; i < clock.length; i++) {
            if (clock[i] < externalClk.clock[i]) {
                clock[i] = externalClk.clock[i];
            }
        }
    }

    @Override
    public int compareTo(VectorClock o) {
        boolean prevEvent = false;
        boolean nextEvent = false;
        for (int i = 0; i < clock.length; i++) {
            if (clock[i] < o.clock[i]) {
                prevEvent = true;
            } else if (clock[i] > o.clock[i]) {
                nextEvent = true;
            }
            if (prevEvent && nextEvent) {
                return 0; // concurrent events
            }
        }
        if (prevEvent) {
            return -1; // previous event
        } else if (nextEvent) {
            return 1; // forward event
        } else {
            return 0; // same event
        }
    }

    @Override
    public String toString() {
        String clk = "{";
        for (int i = 0; i < clock.length - 1; i++) {
            clk += clock[i] + ", ";
        }
        clk += clock[clock.length-1] + "}";
        return clk;
    }

}
