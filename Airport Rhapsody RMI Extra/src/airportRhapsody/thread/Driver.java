package airportRhapsody.thread;

import airportRhapsody.mutex.Driver_ATT;
import airportRhapsody.mutex.Driver_DTT;
import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.RicartAgrawala;
import airportRhapsody.synchronization.VectorClock;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Thread que representa e a entidade Motorista.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Driver implements Runnable {

    /**
     * Monitor da zona de transferência para o terminal de desembarque do
     * aeroporto.
     */
    private final Driver_ATT attMon;

    /**
     * Monitor da zona de transferência para o terminal de embarque do
     * aeroporto.
     */
    private final Driver_DTT dttMon;

    /**
     * Relógio vectorial do processo.
     */
    private final VectorClock clk;
    
    /**
     * Dispositivo de sincronização distribuído de acesso a uma região crítica
     * segundo o algoritmo de Ricart e Agrawala.
     */
    private final RicartAgrawala sync;

    /**
     * Inicializa a thread do motorista, recebendo a referência do monitor da
     * zona de transferência para o terminal de desembarque, a referência do
     * monitor da zona de transferência para o terminal de embarque e o relógio
     * vectorial.
     *
     * @param attMon Monitor da zona de transferência para o terminal de
     * desembarque do aeroporto.
     * @param dttMon Monitor da zona de transferência para o terminal de
     * embarque do aeroporto.
     * @param clk relógio vectorial do processo.
     * @param sync
     */
    public Driver(Driver_ATT attMon, Driver_DTT dttMon, VectorClock clk, RicartAgrawala sync) {
        this.attMon = attMon;
        this.dttMon = dttMon;
        this.clk = clk;
        this.sync = sync;
    }
    
    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        Return ret;
        boolean workEnded;
        ArrayList<Integer> passengersOnTheBus;
        int nPassengersOnTheBus; //numero de passageiros presentes no autocarro
        Random r = new Random(); //gerador de numeros aleatorios
        try {
            //ciclo de vida do motorista
            while (true) {
                
                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = attMon.hasDaysWorkEnded(clk);
                workEnded = (boolean) ret.getObj();
                clk.update(ret.getVc());
                
                if (workEnded) {
                    break;
                }
                
                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = attMon.announcingBusBoarding(clk);
                clk.update(ret.getVc());
                
                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = attMon.goToDepartureTerminal(clk);
                passengersOnTheBus = (ArrayList<Integer>) ret.getObj();
                nPassengersOnTheBus = passengersOnTheBus.size();
                clk.update(ret.getVc());
                
                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                sync.enterCR();
                ret = dttMon.parkTheBusAndLetPassOff(nPassengersOnTheBus, clk);
                System.out.println("o motorista avisa os passageiros que já podem sair");
                // o motorista avisa os passageiros que já podem sair
                for (int id : passengersOnTheBus) {
                    sync.unblock(id + 1);
                }
                sync.exitCR();
                clk.update(ret.getVc());
                
                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                System.out.println("o motorista espera que todos os passageiros saiam");
                sync.block(); // o motorista espera que todos os passageiros saiam
                sync.enterCR();
                ret = dttMon.goToArrivalTerminal(clk);
                sync.exitCR();
                clk.update(ret.getVc());
                
                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = attMon.parkTheBus(clk);
                clk.update(ret.getVc());
            }
            System.out.println("Driver work has ended."); //print apenas para debug
        } catch (InterruptedException ex) {
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
