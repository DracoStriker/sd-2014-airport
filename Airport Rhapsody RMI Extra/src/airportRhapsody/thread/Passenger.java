package airportRhapsody.thread;

import airportRhapsody.mutex.Passenger_ATE;
import airportRhapsody.mutex.Passenger_ATT;
import airportRhapsody.mutex.Passenger_DTE;
import airportRhapsody.mutex.Passenger_DTT;
import airportRhapsody.mutex.Passenger_DZ;
import airportRhapsody.mutex.Passenger_LCP;
import airportRhapsody.protocol.Return;
import airportRhapsody.struct.Bag;
import airportRhapsody.struct.Passenger_Status;
import static airportRhapsody.struct.Passenger_Status.FINAL_DESTINATION_NO_BAGS;
import static airportRhapsody.struct.Passenger_Status.FINAL_DESTINATION_WITH_BAGS;
import static airportRhapsody.struct.Passenger_Status.IN_TRANSIT;
import airportRhapsody.synchronization.RicartAgrawala;
import airportRhapsody.synchronization.VectorClock;
import java.io.IOException;
import java.util.Random;

/**
 * Thread que representa a entidade Passageiro.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Passenger implements Runnable {

    /**
     * Identificação do passageiro.
     */
    private final int id;

    /**
     * Número de malas que o passageiro possui, no início da simulação.
     */
    private final int nBags;

    /**
     * Número de malas que o passageiro possui actualmente.
     */
    private int nBagsReal = 0;

    /**
     * Monitor do terminal de desembarque do aeroporto.
     */
    private final Passenger_ATE ateMon;

    /**
     * Monitor da zona de transferência para o terminal de desembarque do
     * aeroporto.
     */
    private final Passenger_ATT attMon;

    /**
     * Terminal de embarque do aeroporto.
     */
    private final Passenger_DTE dteMon;

    /**
     * * Monitor da zona de transferência para o terminal de embarque do
     * aeroporto.
     */
    private final Passenger_DTT dttMon;

    /**
     * Monitor da zona de desembarque, onde os passageiros saem do avião.
     */
    private final Passenger_DZ dzMon;

    /**
     * Monitor da zona de recolha de bagagens.
     */
    private final Passenger_LCP lcpMon;

    /**
     * Relógio vectorial do processo.
     */
    private final VectorClock clk;

    /**
     * Dispositivo de sincronização distribuído de acesso a uma região crítica
     * segundo o algoritmo de Ricart e Agrawala.
     */
    private final RicartAgrawala sync;

    /**
     * Inicializa a thread do passageiro, recebendo a identificação do mesmo, o
     * número de malas que ele possui no início da simulação, a referência dos
     * monitores em que ele participa e o relógio vectorial.
     *
     * @param id Identificação do passageiro
     * @param nBags Número de malas que o passageiro possui no início da
     * simulação.
     * @param ateMon Monitor do terminal de desembarque do aeroporto.
     * @param attMon Monitor da zona de transferência para o terminal de
     * desembarque do aeroporto.
     * @param dteMon Terminal de embarque do aeroporto.
     * @param dttMon Monitor da zona de transferência para o terminal de
     * embarque do aeroporto.
     * @param dzMon Monitor da zona de desembarque, onde os passageiros saem do
     * avião.
     * @param lcpMon Monitor da zona de recolha de bagagens.
     * @param clk Relógio vectorial do processo.
     * @param sync
     */
    public Passenger(int id, int nBags, Passenger_ATE ateMon, Passenger_ATT attMon, Passenger_DTE dteMon, Passenger_DTT dttMon, Passenger_DZ dzMon, Passenger_LCP lcpMon, VectorClock clk, RicartAgrawala sync) {
        this.id = id;
        this.nBags = nBags;
        this.ateMon = ateMon;
        this.attMon = attMon;
        this.dteMon = dteMon;
        this.dttMon = dttMon;
        this.dzMon = dzMon;
        this.lcpMon = lcpMon;
        this.clk = clk;
        this.sync = sync;
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        Return ret;
        Bag bag;
        Random r = new Random(); //gerador de números aleatórios
        boolean iAmTheLastOne;
        try {
            //ciclo de vida do passageiro

            Thread.sleep(r.nextInt(100));
            clk.incrPassengerClk(id);
            ret = dzMon.whatShouldIDo(id, clk);
            clk.update(ret.getVc());

            switch ((Passenger_Status) ret.getObj()) { //dependendo do estado interno, o passageiro executará varios métodos
                case IN_TRANSIT: //destino do passageiro não é o final

                    Thread.sleep(r.nextInt(100));
                    clk.incrPassengerClk(id);
                    ret = attMon.takeABus(id, clk);
                    clk.update(ret.getVc());

                    Thread.sleep(r.nextInt(100));
                    clk.incrPassengerClk(id);
                    ret = attMon.enterTheBus(id, clk);
                    clk.update(ret.getVc());

                    Thread.sleep(r.nextInt(100));
                    clk.incrPassengerClk(id);
                    System.out.println("o passageiro " + (id + 1) + " espera que o autocarro estacione");
                    sync.block(); // o passageiro espera que o autocarro estacione
                    sync.enterCR();
                    ret = dttMon.leaveTheBus(id, clk);
                    clk.update(ret.getVc());
                    iAmTheLastOne = (Boolean) ret.getObj();
                    if (iAmTheLastOne) {
                        System.out.println("o passageiro " + (id + 1) + " é o último a sair do autocarro, portanto avisa o motorista que já sairam todos");
                        sync.unblock(0); // o último passageiro avisa o motorista que já sairam todos
                    }
                    sync.exitCR();

                    Thread.sleep(r.nextInt(100));
                    clk.incrPassengerClk(id);
                    ret = dteMon.prepareNextLeg(id, clk);
                    clk.update(ret.getVc());

                    break;

                case FINAL_DESTINATION_WITH_BAGS: //destino do passageiro é o final e tem malas

                    Thread.sleep(r.nextInt(100));
                    clk.incrPassengerClk(id);
                    ret = lcpMon.goCollectABag(id, clk);
                    bag = (Bag) ret.getObj();
                    clk.update(ret.getVc());

                    while (bag != null) {

                        nBagsReal++;

                        Thread.sleep(r.nextInt(100));
                        clk.incrPassengerClk(id);
                        ret = lcpMon.goCollectABag(id, clk);
                        bag = (Bag) ret.getObj();
                        clk.update(ret.getVc());
                    }

                    if (nBags != nBagsReal) {

                        Thread.sleep(r.nextInt(100));
                        clk.incrPassengerClk(id);
                        ret = ateMon.reportMissingBags(id, clk);
                        clk.update(ret.getVc());
                    }

                    Thread.sleep(r.nextInt(100));
                    clk.incrPassengerClk(id);
                    ret = ateMon.goHome(id, clk);
                    clk.update(ret.getVc());

                    break;

                case FINAL_DESTINATION_NO_BAGS: //destino do passageiro é o final e não tem malas

                    Thread.sleep(r.nextInt(100));
                    clk.incrPassengerClk(id);
                    ret = ateMon.goHome(id, clk);
                    clk.update(ret.getVc());

                    break;
            }
        } catch (InterruptedException ex) {
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
