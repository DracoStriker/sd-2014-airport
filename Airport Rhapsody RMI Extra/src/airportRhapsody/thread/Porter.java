package airportRhapsody.thread;

import airportRhapsody.mutex.Porter_DZ;
import airportRhapsody.mutex.Porter_LCP;
import airportRhapsody.protocol.Return;
import airportRhapsody.struct.Bag;
import airportRhapsody.synchronization.VectorClock;
import java.rmi.RemoteException;
import java.util.Random;

/**
 * Thread que representa a entidade Bagageiro.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Porter implements Runnable {

    /**
     * Monitor da zona de desembarque, onde os passageiros saem do avião.
     */
    private final Porter_DZ dzMon;

    /**
     * Monitor da zona de recolha de bagagens.
     */
    private final Porter_LCP lcpMon;

    /**
     * Número total de voos.
     */
    private final int K;

    /**
     * Relógio vectorial do processo.
     */
    private final VectorClock clk;

    /**
     * Inicializa a thread do motorista, recebendo o número total de voos, a
     * referência do monitor da zona de desembarque, a referência do monitor da
     * zona de recolha de bagagens e o relógio vectorial.
     *
     * @param K Número total de voos.
     * @param dzMon Monitor da zona de desembarque, onde os passageiros saem do
     * avião.
     * @param lcpMon Monitor da zona de recolha de bagagens.
     * @param clk Relógio vectorial do processo.
     */
    public Porter(int K, Porter_DZ dzMon, Porter_LCP lcpMon, VectorClock clk) {
        this.K = K;
        this.dzMon = dzMon;
        this.lcpMon = lcpMon;
        this.clk = clk;
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        Return ret;
        Bag bag; //mala que recolhe do aviao para o local apropriado
        Random r = new Random(); //gerador de números aleatórios
        //ciclo de vida do bagageiro
        try {
            for (int k = 0; k < K; k++) {

                Thread.sleep(r.nextInt(100));
                clk.incrPorterClk();
                ret = dzMon.takeARest(clk);
                clk.update(ret.getVc());
                
                while (true) {
                    
                    Thread.sleep(r.nextInt(100));
                    clk.incrPorterClk();
                    ret = dzMon.tryToCollectABag(clk);
                    bag = (Bag) ret.getObj();
                    clk.update(ret.getVc());
                    
                    if (bag == null) {
                        break;
                    }
                    
                    Thread.sleep(r.nextInt(100));
                    clk.incrPorterClk();
                    ret = lcpMon.carryItToAppropriateStore(bag, clk);
                    clk.update(ret.getVc());
                }
                
                Thread.sleep(r.nextInt(100));
                clk.incrPorterClk();
                ret = lcpMon.noMoreBagsToCollect(clk);
                clk.update(ret.getVc());
            }
            
            System.out.println("Porter work has ended."); //print apenas para debug
            
        } catch (InterruptedException ex) {
        } catch (RemoteException ex) {
            System.err.println(ex);
        }
    }
}
