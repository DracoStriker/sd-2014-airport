package airportRhapsody.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Conjunto de serviços de serialização de objectos.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 02-07-2014
 */
public class Serialization {

    private Serialization() {
    }

    /**
     * Serializa um objecto.
     * 
     * @param o o Objecto a serializar.
     * @return a serialização do objecto sobre a forma de um array de bytes.
     * @throws IOException quando ocorre alguma falha de entrada ou saída do 
     * sistema.
     */
    public static byte[] serializeObject(Object o) throws IOException {
        byte[] serialization;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            try (ObjectOutput out = new ObjectOutputStream(bos)) {
                out.writeObject(o);
                serialization = bos.toByteArray();
            }
        }
        return serialization;
    }

    /**
     * Deserializa um objecto.
     * 
     * @param serialization a serialização do objecto sobre a forma de um array 
     * de bytes.
     * @return o Objecto a deserializado.
     * @throws IOException quando ocorre alguma falha de entrada ou saída do 
     * sistema.
     * @throws ClassNotFoundException quando o objecto deserializado não é
     * conhecide por esta JVM.
     */
    public static Object deserializeObject(byte[] serialization) throws IOException, ClassNotFoundException {
        Object obj;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(serialization)) {
            try (ObjectInput in = new ObjectInputStream(bis)) {
                obj = in.readObject();
            }
        }
        return obj;
    }
}
