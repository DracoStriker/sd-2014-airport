               AIRPORT RHAPSODY - Description of the internal state of the problem
PLANE    PORTER                  DRIVER
FN BN  Stat CB SR   Stat  Q0 Q1 Q2 Q3 Q4 Q5  S0 S1 S2
                                                             PASSENGERS
St0 Si0 NR0 NA0 St1 Si1 NR1 NA1 St2 Si2 NR2 NA2 St3 Si3 NR3 NA3 St4 Si4 NR4 NA4 St5 Si5 NR5 NA5 
{0, 0, 0, 0, 0, 0, 0, 0}
 1  3  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ FDT  0   0   DZ FDT  0   0   DZ FDT  0   0   DZ FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{1, 1, 2, 1, 1, 1, 0, 0}
 1  3  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ FDT  0   0   DZ FDT  0   0  ATE FDT  0   0   DZ FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{1, 1, 2, 2, 1, 1, 0, 0}
 1  3  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ FDT  0   0   DZ FDT  0   0  ATE FDT  0   0  ATE FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{2, 1, 2, 2, 1, 1, 0, 0}
 1  3  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0   DZ FDT  0   0  ATE FDT  0   0  ATE FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{2, 2, 2, 2, 1, 1, 0, 0}
 1  3  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{2, 2, 2, 2, 1, 1, 2, 1}
 1  2  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{2, 2, 2, 2, 2, 2, 3, 1}
 1  2  ALBC  1  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{2, 2, 2, 2, 2, 2, 3, 1}
 1  2  ALBC  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0   DZ FDT  1   0  LCP FDT  2   1  
{2, 2, 2, 2, 2, 2, 4, 1}
 1  1  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0   DZ FDT  1   0  LCP FDT  2   1  
{2, 2, 2, 2, 2, 2, 5, 1}
 1  1  ALBC  1  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0   DZ FDT  1   0  LCP FDT  2   1  
{2, 2, 2, 2, 2, 2, 5, 1}
 1  1  ALBC  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  LCP FDT  1   1  LCP FDT  2   1  
{2, 2, 2, 2, 2, 2, 6, 1}
 1  0  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  LCP FDT  1   1  LCP FDT  2   1  
{2, 2, 2, 2, 2, 3, 7, 1}
 1  0  ALBC  1  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  LCP FDT  1   1  LCP FDT  2   1  
{2, 2, 2, 2, 2, 3, 7, 1}
 1  0  ALBC  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  LCP FDT  1   1  LCP FDT  2   2  
{2, 2, 2, 2, 2, 3, 8, 1}
 1  0  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  LCP FDT  1   1  LCP FDT  2   2  
{2, 2, 2, 2, 3, 4, 9, 1}
 1  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  LCP FDT  1   1  LCP FDT  2   2  
{2, 2, 2, 2, 4, 4, 9, 1}
 1  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  1   1  LCP FDT  2   2  
{2, 2, 2, 2, 4, 5, 9, 1}
 1  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  0   0  ATE FDT  1   1  ATE FDT  2   2  
{2, 2, 2, 2, 4, 5, 9, 1}
 2  2  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ FDT  0   0   DZ FDT  0   0   DZ TRT  0   0   DZ FDT  2   0   DZ TRT  0   0   DZ FDT  2   0  
{3, 4, 3, 2, 5, 6, 10, 1}
 2  2  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ FDT  0   0  ATE FDT  0   0   DZ TRT  0   0   DZ FDT  2   0   DZ TRT  0   0   DZ FDT  2   0  
{3, 4, 3, 2, 5, 7, 10, 1}
 2  2  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ FDT  0   0  ATE FDT  0   0   DZ TRT  0   0   DZ FDT  2   0   DZ TRT  0   0  ATE FDT  2   0  
{3, 4, 3, 3, 6, 7, 10, 1}
 2  2  WPTL  0  0    AT    4  -  -  -  -  -   -  -  - 
 DZ FDT  0   0  ATE FDT  0   0   DZ TRT  0   0   DZ FDT  2   0  ATT TRT  0   0  ATE FDT  2   0  
{4, 4, 3, 3, 6, 7, 10, 1}
 2  2  WPTL  0  0    AT    4  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0   DZ TRT  0   0   DZ FDT  2   0  ATT TRT  0   0  ATE FDT  2   0  
{4, 4, 3, 3, 7, 7, 10, 2}
 2  2  WPTL  0  0    AT    -  -  -  -  -  -   4  -  - 
ATE FDT  0   0  ATE FDT  0   0   DZ TRT  0   0   DZ FDT  2   0   TT TRT  0   0  ATE FDT  2   0  
{4, 4, 3, 3, 7, 7, 11, 2}
 2  1  APHL  0  0    AT    -  -  -  -  -  -   4  -  - 
ATE FDT  0   0  ATE FDT  0   0   DZ TRT  0   0   DZ FDT  2   0   TT TRT  0   0  ATE FDT  2   0  
{4, 4, 4, 3, 7, 7, 11, 2}
 2  1  APHL  0  0    AT    2  -  -  -  -  -   4  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0   DZ FDT  2   0   TT TRT  0   0  ATE FDT  2   0  
{4, 4, 4, 4, 7, 7, 12, 2}
 2  1  ALBC  1  0    AT    2  -  -  -  -  -   4  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0   DZ FDT  2   0   TT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 7, 7, 12, 3}
 2  1  ALBC  1  0    DF    2  -  -  -  -  -   4  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0   DZ FDT  2   0   TT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 7, 7, 12, 3}
 2  1  ALBC  0  0    DF    2  -  -  -  -  -   4  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   1   TT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 8, 7, 12, 4}
 2  1  ALBC  0  0    DT    2  -  -  -  -  -   4  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   1   TT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 8, 7, 12, 4}
 2  1  ALBC  0  0    DT    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   1  DTT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 8, 7, 12, 5}
 2  1  ALBC  0  0    DB    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   1  DTT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 8, 7, 13, 5}
 2  0  APHL  0  0    DB    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   1  DTT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 8, 7, 14, 5}
 2  0  ALBC  1  0    DB    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   1  DTT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 8, 7, 14, 6}
 2  0  ALBC  1  0    AT    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   1  DTT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 4, 8, 7, 15, 7}
 2  0  APHL  0  0    AT    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   2  DTT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 6, 8, 7, 16, 7}
 2  0  WPTL  0  0    AT    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   2  DTT TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 6, 9, 7, 17, 7}
 2  0  WPTL  0  0    AT    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 7, 9, 7, 17, 7}
 2  0  WPTL  0  0    AT    2  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  ATT TRT  0   0  ATE FDT  2   2  DTE TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 7, 9, 7, 17, 8}
 2  0  WPTL  0  0    AT    -  -  -  -  -  -   2  -  - 
ATE FDT  0   0  ATE FDT  0   0   TT TRT  0   0  ATE FDT  2   2  DTE TRT  0   0  ATE FDT  2   0  
{4, 4, 5, 7, 9, 7, 17, 9}
 2  0  WPTL  0  0    DF    -  -  -  -  -  -   2  -  - 
ATE FDT  0   0  ATE FDT  0   0   TT TRT  0   0  ATE FDT  2   2  DTE TRT  0   0  ATE FDT  2   0  
{4, 4, 6, 7, 9, 7, 17, 10}
 2  0  WPTL  0  0    DT    -  -  -  -  -  -   2  -  - 
ATE FDT  0   0  ATE FDT  0   0   TT TRT  0   0  ATE FDT  2   2  DTE TRT  0   0  ATE FDT  2   0  
{4, 4, 6, 7, 9, 7, 17, 10}
 2  0  WPTL  0  0    DT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  DTT TRT  0   0  ATE FDT  2   2  DTE TRT  0   0  ATE FDT  2   0  
{4, 4, 7, 7, 9, 7, 17, 10}
 2  0  WPTL  0  0    DT    -  -  -  -  -  -   -  -  - 
ATE FDT  0   0  ATE FDT  0   0  DTE TRT  0   0  ATE FDT  2   2  DTE TRT  0   0  ATE FDT  2   0  
{4, 4, 7, 7, 9, 7, 17, 11}
 3  0  WPTL  0  0    DB    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0   DZ FDT  1   0   DZ TRT  2   0   DZ FDT  2   0   DZ TRT  0   0   DZ FDT  2   0  
{4, 4, 7, 7, 9, 7, 17, 11}
 3  5  WPTL  0  0    DB    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0   DZ FDT  1   0   DZ FDT  2   0   DZ TRT  0   0   DZ FDT  0   0   DZ FDT  2   0  
{4, 5, 8, 9, 9, 8, 17, 11}
 3  5  WPTL  0  0    DB    3  -  -  -  -  -   -  -  - 
 DZ FDT  1   0   DZ FDT  1   0   DZ FDT  2   0  ATT TRT  0   0   DZ FDT  0   0   DZ FDT  2   0  
{5, 5, 8, 9, 11, 8, 17, 11}
 3  5  WPTL  0  0    DB    3  -  -  -  -  -   -  -  - 
 DZ FDT  1   0   DZ FDT  1   0   DZ FDT  2   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{5, 5, 8, 10, 11, 8, 17, 12}
 3  5  WPTL  0  0    AT    3  -  -  -  -  -   -  -  - 
 DZ FDT  1   0   DZ FDT  1   0   DZ FDT  2   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{5, 5, 8, 10, 11, 8, 18, 12}
 3  4  APHL  0  0    AT    3  -  -  -  -  -   -  -  - 
 DZ FDT  1   0   DZ FDT  1   0   DZ FDT  2   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 6, 9, 10, 11, 9, 19, 13}
 3  4  ALBC  1  0    AT    3  -  -  -  -  -   -  -  - 
 DZ FDT  1   0   DZ FDT  1   0   DZ FDT  2   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 6, 9, 10, 11, 9, 19, 13}
 3  4  ALBC  0  0    AT    3  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1   DZ FDT  2   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 6, 9, 10, 11, 9, 20, 13}
 3  3  APHL  0  0    AT    3  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1   DZ FDT  2   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 6, 9, 10, 11, 9, 20, 14}
 3  3  APHL  0  0    AT    -  -  -  -  -  -   3  -  - 
 DZ FDT  1   0  LCP FDT  1   1   DZ FDT  2   0   TT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 6, 9, 10, 11, 9, 20, 15}
 3  3  APHL  0  0    DF    -  -  -  -  -  -   3  -  - 
 DZ FDT  1   0  LCP FDT  1   1   DZ FDT  2   0   TT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 7, 9, 10, 11, 9, 21, 15}
 3  3  ALBC  1  0    DF    -  -  -  -  -  -   3  -  - 
 DZ FDT  1   0  LCP FDT  1   1   DZ FDT  2   0   TT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 7, 9, 10, 11, 9, 21, 15}
 3  3  ALBC  0  0    DF    -  -  -  -  -  -   3  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1   TT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 7, 9, 10, 11, 9, 22, 15}
 3  2  APHL  0  0    DF    -  -  -  -  -  -   3  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1   TT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 7, 9, 11, 11, 9, 22, 16}
 3  2  APHL  0  0    DT    -  -  -  -  -  -   3  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1   TT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 7, 9, 11, 11, 9, 22, 16}
 3  2  APHL  0  0    DT    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1  DTT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 7, 9, 12, 11, 9, 22, 16}
 3  2  APHL  0  0    DT    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1  DTE TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 7, 10, 12, 11, 9, 23, 16}
 3  2  ALBC  1  0    DT    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1  DTE TRT  0   0  ATE FDT  0   0   DZ FDT  2   0  
{6, 7, 10, 12, 11, 9, 23, 16}
 3  2  ALBC  0  0    DT    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 10, 12, 11, 9, 23, 17}
 3  2  ALBC  0  0    DB    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 10, 12, 11, 9, 24, 17}
 3  1  APHL  0  0    DB    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 10, 12, 11, 9, 25, 17}
 3  1  ALBC  1  0    DB    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   1  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 10, 12, 11, 9, 25, 17}
 3  1  ALBC  0  0    DB    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 10, 12, 11, 9, 26, 17}
 3  0  APHL  0  0    DB    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 10, 12, 11, 9, 26, 18}
 3  0  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 11, 12, 11, 10, 27, 19}
 3  0  ALBC  1  0    AT    -  -  -  -  -  -   -  -  - 
 DZ FDT  1   0  LCP FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 11, 12, 11, 10, 27, 19}
 3  0  ALBC  0  0    AT    -  -  -  -  -  -   -  -  - 
LCP FDT  1   1  LCP FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{6, 7, 11, 12, 11, 10, 28, 19}
 3  0  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
LCP FDT  1   1  LCP FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{7, 7, 11, 12, 11, 10, 29, 19}
 3  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
LCP FDT  1   1  LCP FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  
{7, 7, 11, 12, 11, 11, 29, 19}
 3  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
LCP FDT  1   1  LCP FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  BRO FDT  2   1  
{7, 8, 11, 12, 11, 11, 30, 19}
 3  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
LCP FDT  1   1  ATE FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  BRO FDT  2   1  
{7, 8, 11, 12, 11, 12, 30, 19}
 3  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
LCP FDT  1   1  ATE FDT  1   1  LCP FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  ATE FDT  2   1  
{7, 8, 12, 12, 11, 12, 30, 19}
 3  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
LCP FDT  1   1  ATE FDT  1   1  ATE FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  ATE FDT  2   1  
{8, 8, 12, 12, 11, 12, 30, 19}
 3  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
ATE FDT  1   1  ATE FDT  1   1  ATE FDT  2   2  DTE TRT  0   0  ATE FDT  0   0  ATE FDT  2   1  
{8, 8, 12, 12, 11, 12, 30, 19}
 4  2  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ TRT  0   0   DZ TRT  0   0   DZ FDT  1   0   DZ FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{9, 8, 13, 14, 11, 13, 30, 19}
 4  2  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ TRT  0   0   DZ TRT  0   0   DZ FDT  1   0  ATE FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{9, 9, 13, 14, 12, 13, 31, 19}
 4  1  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ TRT  0   0   DZ TRT  0   0   DZ FDT  1   0  ATE FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{9, 9, 14, 14, 13, 13, 32, 19}
 4  1  ALBC  1  0    AT    -  -  -  -  -  -   -  -  - 
 DZ TRT  0   0   DZ TRT  0   0   DZ FDT  1   0  ATE FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{10, 9, 14, 14, 13, 13, 32, 19}
 4  1  ALBC  0  0    AT    0  -  -  -  -  -   -  -  - 
ATT TRT  0   0   DZ TRT  0   0   DZ FDT  1   0  ATE FDT  0   0   DZ FDT  1   1   DZ FDT  2   0  
{10, 9, 14, 14, 13, 13, 32, 19}
 4  1  ALBC  0  0    AT    0  -  -  -  -  -   -  -  - 
ATT TRT  0   0   DZ TRT  0   0   DZ FDT  1   0  ATE FDT  0   0  LCP FDT  1   1   DZ FDT  2   0  
{10, 9, 14, 14, 13, 14, 32, 19}
 4  0  ALBC  0  0    AT    0  -  -  -  -  -   -  -  - 
ATT TRT  0   0   DZ TRT  0   0   DZ FDT  1   0  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{10, 9, 14, 14, 13, 14, 33, 19}
 4  0  APHL  0  0    AT    0  -  -  -  -  -   -  -  - 
ATT TRT  0   0   DZ TRT  0   0   DZ FDT  1   0  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{10, 10, 14, 14, 13, 14, 33, 20}
 4  0  APHL  0  0    AT    0  1  -  -  -  -   -  -  - 
ATT TRT  0   0  ATT TRT  0   0   DZ FDT  1   0  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{11, 10, 14, 14, 13, 14, 33, 20}
 4  0  APHL  0  0    AT    1  -  -  -  -  -   0  -  - 
 TT TRT  0   0  ATT TRT  0   0   DZ FDT  1   0  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{11, 10, 14, 14, 14, 14, 34, 20}
 4  0  ALBC  1  0    AT    1  -  -  -  -  -   0  -  - 
 TT TRT  0   0  ATT TRT  0   0   DZ FDT  1   0  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{11, 10, 14, 14, 14, 14, 34, 20}
 4  0  ALBC  0  0    AT    1  -  -  -  -  -   0  -  - 
 TT TRT  0   0  ATT TRT  0   0  LCP FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{11, 11, 14, 14, 14, 14, 34, 20}
 4  0  ALBC  0  0    AT    -  -  -  -  -  -   0  1  - 
 TT TRT  0   0   TT TRT  0   0  LCP FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{11, 11, 14, 14, 14, 14, 35, 20}
 4  0  APHL  0  0    AT    -  -  -  -  -  -   0  1  - 
 TT TRT  0   0   TT TRT  0   0  LCP FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{11, 11, 14, 14, 14, 14, 35, 21}
 4  0  APHL  0  0    DF    -  -  -  -  -  -   0  1  - 
 TT TRT  0   0   TT TRT  0   0  LCP FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{11, 11, 15, 14, 14, 14, 36, 21}
 4  0  WPTL  0  0    DF    -  -  -  -  -  -   0  1  - 
 TT TRT  0   0   TT TRT  0   0  LCP FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{11, 11, 16, 14, 14, 14, 36, 21}
 4  0  WPTL  0  0    DF    -  -  -  -  -  -   0  1  - 
 TT TRT  0   0   TT TRT  0   0  ATE FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{12, 12, 16, 14, 14, 14, 36, 22}
 4  0  WPTL  0  0    DT    -  -  -  -  -  -   0  1  - 
 TT TRT  0   0   TT TRT  0   0  ATE FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{12, 12, 16, 14, 14, 14, 36, 22}
 4  0  WPTL  0  0    DT    -  -  -  -  -  -   0  -  - 
 TT TRT  0   0  DTT TRT  0   0  ATE FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{12, 12, 16, 14, 14, 14, 36, 22}
 4  0  WPTL  0  0    DT    -  -  -  -  -  -   -  -  - 
DTT TRT  0   0  DTT TRT  0   0  ATE FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{12, 13, 16, 14, 14, 14, 36, 22}
 4  0  WPTL  0  0    DT    -  -  -  -  -  -   -  -  - 
DTT TRT  0   0  DTE TRT  0   0  ATE FDT  1   1  ATE FDT  0   0  LCP FDT  1   1  ATE FDT  2   0  
{12, 13, 16, 14, 15, 14, 36, 22}
 4  0  WPTL  0  0    DT    -  -  -  -  -  -   -  -  - 
DTT TRT  0   0  DTE TRT  0   0  ATE FDT  1   1  ATE FDT  0   0  ATE FDT  1   1  ATE FDT  2   0  
{13, 13, 16, 14, 15, 14, 36, 22}
 4  0  WPTL  0  0    DT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0  DTE TRT  0   0  ATE FDT  1   1  ATE FDT  0   0  ATE FDT  1   1  ATE FDT  2   0  
{13, 13, 16, 14, 15, 14, 37, 22}
 5  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ TRT  0   0   DZ FDT  1   0   DZ FDT  1   0   DZ FDT  0   0   DZ FDT  1   0   DZ FDT  2   0  
{13, 13, 16, 14, 15, 14, 37, 22}
 5  3  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ TRT  0   0   DZ FDT  1   0   DZ TRT  0   0   DZ FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{14, 14, 16, 16, 16, 14, 37, 22}
 5  3  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
 DZ TRT  0   0   DZ FDT  1   0   DZ TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{14, 14, 16, 16, 16, 14, 37, 23}
 5  3  WPTL  0  0    DB    -  -  -  -  -  -   -  -  - 
 DZ TRT  0   0   DZ FDT  1   0   DZ TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{15, 14, 17, 16, 16, 15, 37, 23}
 5  3  WPTL  0  0    DB    0  -  -  -  -  -   -  -  - 
ATT TRT  0   0   DZ FDT  1   0   DZ TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{15, 14, 17, 16, 16, 15, 37, 24}
 5  3  WPTL  0  0    AT    0  -  -  -  -  -   -  -  - 
ATT TRT  0   0   DZ FDT  1   0   DZ TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{15, 14, 18, 16, 16, 15, 37, 25}
 5  3  WPTL  0  0    AT    0  2  -  -  -  -   -  -  - 
ATT TRT  0   0   DZ FDT  1   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{15, 14, 18, 16, 16, 15, 38, 25}
 5  2  APHL  0  0    AT    0  2  -  -  -  -   -  -  - 
ATT TRT  0   0   DZ FDT  1   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{16, 14, 19, 16, 16, 15, 38, 26}
 5  2  APHL  0  0    AT    2  -  -  -  -  -   0  -  - 
 TT TRT  0   0   DZ FDT  1   0  ATT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{16, 14, 19, 16, 16, 15, 38, 26}
 5  2  APHL  0  0    AT    -  -  -  -  -  -   0  2  - 
 TT TRT  0   0   DZ FDT  1   0   TT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{16, 15, 19, 16, 17, 16, 39, 26}
 5  2  ALBC  1  0    AT    -  -  -  -  -  -   0  2  - 
 TT TRT  0   0   DZ FDT  1   0   TT TRT  0   0  ATE FDT  0   0   DZ FDT  2   0   DZ FDT  1   0  
{16, 15, 19, 16, 17, 16, 39, 26}
 5  2  ALBC  0  0    AT    -  -  -  -  -  -   0  2  - 
 TT TRT  0   0   DZ FDT  1   0   TT TRT  0   0  ATE FDT  0   0  LCP FDT  2   1   DZ FDT  1   0  
{16, 15, 19, 16, 17, 16, 39, 27}
 5  2  ALBC  0  0    DF    -  -  -  -  -  -   0  2  - 
 TT TRT  0   0   DZ FDT  1   0   TT TRT  0   0  ATE FDT  0   0  LCP FDT  2   1   DZ FDT  1   0  
{16, 15, 19, 16, 17, 16, 40, 27}
 5  1  APHL  0  0    DF    -  -  -  -  -  -   0  2  - 
 TT TRT  0   0   DZ FDT  1   0   TT TRT  0   0  ATE FDT  0   0  LCP FDT  2   1   DZ FDT  1   0  
{17, 15, 20, 16, 17, 16, 40, 28}
 5  1  APHL  0  0    DT    -  -  -  -  -  -   0  2  - 
 TT TRT  0   0   DZ FDT  1   0   TT TRT  0   0  ATE FDT  0   0  LCP FDT  2   1   DZ FDT  1   0  
{17, 15, 20, 16, 17, 16, 40, 28}
 5  1  APHL  0  0    DT    -  -  -  -  -  -   -  2  - 
DTT TRT  0   0   DZ FDT  1   0   TT TRT  0   0  ATE FDT  0   0  LCP FDT  2   1   DZ FDT  1   0  
{17, 15, 20, 16, 17, 16, 40, 28}
 5  1  APHL  0  0    DT    -  -  -  -  -  -   -  -  - 
DTT TRT  0   0   DZ FDT  1   0  DTT TRT  0   0  ATE FDT  0   0  LCP FDT  2   1   DZ FDT  1   0  
{17, 15, 20, 16, 18, 16, 41, 28}
 5  1  ALBC  1  0    DT    -  -  -  -  -  -   -  -  - 
DTT TRT  0   0   DZ FDT  1   0  DTT TRT  0   0  ATE FDT  0   0  LCP FDT  2   1   DZ FDT  1   0  
{17, 15, 20, 16, 18, 16, 41, 28}
 5  1  ALBC  0  0    DT    -  -  -  -  -  -   -  -  - 
DTT TRT  0   0   DZ FDT  1   0  DTT TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  LCP FDT  1   1  
{17, 15, 21, 16, 18, 16, 41, 28}
 5  1  ALBC  0  0    DT    -  -  -  -  -  -   -  -  - 
DTT TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  LCP FDT  1   1  
{18, 15, 21, 16, 18, 16, 41, 28}
 5  1  ALBC  0  0    DT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  LCP FDT  1   1  
{18, 15, 21, 16, 18, 16, 41, 29}
 5  1  ALBC  0  0    DB    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  LCP FDT  1   1  
{18, 15, 21, 16, 18, 16, 42, 29}
 5  0  APHL  0  0    DB    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  LCP FDT  1   1  
{18, 15, 21, 16, 18, 16, 42, 30}
 5  0  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  LCP FDT  1   1  
{18, 15, 21, 16, 18, 17, 43, 30}
 5  0  ALBC  1  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   1  LCP FDT  1   1  
{18, 15, 21, 16, 18, 17, 43, 30}
 5  0  ALBC  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   2  LCP FDT  1   1  
{18, 15, 21, 16, 18, 17, 44, 31}
 5  0  APHL  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   2  LCP FDT  1   1  
{18, 15, 21, 16, 19, 17, 45, 31}
 5  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0   DZ FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   2  LCP FDT  1   1  
{18, 15, 21, 16, 19, 17, 45, 31}
 5  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0  LCP FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  LCP FDT  2   2  LCP FDT  1   1  
{18, 15, 21, 16, 20, 17, 45, 31}
 5  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0  LCP FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  ATE FDT  2   2  LCP FDT  1   1  
{18, 15, 21, 16, 20, 18, 45, 31}
 5  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0  LCP FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  ATE FDT  2   2  ATE FDT  1   1  
{18, 16, 21, 16, 20, 18, 45, 31}
 5  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0  BRO FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  ATE FDT  2   2  ATE FDT  1   1  
{18, 17, 21, 16, 20, 18, 45, 31}
 5  0  WPTL  0  0    AT    -  -  -  -  -  -   -  -  - 
DTE TRT  0   0  ATE FDT  1   0  DTE TRT  0   0  ATE FDT  0   0  ATE FDT  2   2  ATE FDT  1   1  
