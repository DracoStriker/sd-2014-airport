#!/bin/bash
#!/usr/bin/expect

jarRun=( RegistryServer GIR_Process Logger_Process ATE_Process ATT_Process DTE_Process DTT_Process DZ_Process LCP_Process Passengers_Process Porter_Process Driver_Process )
machines=(1 2 3 4 5 6 7 9 10 11 1 2)
ports=(22441 22441 22441 22441 22441 22441 22441 22441 22441 22441 22442 22442)
rsAddress=192.168.8.171
rsPort=22440

for i in 1 2 3 4 5 6 7 8 9 10
do

printf "\nSending files to distributed machine ${machines[$i-1]} \n"
/usr/bin/expect<<End
set timeout 10 

spawn scp -o ConnectTimeout=5 -r ./Airport_Rhapsody_RMI.jar sd0405@l040101-ws${machines[$i-1]}.clients.ua.pt:~/
while {1} {
  expect {
 
    eof                          {break}
    "The authenticity of host"   {send "yes\r"}
    "password:"                  {send "yoloswag\r"}
    "*\]"                        {send "exit\r"}
  }
}

spawn scp -o ConnectTimeout=5 -r ./java.policy sd0405@l040101-ws${machines[$i-1]}.clients.ua.pt:~/
while {1} {
  expect {
 
    eof                          {break}
    "The authenticity of host"   {send "yes\r"}
    "password:"                  {send "yoloswag\r"}
    "*\]"                        {send "exit\r"}
  }
}

spawn scp -o ConnectTimeout=5 -r ./default.xml sd0405@l040101-ws${machines[$i-1]}.clients.ua.pt:~/
while {1} {
  expect {
 
    eof                          {break}
    "The authenticity of host"   {send "yes\r"}
    "password:"                  {send "yoloswag\r"}
    "*\]"                        {send "exit\r"}
  }
}

End
done

for j in 1 2 3 4 5 6 7 8 9 10 11 12
do

printf "\nExecuting in distributed machine ${machines[$j-1]} \n"
/usr/bin/expect<<EOF
set timeout 10
spawn ssh -o ConnectTimeout=5 sd0405@l040101-ws${machines[$j-1]}.clients.ua.pt
expect "password:"
send "yoloswag\r"
expect "\*]"
send "rm ${jarRun[$j-1]}_out.txt\r"
send "java -cp Airport_Rhapsody_RMI.jar airportRhapsody.run.${jarRun[$j-1]} >> ${jarRun[$j-1]}_out.txt $rsAddress $rsPort ${ports[$j-1]} &\r"
expect "*\]"
send "exit\r"
wait
EOF
done

