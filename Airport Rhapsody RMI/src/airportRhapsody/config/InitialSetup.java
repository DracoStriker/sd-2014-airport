package airportRhapsody.config;

import airportRhapsody.struct.Simulation;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Interface da configuração para uma simulação da Rapsódia no Aeroporto, esta
 * deve ser registada e acedida através de um servidor de registos.
 * 
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface InitialSetup extends RemotelyCloseable {

    /**
     * Obtém a lista de simulações, contendo toda a informação necessária para
     * cada simulação.
     *
     * @return Lista de simulações, contendo toda a informação necessária para
     * cada simulação.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public ArrayList<Simulation> getSimulations() throws RemoteException;

    /**
     * Obtém o número de voos.
     *
     * @return Número de voos.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public int getFlights() throws RemoteException;

    /**
     * Obtém o número de passageiros por voo.
     *
     * @return Número de passageiros por voo.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public int getPassengersPerFlight() throws RemoteException;

    /**
     * Obtém o número de lugares do autocarro de transferência.
     *
     * @return Número de lugares do autocarro de transferência.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public int getBusSeats() throws RemoteException;

    /**
     * Obtém o número máximo de malas de cada passageiro.
     *
     * @return Número máximo de malas de cada passageiro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public int getLuggage() throws RemoteException;
}
