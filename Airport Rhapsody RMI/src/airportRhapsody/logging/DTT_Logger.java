package airportRhapsody.logging;

import airportRhapsody.struct.Driver_State;
import airportRhapsody.struct.Passenger_State;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Acções de relatório provenientes da zona de transferência do terminal de
 * embarque.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 2.0
 * @since 17-03-2014
 */
public interface DTT_Logger extends RemotelyCloseable {

    /**
     * Muda o estado do ciclo de vida do motorista, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do motorista.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro
     * de logging.
     */
    public void setDriverState(Driver_State stat, VectorClock extClk) throws RemoteException, IOException;

    /**
     * Um passageiro sai do autocarro, libertando o seu lugar.
     *
     * @param id número de identificação do passageiro que saíu no autocarro.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void revokePassengerHerBusSeat(Integer id) throws RemoteException;

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro
     * de logging.
     */
    public void setPassengerState(Passenger_State st, Integer id, VectorClock extClk) throws RemoteException, IOException;
}
