package airportRhapsody.logging;

/**
 * Interface do monitor de reportagem dos eventos dos voos durante um dia de 
 * trabalho.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 */
public interface Logger_Interface extends ATE_Logger, ATT_Logger, DTE_Logger,
        DTT_Logger, DZ_Logger, LCP_Logger, Control_Logger {
}
