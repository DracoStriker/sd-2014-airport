package airportRhapsody.mutex;

import airportRhapsody.logging.DTT_Logger;
import airportRhapsody.protocol.Return;
import airportRhapsody.registry.Register;
import airportRhapsody.struct.Client;
import airportRhapsody.struct.Driver_State;
import airportRhapsody.struct.Passenger_State;
import airportRhapsody.synchronization.VectorClock;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Zona de transferência para o terminal de embarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Departure_Transfer_Terminal implements Departure_Transfer_Terminal_Interface {

    /**
     * Condição para indicar que o autocarro chegou e estacionou.
     */
    private boolean theBusHasArrived;

    /**
     * Número de passageiros que ainda estão dentro do autocarro.
     */
    private int nPassengersOnTheBus;

    /**
     * O repositório geral de informação.
     */
    private final DTT_Logger logger;

    /**
     * Ponto de sincronização do monitor.
     */
    private final Object lock;

    /**
     * Indica se ainda há passageiros em execução.
     */
    private boolean passengerKeepAlive;

    /**
     * Indica se o bagageiro ainda está em execução.
     */
    private boolean porterKeepAlive;

    /**
     * Indica se o motorista ainda está em execução.
     */
    private boolean driverKeepAlive;

    /**
     * Proxy do servidor de registos.
     */
    private final Register register;

    /**
     * Relógio vectorial do servidor.
     */
    private final VectorClock clk;
    
    /**
     * Inicializa este monitor.
     *
     * @param logger O repositório geral de informação.
     * @param register proxy do servidor de rregistos.
     * @param nPassengers número de passageiros por voo.
     */
    public Departure_Transfer_Terminal(DTT_Logger logger, Register register, int nPassengers) {
        this.logger = logger;
        this.register = register;
        this.clk=new VectorClock(nPassengers);
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
        theBusHasArrived = false;
        lock = new Object();
    }

    /**
     * O passageiro sai do autocarro.
     *
     * @param id número de identificação do passageiro que vai sair do
     * autocarro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return leaveTheBus(Integer id, VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        synchronized (lock) {
            while (!theBusHasArrived) { // o passageiro espera que o autocarro estacione
                try {
                    lock.wait();
                } catch (InterruptedException ex) {
                }
            }
            if (--nPassengersOnTheBus == 0) { // o último passageiro avisa o motorista que já sairam todos
                lock.notify();
            }
            System.out.println("Passenger " + id + " left the bus.");
            try {
                logger.revokePassengerHerBusSeat(id);
                logger.setPassengerState(
                        Passenger_State.AT_THE_DEPARTURE_TRANSFER_TERMINAL, id, clk);
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
        return new Return(new VectorClock(clk));
    }

    /**
     * O autocarro estaciona à entrada do terminal de embarque do aeroporto e
     * deixa os passageiros saírem.
     *
     * @param nPassengersOnTheBus número de passageiros no autocarro quando este
     * estacionou.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return parkTheBusAndLetPassOff(Integer nPassengersOnTheBus, VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        synchronized (lock) {
            this.nPassengersOnTheBus = nPassengersOnTheBus;
            theBusHasArrived = true;
            System.out.println("The bus parked at the Departure Transfer "
                    + "Terminal with " + nPassengersOnTheBus
                    + " passenger(s).");
            try {
                logger.setDriverState(
                        Driver_State.PARKING_AT_THE_DEPARTURE_TERMINAL, clk);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            lock.notifyAll(); // o motorista avisa os passageiros que já podem sair
            while (this.nPassengersOnTheBus > 0) { // o motorista espera que todos os passageiros saiam
                try {
                    lock.wait();
                } catch (InterruptedException ex) {
                }
            }
            System.out.println("All passengers have left the bus.");
        }
        return new Return(new VectorClock(clk));
    }

    /**
     * O motorista do autocarro regressa ao terminal de desembarque.
     * 
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public Return goToArrivalTerminal(VectorClock extClk) throws RemoteException {
        clk.update(extClk);
        synchronized (lock) {
            theBusHasArrived = false;
            System.out.println(
                    "The bus left to the Arrival Transfer Terminal.");
            try {
                logger.setDriverState(Driver_State.DRIVING_BACKWARD, clk);
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
        return new Return(new VectorClock(clk));
    }

    /**
     * Retira o registo deste objecto remoto do servidor de registos e termina
     * a execução do processo que instanciou este objecto.
     *
     * @param client Tipo de cliente que está a terminar o servidor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    @Override
    public synchronized void remoteClose(Client client) throws RemoteException {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            try {
                register.unbind("DTT");
            } catch (NotBoundException ex) {
                System.err.println(ex);
            }
            UnicastRemoteObject.unexportObject(this, true);
            System.out.println("\nserver is closing...");
        }
    }

}
