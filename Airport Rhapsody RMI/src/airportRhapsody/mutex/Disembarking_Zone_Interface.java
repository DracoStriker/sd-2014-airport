package airportRhapsody.mutex;

/**
 * Interface do monitor que implementa as acções do passageiro e do bagageiro 
 * na zona de desembarque, onde os passageiros saem do avião.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface Disembarking_Zone_Interface extends Passenger_DZ, Porter_DZ {
}
