package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;

/**
 * Acções do motorista na zona de transferência para o terminal de embarque do
 * aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Driver_ATT extends RemotelyCloseable {

    /**
     * O motorista verifica se já transportou todos os passageiros em trânsito e
     * caso sim termina o seu dia de trabalho.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return se todos os passageiros em trânsito já foram transportados para o
     * terminal de embarque e o relógio vectorial do servidor remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return hasDaysWorkEnded(VectorClock extClk) throws RemoteException;

    /**
     * O motorista anuncia que os passageiros podem entrar no autocarro, e
     * controla a entrada dos passageiros um a um até que não existam mais
     * lugares livres mas caso chegue a hora de partida e há pelo menos um
     * passageiro no autocarro este parte ainda com lugares livres.
     * 
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return announcingBusBoarding(VectorClock extClk) throws RemoteException;

    /**
     * Após os passageiros terem embarcado no autocarro, o motorista parte para
     * o terminal de embarque.
     *
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return o número de passageiros que embarcaram no autocarro e o relógio
     * vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return goToDepartureTerminal(VectorClock extClk) throws RemoteException;

    /**
     * O motorista estaciona o autocarro na zona de transferência do terminal de
     * desembarque após ter regressado do terminal de embarque.
     * 
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return parkTheBus(VectorClock extClk) throws RemoteException;

}
