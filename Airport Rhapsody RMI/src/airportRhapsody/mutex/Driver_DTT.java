package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;

/**
 * Acções do motorista na zona de transferência para o terminal de desembarque
 * do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Driver_DTT extends RemotelyCloseable {
    
    /**
     * O autocarro estaciona à entrada do terminal de embarque do aeroporto e
     * deixa os passageiros sairem.
     *
     * @param nPassengersOnTheBus número de passageiros no autocarro quando este
     * estacionou.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return parkTheBusAndLetPassOff(Integer nPassengersOnTheBus, VectorClock extClk) throws RemoteException;
    
    /**
     * O motorista do autocarro regressa ao terminal de desembarque.
     * 
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return goToArrivalTerminal(VectorClock extClk) throws RemoteException;
}
