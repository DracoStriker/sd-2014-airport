package airportRhapsody.mutex;

/**
 * Interface do monitor que implementa as acções do passageiro e bagageiro na 
 * zona de recolha de bagagens.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface Luggage_Collection_Point_Interface extends Passenger_LCP, Porter_LCP {
}
