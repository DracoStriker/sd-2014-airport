package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;

/**
 * Acções do passageiro na zona de transferência para o terminal de embarque do
 * aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_ATT extends RemotelyCloseable {

    /**
     * Inicializa o estado do voo actual.
     *
     * @param nFlight número do voo actual.
     * @param nPassengersWaitingForADrive número de passageiros em trânsito do
     * voo actual. 
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void startFlight(Integer nFlight,
            Integer nPassengersWaitingForADrive) throws RemoteException;
    
    /**
     * Os passageiros colocam-se na fila de espera do autocarro pela sua ordem
     * de chegada.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return takeABus(Integer id, VectorClock extClk) throws RemoteException;

    /**
     * Após o motorista anunciar o embarque dos passageiros, cada passageiro
     * embarca no autocarro conforme a ordem na fila de espera.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return enterTheBus(Integer id, VectorClock extClk) throws RemoteException;

}
