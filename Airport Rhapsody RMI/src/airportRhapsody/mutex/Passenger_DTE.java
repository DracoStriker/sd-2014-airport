package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;

/**
 * Acções do passageiro no terminal de desembarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_DTE extends RemotelyCloseable {

    /**
     * O passageiro embarca no avião.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * avião.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return relógio vectorial do objecto remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return prepareNextLeg(Integer id, VectorClock extClk) throws RemoteException;

}
