package airportRhapsody.mutex;

import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.util.RemotelyCloseable;
import java.rmi.RemoteException;

/**
 * Interface das acções do bagageiro na zona de recolha de bagagens.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_LCP extends RemotelyCloseable {

    /**
     * O passageiro recolhe uma mala da passadeira rolante de recolha.
     *
     * @param id Identificação do passageiro a recolher a mala.
     * @param extClk relógio de sincronização vectorial do cliente para 
     * actualizar o relógio vectorial do objecto remoto.
     * @return Mala recolhida ou null (caso não haja nenhuma mala a recolher) e
     * o relógio vectorial do servidor remoto.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public Return goCollectABag(Integer id, VectorClock extClk) throws RemoteException;

    /**
     * Redefine o valor da condição de espera para recolha de bagagens por parte
     * dos passageiros.
     *
     * @param hasMoreBags Condição de espera para recolha de bagagens por parte
     * dos passageiros, indicando se há mais sacos para recolher.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void setHasMoreBags(Boolean hasMoreBags) throws RemoteException;

}
