/**
 * Contém as entidades passivas da simulação da rapsódia do aeroporto e suas
 * respectivas interfaces.
 */
package airportRhapsody.mutex;
