package airportRhapsody.registry;

import airportRhapsody.util.RemotelyCloseable;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface de um proxy de um servidor de registos RMI.
 * 
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface Register extends RemotelyCloseable {
    
    /**
     * Regista um objecto com o nome fornecido.
     * 
     * @param name nome do registo.
     * @param obj objecto a registar.
     * @throws RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws AlreadyBoundException se o objecto com o nome definido já se 
     * encontra registado.
     */
    public void bind(String name, Remote obj) throws RemoteException, AlreadyBoundException;

    /**
     * Retira o registo de um objecto remoto com o nome fornecido.
     * 
     * @param name nome do registo.
     * @throws RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     * @throws NotBoundException caso um objecto com o nome fornecido não
     * se encontre registado.
     */
    public void unbind(String name) throws RemoteException, NotBoundException;

    /**
     * Regista um objecto com o nome fornecido.
     * 
     * @param name nome do registo.
     * @param obj objecto a registar.
     * @throws RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void rebind(String name, Remote obj) throws RemoteException;
}
