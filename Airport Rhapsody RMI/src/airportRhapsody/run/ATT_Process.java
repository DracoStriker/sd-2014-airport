package airportRhapsody.run;

import airportRhapsody.config.InitialSetup;
import airportRhapsody.logging.ATT_Logger;
import airportRhapsody.mutex.Arrival_Transfer_Terminal;
import airportRhapsody.mutex.Arrival_Transfer_Terminal_Interface;
import airportRhapsody.registry.Register;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Processo de simulação do monitor da zona de transferência para o terminal de desembarque.
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 */
public class ATT_Process {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    /**
     * Endereço IP do servidor de registos.
     */
    private String registryServerHostName;

    /**
     * Porto TCP do servidor de registos.
     */
    private int registryServerPort;

    /**
     * Porto TCP do servidor.
     */
    private int port;

    private ATT_Process() {
    }

    /**
     * @param args Argumentos de execução (endereço IP do servidor de registos, porto TCP do servidor de registos, porto TCP do servidor)
     */
    public static void main(String[] args) {
        ATT_Process attProcess = new ATT_Process();
        attProcess.init(args);
        attProcess.runProcess();
    }

    /**
     * Corre o servidor.
     */
    private void runProcess() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(registryServerHostName, registryServerPort);
            Register register = (Register) registry.lookup("Register");
            InitialSetup setup = (InitialSetup) registry.lookup("InitialSetup");
            ATT_Logger logger = (ATT_Logger) registry.lookup("Logger");
            Arrival_Transfer_Terminal att = new Arrival_Transfer_Terminal(setup.getBusSeats(), setup.getFlights(), logger, register, setup.getPassengersPerFlight());
            Arrival_Transfer_Terminal_Interface attProxy = (Arrival_Transfer_Terminal_Interface) UnicastRemoteObject.exportObject(att, port);
            register.rebind("ATT", attProxy);
        } catch (RemoteException | NotBoundException ex) {
            System.err.println(ex);
        }
        System.out.println("ATT Process is running...");
    }

    /**
     * Inicialização deste processo.
     *
     * @param args argumentos da linha de comandos.
     */
    private void init(String[] args) {
        if (args.length < 3) {
            System.out.println("java -cp Airport_Rhapsody_RMI.jar airportRhapsody.run.ATT_Process <registry server host name> <registry server port> <att port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
            port = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("arguments 2 and 3 must be a integer between [4000, 65535]");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nserver is closing...");
            }
        });
    }

}
