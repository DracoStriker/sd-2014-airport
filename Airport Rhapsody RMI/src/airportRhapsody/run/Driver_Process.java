package airportRhapsody.run;

import airportRhapsody.config.InitialSetup;
import airportRhapsody.logging.Control_Logger;
import airportRhapsody.mutex.Arrival_Terminal_Exit_Interface;
import airportRhapsody.mutex.Arrival_Transfer_Terminal_Interface;
import airportRhapsody.mutex.Departure_Terminal_Entrance_Interface;
import airportRhapsody.mutex.Departure_Transfer_Terminal_Interface;
import airportRhapsody.mutex.Disembarking_Zone_Interface;
import airportRhapsody.mutex.Luggage_Collection_Point_Interface;
import airportRhapsody.registry.Register;
import airportRhapsody.struct.Client;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.thread.Driver;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Simulação do bagageiro da Rapsódia no Aeroporto.
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 */
public class Driver_Process {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    /**
     * Endereço IP do servidor de registos.
     */
    private String registryServerHostName;

    /**
     * Porto TCP do servidor de registos.
     */
    private int registryServerPort;

    private Driver_Process() {
    }

    /**
     * @param args Argumentos de execução (endereço IP do servidor de registos, porto TCP do servidor de registos)
     */
    public static void main(String[] args) {
        Driver_Process loggerProcess = new Driver_Process();
        loggerProcess.init(args);
        loggerProcess.runProcess();
    }

    /**
     * Corre o servidor.
     */
    private void runProcess() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(registryServerHostName, registryServerPort);
            Register register = (Register) registry.lookup("Register");
            InitialSetup initialSetup = (InitialSetup) registry.lookup("InitialSetup");
            Arrival_Terminal_Exit_Interface ateProxy = (Arrival_Terminal_Exit_Interface) registry.lookup("ATE");
            Arrival_Transfer_Terminal_Interface attProxy = (Arrival_Transfer_Terminal_Interface) registry.lookup("ATT");
            Departure_Terminal_Entrance_Interface dteProxy = (Departure_Terminal_Entrance_Interface) registry.lookup("DTE");
            Departure_Transfer_Terminal_Interface dttProxy = (Departure_Transfer_Terminal_Interface) registry.lookup("DTT");
            Disembarking_Zone_Interface dzProxy = (Disembarking_Zone_Interface) registry.lookup("DZ");
            Luggage_Collection_Point_Interface lcpProxy = (Luggage_Collection_Point_Interface) registry.lookup("LCP");
            Control_Logger loggerProxy = (Control_Logger) registry.lookup("Logger");
            int N = initialSetup.getPassengersPerFlight();
            VectorClock clk = new VectorClock(N);
            Driver driver = new Driver(attProxy, dttProxy, clk);
            Thread driverThread = new Thread(driver);
            driverThread.start();
            try {
                driverThread.join();
            } catch (InterruptedException ex) {
            }
            System.out.println("Terminating ATE");
            ateProxy.remoteClose(Client.DRIVER);
            System.out.println("Terminating ATT");
            attProxy.remoteClose(Client.DRIVER);
            System.out.println("Terminating DTE");
            dteProxy.remoteClose(Client.DRIVER);
            System.out.println("Terminating DTT");
            dttProxy.remoteClose(Client.DRIVER);
            System.out.println("Terminating DZ");
            dzProxy.remoteClose(Client.DRIVER);
            System.out.println("Terminating LCP");
            lcpProxy.remoteClose(Client.DRIVER);
            System.out.println("Terminating Logger");
            loggerProxy.remoteClose(Client.DRIVER);
            System.out.println("Terminating GIR");
            initialSetup.remoteClose(Client.DRIVER);
            System.out.println("Terminating RegistryServer");
            register.remoteClose(Client.DRIVER);
        } catch (RemoteException | NotBoundException ex) {
            System.err.println(ex);
        }
        System.out.println("Driver Process terminating.");
    }

    /**
     * Inicialização deste processo.
     *
     * @param args argumentos da linha de comandos.
     */
    private void init(String[] args) {
        if (args.length < 2) {
            System.out.println("java -cp Airport_Rhapsody_RMI.jar airportRhapsody.run.Driver_Process <registry server host name> <registry server port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            System.out.println("argument 2 must be a integer between [4000, 65535]");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nserver is closing...");
            }
        });
    }

}
