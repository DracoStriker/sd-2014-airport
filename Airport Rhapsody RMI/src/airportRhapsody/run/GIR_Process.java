package airportRhapsody.run;

import airportRhapsody.config.InitialSetup;
import airportRhapsody.config.SimulationSetup;
import airportRhapsody.parser.XMLReader;
import airportRhapsody.registry.Register;
import airportRhapsody.struct.Bag;
import airportRhapsody.struct.Passenger_Status;
import airportRhapsody.struct.Simulation;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Processo de execução do distribuidor de parâmetros da simulação.
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 */
public class GIR_Process {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    /**
     * Nº de voos da simulação.
     */
    private static int K;

    /**
     * Número de passageiros por voo.
     */
    private static int N;

    /**
     * Nº de lugares do autocarro de transferência.
     */
    private static int T;

    /**
     * Nº máximo de malas do passageiro.
     */
    private static int M;

    /**
     * Gerador de números aleatórios
     */
    private static Random r;

    /**
     * Valores de estado interno possiveis para os passageiros.
     */
    private static Passenger_Status[] possibleStatus;

    /**
     * Lista de malas do avião.
     */
    private static ArrayList<Bag> planeBags;

    /**
     * Lista de estados internos dos passageiros.
     */
    private static ArrayList<Passenger_Status> passengers;

    /**
     * Lista do numero de malas de cada passageiro.
     */
    private static ArrayList<Integer> passengersBagNumber;

    /**
     * Número total de passageiros cujo destino não é final.
     */
    private static int passengersInTransit;

    /**
     * Lista de simulações com respectivas variáveis.
     */
    private static ArrayList<Simulation> simulations;

    /**
     * Endereço IP do servidor de registos.
     */
    private static String registryServerHostName;

    /**
     * Porto TCP do servidor de registos.
     */
    private static int registryServerPort;

    /**
     * Porto TCP do servidor.
     */
    private static int port;

    /**
     * Proxy do RMI Registry.
     */
    private static Register register;

    private GIR_Process() {
    }

    /**
     * 
     * @param args Argumentos de execução (endereço IP do servidor de registos, porto TCP do servidor de registos, porto TCP do servidor)
     */
    public static void main(String... args) {
        System.out.println("General Information Repository Process");

        init(args);
        
        //read and parse the file
        XMLReader config = new XMLReader("default.xml");
        K = config.getFlights();
        N = config.getPassengersPerFlight();
        T = config.getBusSeats();
        M = config.getBags();
        r = new Random();
        simulations = new ArrayList<>();

        for (int k = 0; k < K; k++) {

            possibleStatus = Passenger_Status.values(); //valores de estado interno possiveis para os passageiros
            planeBags = new ArrayList<>(); //lista de malas do avião
            passengers = new ArrayList<>(); //lista de estados internos dos passageiros
            passengersBagNumber = new ArrayList<>(); //lista do numero de malas de cada passageiro
            passengersInTransit = 0; //numero total de passageiros cujo destino não é final

            //criar lista de malas do avião e lista do número de malas para cada passageiro
            for (int i = 0; i < N; i++) {
                int bagsPerPassenger = 0; //numero de malas do passageiro
                double isGoingToMissBags = r.nextDouble(); //gerar valor de perda de malas
                int bagsToAdd; //numero de malas do passageiro a adicionar ao avião

                //gerar passageiros para o voo em simulação
                passengers.add(possibleStatus[r.nextInt(possibleStatus.length)]); //adicionar passageiro com estado aleatorio
                if (passengers.get(passengers.size() - 1) == Passenger_Status.IN_TRANSIT) { //se o destino do passageiro nao for o final
                    passengersInTransit++; //incrementar o numero total de passageiros cujo destino não é o final
                }

                if (passengers.get(i) == Passenger_Status.FINAL_DESTINATION_NO_BAGS) { //se o passageiro não tiver malas
                    passengersBagNumber.add(0);                                        //adiciona 0 a lista
                    continue; //passa ao próximo passageiro
                }
                //gerar o número de malas para o passageiro (sem ser 0 no caso do estado interno ser destino final com malas, porque o passageiro tem de ter pelo menos 1 mala neste estado interno)
                while (bagsPerPassenger == 0 && passengers.get(i) == Passenger_Status.FINAL_DESTINATION_WITH_BAGS) {
                    bagsPerPassenger = r.nextInt(M + 1);
                }
                passengersBagNumber.add(bagsPerPassenger); //adiciona o numero de bagagens a lista

                bagsToAdd = bagsPerPassenger;
                if (isGoingToMissBags > 0.8) { //se o valor de perda de malas e superior a 0.8, entao perde uma mala (probabilidade de ~20%)
                    bagsToAdd--;
                }
                //adicionar o numero de malas decidido para o aviao
                for (int j = 0; j < bagsToAdd; j++) {
                    planeBags.add(new Bag(i, passengers.get(i)));
                }
            }
            //colocar malas de modo aleatório
            Collections.shuffle(planeBags);

            //adicionar os dados calculados à lista de simulações
            simulations.add(new Simulation(planeBags, passengers, passengersBagNumber, passengersInTransit));

        }
        runProcess();
    }

    /**
     * Constrói as configurações da simulação para distribuir a todos os monitores e entidades.
     * @return as configurações compiladas
     * @throws IOException no caso de ocorrer um erro a nível de ficheiro
     */
    private static SimulationSetup buildConfigs() throws IOException {
        SimulationSetup setup = new SimulationSetup(simulations, K, N, T, M, register);
        return setup;
    }

    private static void runProcess() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(registryServerHostName, registryServerPort);
            register = (Register) registry.lookup("Register");
            SimulationSetup setup = buildConfigs();
            InitialSetup initialSetup = (InitialSetup) UnicastRemoteObject.exportObject(setup, port);
            register.rebind("InitialSetup", initialSetup);
        } catch (RemoteException | NotBoundException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("GIR Process is running...");
    }
    
    /**
     * Inicialização deste processo.
     *
     * @param args argumentos da linha de comandos.
     */
    private static void init(String[] args) {
        if (args.length < 3) {
            System.out.println("java -cp Airport_Rhapsody_RMI.jar airportRhapsody.run.GIR_Process <registry server host name> <registry server port> <gir port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
            port = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("arguments 2 and 3 must be a integer between [4000, 65535]");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nserver is closing...");
            }
        });

    }
}
