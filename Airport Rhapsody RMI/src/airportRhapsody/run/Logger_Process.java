package airportRhapsody.run;

import airportRhapsody.config.InitialSetup;
import airportRhapsody.logging.Logger;
import airportRhapsody.logging.Logger_Interface;
import airportRhapsody.registry.Register;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Processo de simulação do monitor de repositório geral de informação (logger).
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 */
public class Logger_Process {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    /**
     * Endereço IP do servidor de registos.
     */
    private String registryServerHostName;

    /**
     * Porto TCP do servidor de registos.
     */
    private int registryServerPort;

    /**
     * Porto TCP do servidor.
     */
    private int port;

    private Logger_Process() {
    }

    /**
     * @param args Argumentos de execução (endereço IP do servidor de registos, porto TCP do servidor de registos, porto TCP do servidor)
     */
    public static void main(String[] args) {
        Logger_Process loggerProcess = new Logger_Process();
        loggerProcess.init(args);
        loggerProcess.runProcess();
    }

    /**
     * Corre o servidor.
     */
    private void runProcess() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(registryServerHostName, registryServerPort);
            Register register = (Register) registry.lookup("Register");
            InitialSetup setup = (InitialSetup) registry.lookup("InitialSetup");
            Logger logger = new Logger(setup.getFlights(), setup.getPassengersPerFlight(), setup.getBusSeats(), setup.getLuggage(), register);
            try {
                logger.open("airport_rhapsody_log.txt");
            } catch (IOException ex) {
                System.err.println(ex);
                System.exit(0);
            }
            Logger_Interface loggerProxy = (Logger_Interface) UnicastRemoteObject.exportObject(logger, port);
            register.rebind("Logger", loggerProxy);
        } catch (RemoteException | NotBoundException ex) {
            System.err.println(ex);
        }
        System.out.println("Logger Process is running...");
    }

    
    /**
     * Inicialização deste processo.
     *
     * @param args argumentos da linha de comandos.
     */
    private void init(String[] args) {
        if (args.length < 3) {
            System.out.println("java -cp Airport_Rhapsody_RMI.jar airportRhapsody.run.Logger_Process <registry server host name> <registry server port> <logger port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
            port = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("arguments 2 and 3 must be a integer between [4000, 65535]");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nserver is closing...");
            }
        });
    }

}
