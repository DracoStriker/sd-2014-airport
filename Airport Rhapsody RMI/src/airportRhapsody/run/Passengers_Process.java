package airportRhapsody.run;

import airportRhapsody.config.InitialSetup;
import airportRhapsody.logging.Control_Logger;
import airportRhapsody.mutex.Arrival_Terminal_Exit_Interface;
import airportRhapsody.mutex.Arrival_Transfer_Terminal_Interface;
import airportRhapsody.mutex.Departure_Terminal_Entrance_Interface;
import airportRhapsody.mutex.Departure_Transfer_Terminal_Interface;
import airportRhapsody.mutex.Disembarking_Zone_Interface;
import airportRhapsody.mutex.Luggage_Collection_Point_Interface;
import airportRhapsody.registry.Register;
import airportRhapsody.struct.Bag;
import airportRhapsody.struct.Client;
import airportRhapsody.struct.Passenger_Status;
import airportRhapsody.struct.Simulation;
import airportRhapsody.synchronization.VectorClock;
import airportRhapsody.thread.Passenger;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

/**
 * Simulação dos vários passageiros da Rapsódia no Aeroporto.
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 */
public class Passengers_Process {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    /**
     * Endereço IP do servidor de registos.
     */
    private String registryServerHostName;

    /**
     * Porto TCP do servidor de registos.
     */
    private int registryServerPort;

    private Passengers_Process() {
    }

    /**
     * @param args Argumentos de execução (endereço IP do servidor de registos, porto TCP do servidor de registos)
     */
    public static void main(String[] args) {
        Passengers_Process loggerProcess = new Passengers_Process();
        loggerProcess.init(args);
        loggerProcess.runProcess();
    }

    /**
     * Corre o servidor.
     */
    private void runProcess() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(registryServerHostName, registryServerPort);
            Register register = (Register) registry.lookup("Register");
            InitialSetup initialSetup = (InitialSetup) registry.lookup("InitialSetup");
            Arrival_Terminal_Exit_Interface ateProxy = (Arrival_Terminal_Exit_Interface) registry.lookup("ATE");
            Arrival_Transfer_Terminal_Interface attProxy = (Arrival_Transfer_Terminal_Interface) registry.lookup("ATT");
            Departure_Terminal_Entrance_Interface dteProxy = (Departure_Terminal_Entrance_Interface) registry.lookup("DTE");
            Departure_Transfer_Terminal_Interface dttProxy = (Departure_Transfer_Terminal_Interface) registry.lookup("DTT");
            Disembarking_Zone_Interface dzProxy = (Disembarking_Zone_Interface) registry.lookup("DZ");
            Luggage_Collection_Point_Interface lcpProxy = (Luggage_Collection_Point_Interface) registry.lookup("LCP");
            Control_Logger loggerProxy = (Control_Logger) registry.lookup("Logger");

            int K = initialSetup.getFlights();
            int N = initialSetup.getPassengersPerFlight();
            ArrayList<Simulation> simulations = initialSetup.getSimulations();
            System.out.println("Passengers Process is running...");
            VectorClock[] clk = new VectorClock[N];
            for (int i = 0; i < N; i++) {
                clk[i] = new VectorClock(N);
            }
            for (int k = 0; k < K; k++) {
                Simulation simulation = simulations.get(k);
                loggerProxy.startFlight();
                loggerProxy.setFlightNumber(k + 1);
                int nPassengersWaitingForADrive
                        = simulation.getPassengersInTransit();
                attProxy.startFlight(k + 1, nPassengersWaitingForADrive);
                ArrayList<Integer> passengerBags
                        = simulation.getPassengersBagNumber();
                ArrayList<Passenger_Status> passengersStatus
                        = simulation.getPassengers();
                ArrayList<Bag> planeBags = simulation.getPlaneBags();
                for (int i = 0; i < N; i++) {
                    if (passengersStatus.get(i)
                            == Passenger_Status.FINAL_DESTINATION_NO_BAGS) {
                        loggerProxy.setPassengerStatus(passengersStatus.get(i), i);
                        continue;
                    }
                    loggerProxy.setLuggageCarriedAtTheStartOfHerJourney(passengerBags.get(i), i);
                    loggerProxy.setPassengerStatus(passengersStatus.get(i), i);
                }
                loggerProxy.setLuggageOnThePlane(planeBags.size());
                loggerProxy.save();
                Passenger[] passengers = new Passenger[N];
                for (int i = 0; i < N; i++) {
                    passengers[i] = new Passenger(i, passengerBags.get(i), ateProxy, attProxy,
                            dteProxy, dttProxy, dzProxy, lcpProxy, clk[i]);
                }
                dzProxy.setLists(passengersStatus, planeBags);

                Thread[] passengerThreads = new Thread[N];
                for (int i = 0; i < N; i++) {
                    passengerThreads[i] = new Thread(passengers[i]);
                }
                for (int i = 0; i < N; i++) {
                    passengerThreads[i].start();
                }
                for (int i = 0; i < N; i++) {
                    try {
                        passengerThreads[i].join();
                    } catch (InterruptedException ex) {
                    }
                }
                lcpProxy.setHasMoreBags(true);
                loggerProxy.emptyStoreroom();
            }
            //terminate servers
            System.out.println("Terminating ATE");
            ateProxy.remoteClose(Client.PASSENGER);
            System.out.println("Terminating ATT");
            attProxy.remoteClose(Client.PASSENGER);
            System.out.println("Terminating DTE");
            dteProxy.remoteClose(Client.PASSENGER);
            System.out.println("Terminating DTT");
            dttProxy.remoteClose(Client.PASSENGER);
            System.out.println("Terminating DZ");
            dzProxy.remoteClose(Client.PASSENGER);
            System.out.println("Terminating LCP");
            lcpProxy.remoteClose(Client.PASSENGER);
            System.out.println("Terminating Logger");
            loggerProxy.remoteClose(Client.PASSENGER);
            System.out.println("Terminating GIR");
            initialSetup.remoteClose(Client.PASSENGER);
            System.out.println("Terminating RegistryServer");
            register.remoteClose(Client.PASSENGER);

        } catch (RemoteException | NotBoundException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("Passengers Process terminating.");
    }

    /**
     * Inicialização deste processo.
     *
     * @param args argumentos da linha de comandos.
     */
    private void init(String[] args) {
        if (args.length < 2) {
            System.out.println("java -cp Airport_Rhapsody_RMI.jar airportRhapsody.run.Passengers_Process <registry server host name> <registry server port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            System.out.println("arguments 2 must be a integer between [4000, 65535]");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nserver is closing...");
            }
        });
    }

}
