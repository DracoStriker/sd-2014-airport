package airportRhapsody.struct;

import java.io.Serializable;

/**
 * Estrutura que representa uma mala.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Bag implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * Número de identificação do dono da mala.
     */
    private final int id;
    
    /**
     * Tipo de passageiro do dono da mala.
     */
    private final Passenger_Status status;

    /**
     * Inicializa a estrutura da mala, recebendo a identificação do proprietário
     * da mala e o estado interno do mesmo.
     *
     * @param id Identificação do passageiro proprietário da mala.
     * @param status Estado interno do passageiro proprietário da mala.
     */
    public Bag(int id, Passenger_Status status) {
        this.id = id;
        this.status = status;
    }

    /**
     * Retorna a identificação do proprietário da mala.
     *
     * @return Identificação do passageiro proprietário da mala.
     */
    public int getId() {
        return id;
    }

    /**
     * Retorna o estado interno do proprietário da mala.
     *
     * @return Estado interno do passageiro proprietário da mala.
     */
    public Passenger_Status getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Bag<" + id + ">";
    }
}
