package airportRhapsody.struct;

import java.io.Serializable;

/**
 * Estados possíveis do motorista.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public enum Driver_State implements Serializable {

    PARKING_AT_THE_ARRIVAL_TERMINAL("AT"), DRIVING_FORWARD("DF"),
    PARKING_AT_THE_DEPARTURE_TERMINAL("DT"), DRIVING_BACKWARD("DB");
    
    private static final long serialVersionUID = 1L;

    /**
     * String de representação deste tipo de dados enumerado.
     */
    private final String state;

    /**
     * Construtor deste tipo enuemrado fornecendo a sua representação em String.
     * 
     * @param state representação em String deste tipo de dados enumerado.
     */
    private Driver_State(String state) {
        this.state = state;
    }
    
    @Override
    public String toString() {
        return state;
    }
}
