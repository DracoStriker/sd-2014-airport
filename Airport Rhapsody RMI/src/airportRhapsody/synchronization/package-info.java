/**
 * Providencia ferramentas de sincronização de relógios distribuidos para a
 * simulação da rapsódia no aeroporto usando relógios vectoriais.
 */
package airportRhapsody.synchronization;
