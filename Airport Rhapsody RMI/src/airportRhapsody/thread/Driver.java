package airportRhapsody.thread;

import airportRhapsody.mutex.Driver_ATT;
import airportRhapsody.mutex.Driver_DTT;
import airportRhapsody.protocol.Return;
import airportRhapsody.synchronization.VectorClock;
import java.rmi.RemoteException;
import java.util.Random;

/**
 * Thread que representa e a entidade Motorista.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Driver implements Runnable {

    /**
     * Monitor da zona de transferência para o terminal de desembarque do
     * aeroporto.
     */
    private final Driver_ATT attMon;

    /**
     * Monitor da zona de transferência para o terminal de embarque do
     * aeroporto.
     */
    private final Driver_DTT dttMon;

    /**
     * Relógio vectorial do processo.
     */
    private final VectorClock clk;

    /**
     * Inicializa a thread do motorista, recebendo a referência do monitor da
     * zona de transferência para o terminal de desembarque, a referência do
     * monitor da zona de transferência para o terminal de embarque e o relógio vectorial.
     *
     * @param attMon Monitor da zona de transferência para o terminal de
     * desembarque do aeroporto.
     * @param dttMon Monitor da zona de transferência para o terminal de
     * embarque do aeroporto.
     * @param clk relógio vectorial do processo.
     */
    public Driver(Driver_ATT attMon, Driver_DTT dttMon, VectorClock clk) {
        this.attMon = attMon;
        this.dttMon = dttMon;
        this.clk = clk;
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        Return ret;
        boolean workEnded;
        int nPassengersOnTheBus; //numero de passageiros presentes no autocarro
        Random r = new Random(); //gerador de numeros aleatorios
        try {
            //ciclo de vida do motorista
            while (true) {

                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = attMon.hasDaysWorkEnded(clk);
                workEnded = (boolean) ret.getObj();
                clk.update(ret.getVc());

                if (workEnded) {
                    break;
                }

                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = attMon.announcingBusBoarding(clk);
                clk.update(ret.getVc());

                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = attMon.goToDepartureTerminal(clk);
                nPassengersOnTheBus = (int) ret.getObj();
                clk.update(ret.getVc());

                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = dttMon.parkTheBusAndLetPassOff(nPassengersOnTheBus, clk);
                clk.update(ret.getVc());

                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = dttMon.goToArrivalTerminal(clk);
                clk.update(ret.getVc());

                Thread.sleep(r.nextInt(100));
                clk.incrDriverClk();
                ret = attMon.parkTheBus(clk);
                clk.update(ret.getVc());
            }
            System.out.println("Driver work has ended."); //print apenas para debug
        } catch (InterruptedException ex) {
        } catch (RemoteException ex) {
            System.err.println(ex);
        }
    }
}
