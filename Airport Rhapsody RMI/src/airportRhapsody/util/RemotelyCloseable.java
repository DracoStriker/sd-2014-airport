package airportRhapsody.util;

import airportRhapsody.struct.Client;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface que representa um objecto cujo processo pode ser terminado
 * remotamente.
 * 
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 14-05-2014
 */
public interface RemotelyCloseable extends Remote {
    
    /**
     * Operação de encerramento remoto.
     * 
     * @param client tipo de entidade activa que está a solicitar o encerramento
     * so servidor.
     * @throws java.rmi.RemoteException quando ocorre algum erro na invocação
     * deste método remoto.
     */
    public void remoteClose(Client client) throws RemoteException;
}
