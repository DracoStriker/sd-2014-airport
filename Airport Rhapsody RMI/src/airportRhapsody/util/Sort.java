package airportRhapsody.util;

import java.util.List;

/**
 * Algoritmos genéricos de ordenação.
 * 
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 27-05-2014
 */
public class Sort {

    /**
     * Implementação genérica de um bubble sort.
     * 
     * @param <T> tipo de objecto a ordenar.
     * @param list lista de objectos a ordenar.
     */
    public static <T extends Comparable<? super T>> void bubble(List<T> list) {
        for (int i = (list.size() - 1); i >= 0; i--) {
            for (int j = 1; j <= i; j++) {
                if (list.get(j - 1).compareTo(list.get(j)) > 0) {
                    T elem = list.get(j - 1);
                    list.set(j - 1, list.get(j));
                    list.set(j, elem);
                }
            }
        }
    }
}
