package logging;

import java.io.IOException;
import struct.Passenger_State;

/**
 * Acções de relatório provenientes da saída do terminal de desembarque.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 17-03-2014
 */
public interface ATE_Logger {

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setPassengerState(Passenger_State st, Integer id) throws IOException;

}
