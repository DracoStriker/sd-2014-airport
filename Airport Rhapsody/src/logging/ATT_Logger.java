package logging;

import java.io.IOException;
import struct.Driver_State;
import struct.Passenger_State;

/**
 * Acções de relatório provenientes da zona de transferência do terminal de
 * desembarque.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 17-03-2014
 */
public interface ATT_Logger {

    /**
     * Muda o estado do ciclo de vida do motorista, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do motorista.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setDriverState(Driver_State stat) throws IOException;

    /**
     * Um passageiro entra para a última posição livre da fila de espera do
     * autocarro.
     *
     * @param id número de identificação do passageiro que entrou na fila de
     * espera.
     */
    public void addPassengerToTheWaitingQueue(Integer id);

    /**
     * Um passageiro embarca no autocarro.
     *
     * @param id número de identificação do passageiro que entrou no autocarro.
     */
    public void attributePassengerHerBusSeat(Integer id);

    /**
     * O primeiro passageiro na fila de espera dai desta para entrar no
     * autocarro.
     */
    public void removePassengerFromTheWaitingQueue();

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setPassengerState(Passenger_State st, Integer id) throws IOException;

}
