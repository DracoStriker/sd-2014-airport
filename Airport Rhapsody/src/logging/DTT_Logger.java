package logging;

import java.io.IOException;
import struct.Driver_State;
import struct.Passenger_State;

/**
 * Acções de relatório provenientes da zona de transferência do terminal de
 * embarque.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 17-03-2014
 */
public interface DTT_Logger {

    /**
     * Muda o estado do ciclo de vida do motorista, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do motorista.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setDriverState(Driver_State stat) throws IOException;

    /**
     * Um passageiro sai do autocarro, libertando o seu lugar.
     *
     * @param id número de identificação do passageiro que saíu no autocarro.
     */
    public void revokePassengerHerBusSeat(Integer id);

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setPassengerState(Passenger_State st, Integer id) throws IOException;

}
