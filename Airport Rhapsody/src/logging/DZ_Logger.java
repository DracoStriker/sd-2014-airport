package logging;

import java.io.IOException;
import struct.Passenger_State;
import struct.Porter_State;

/**
 * Acções de relatório provenientes da zona de desembarque.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 17-03-2014
 */
public interface DZ_Logger {

    /**
     * O bagageiro retira uma peça de bagagem do avião.
     */
    public void restoreAPieceOfLuggageFromThePlane();

    /**
     * Muda o estado do ciclo de vida do bagageiro, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do bagageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setPorterState(Porter_State stat) throws IOException;

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setPassengerState(Passenger_State st, Integer id) throws IOException;

}
