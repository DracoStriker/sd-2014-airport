package logging;

import struct.Driver_State;
import struct.Passenger_State;
import struct.Passenger_Status;
import struct.Porter_State;

/**
 * Todo o estado de um voo.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 16-03-2014
 */
class FullStateOfTheProblem {

    /**
     * Estado do avião.
     */
    final Plane plane;

    /**
     * Estado do bagageiro.
     */
    final Porter porter;

    /**
     * Estado do motorista.
     */
    final Driver driver;

    /**
     * Estado dos passageiros.
     */
    final Passenger[] passengers;

    /**
     * Inicializa o estado do voo com o número de passageiros e número de
     * lugares no autocarro.
     *
     * @param N número de passageiros.
     * @param T número de lugares no autocarro.
     */
    FullStateOfTheProblem(int N, int T) {
        plane = new Plane();
        porter = new Porter();
        driver = new Driver();
        driver.q = new int[N];
        driver.s = new int[T];
        passengers = new Passenger[N];
        for (int i = 0; i < N; i++) {
            passengers[i] = new Passenger();
        }
    }

    @Override
    public String toString() {
        String str = plane.toString() + porter.toString() + driver.toString()
                + "\n";
        for (Passenger p : passengers) {
            str += p.toString();
        }
        return str + "\n";
    }

    /**
     * Todo o estado do avião durante a simulação de um voo.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 16-03-2014
     */
    class Plane {

        /**
         * Número do voo.
         */
        int fn;

        /**
         * Quantidade de bagagem na aterragem do avião.
         */
        int bn;

        @Override
        public String toString() {
            return " " + fn + "  " + bn + " ";
        }
    }

    /**
     * Todo o estado do bagageiro durante a simulação de um voo.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 16-03-2014
     */
    class Porter {

        /**
         * Estado do bagageiro no seu ciclo de vida.
         */
        Porter_State stat;

        /**
         * Quantidade de bagagens na correia transportadora.
         */
        int cb;

        /**
         * Quantidade de bagagens dos passageiros em trânsito armazenada no
         * armazem.
         */
        int sr;

        @Override
        public String toString() {
            return " " + stat + "  " + cb + "  " + sr + " ";
        }
    }

    /**
     * Todo o estado do motorista durante a simulação de um voo.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 16-03-2014
     */
    class Driver {

        /**
         * Estado do motorista no seu ciclo de vida.
         */
        Driver_State stat;

        /**
         * Estado da fila de espera do autocarro.
         */
        int[] q;

        /**
         * Estado dos lugares do autocarro.
         */
        int[] s;

        @Override
        public String toString() {
            String str = "   " + stat + "    ";
            for (int i : q) {
                if (i == -1) {
                    str += "-  ";
                } else {
                    str += i + "  ";
                }
            }
            for (int i : s) {
                if (i == -1) {
                    str += " - ";
                } else {
                    str += " " + i + " ";
                }
            }
            return str;
        }
    }

    /**
     * Todo o estado de um passageiro durante a simulação de um voo.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 16-03-2014
     */
    class Passenger {

        /**
         * Estado do passageiro no seu ciclo de vida.
         */
        Passenger_State st;

        /**
         * Condição do passageiro, se está em trânsito ou este aeroporto é o seu
         * destino final.
         */
        Passenger_Status si;

        /**
         * Quantidade de bagagem que o passageiro transportava ao início da
         * viagem.
         */
        int nr;

        /**
         * Quantidade de bagagem que o passageiro recolheu da correia de
         * transporte.
         */
        int na;

        @Override
        public String toString() {
            return st + " " + si + "  " + nr + "   " + na + "  ";
        }
    }
}
