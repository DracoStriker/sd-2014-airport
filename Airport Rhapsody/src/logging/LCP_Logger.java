package logging;

import java.io.IOException;
import struct.Passenger_State;
import struct.Porter_State;

/**
 * Acções de relatório provenientes da zona de recolha de bagagem.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 17-03-2014
 */
public interface LCP_Logger {

    /**
     * Muda o estado do ciclo de vida do bagageiro, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do bagageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setPorterState(Porter_State stat) throws IOException;

    /**
     * O bagageiro adiciona uma peça de bagagem à correia de transporte.
     */
    public void addLuggageToTheConveyorBelt();

    /**
     * O passageiro recolhe uma peça de bagagem da correia de transporte.
     *
     * @param id número de identificação do passageiro.
     */
    public void restoreLugageFromTheConveyorBelt(Integer id);

    /**
     * O bagageiro adiciona uma peça de bagagem no armazém.
     */
    public void addLuggageToTheStoreroom();

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    public void setPassengerState(Passenger_State st, Integer id) throws IOException;

}
