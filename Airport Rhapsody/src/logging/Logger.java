package logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import protocol.InitialSetup;
import struct.Driver_State;
import struct.Passenger_State;
import struct.Passenger_Status;
import struct.Porter_State;

/**
 * Monitor de reportagem dos eventos dos voos durante um dia de trabalho.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 16-03-2014
 */
public class Logger implements ATE_Logger, ATT_Logger, DTE_Logger, DTT_Logger,
        DZ_Logger, LCP_Logger {

    /**
     * Número total de voos.
     */
    public final int K;

    /**
     * Número total de passageiro em cada voo.
     */
    public final int N;

    /**
     * Número de lugares no autocarro de transferência.
     */
    public final int T;

    /**
     * Número máximo de malas para o passageiro.
     */
    public final int M;

    /**
     * Todo o estado de um voo.
     */
    private final FullStateOfTheProblem fullStat;

    /**
     * Impressora do ficheiro de log.
     */
    private BufferedWriter bufferWritter;

    /**
     * Texto de reportagem anterior.
     */
    private String previousLog;

    /**
     * Inicializa o monitor de log.
     * @param K
     * @param N
     * @param T
     * @param M
     */
    public Logger(int K, int N, int T, int M) {
        this.K = K;
        this.N = N;
        this.T = T;
        this.M = M;
        fullStat = new FullStateOfTheProblem(N, T);
        previousLog = "";
    }
    
    /**
     * Inicializa o monitor de log.
     * @param setup
     */
    public Logger(InitialSetup setup) {
        this.K = setup.getFlights();
        this.N = setup.getPassengersPerFlight();
        this.T = setup.getBusSeats();
        this.M = setup.getBusSeats();
        fullStat = new FullStateOfTheProblem(N, T);
        previousLog = "";
    }

    /**
     * Abre um novo ficheiro de log e caso um com o mesmo nome exista o anterior
     * é apagado.
     *
     * @param fileName nome do ficheiro de log.
     * @throws IOException se ocorrer algum problema ao abrir o ficheiro.
     */
    public synchronized void open(String fileName) throws IOException {
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        } else {
            file.delete();
            file.createNewFile();
        }
        bufferWritter = new BufferedWriter(new FileWriter(file, true));
        String header = "               AIRPORT RHAPSODY - Description of the "
                + "internal state of the problem\nPLANE    PORTER              "
                + "    DRIVER\nFN BN  Stat CB SR   Stat  ";
        for (int i = 0; i < N; i++) {
            header += "Q" + i + " ";
        }
        for (int i = 0; i < T; i++) {
            header += " S" + i;
        }
        header += "\n                                                          "
                + "   PASSENGERS\n";
        for (int i = 0; i < N; i++) {
            header += "St" + i + " Si" + i + " NR" + i + " NA" + i + " ";
        }
        header += "\n";
        bufferWritter.write(header);
    }

    /**
     * Começa um novo voo limpando toda a estrutura de dados.
     */
    public synchronized void startFlight() {
//        fullStat.plane.fn = 0;
//        fullStat.plane.bn = 0;
        fullStat.porter.stat = Porter_State.WAITING_FOR_A_PLANE_TO_LAND;
        fullStat.porter.cb = 0;
        fullStat.porter.sr = 0;
        fullStat.driver.stat = Driver_State.PARKING_AT_THE_ARRIVAL_TERMINAL;
        for (int i = 0; i < N; i++) {
            fullStat.driver.q[i] = -1;
        }
        for (int i = 0; i < T; i++) {
            fullStat.driver.s[i] = -1;
        }
        for (int i = 0; i < N; i++) {
            fullStat.passengers[i].st = Passenger_State.AT_THE_DISEMBARKING_ZONE;
//            fullStat.passengers[i].si = Passenger_Status.IN_TRANSIT;
//            fullStat.passengers[i].nr = 0;
            fullStat.passengers[i].na = 0;
        }
    }

    /**
     * Grava o estado actual no ficheiro de log caso este seja diferente do
     * anterior.
     *
     * @throws IOException se ocorrer algum problema ao escrever no ficheiro.
     */
    public synchronized void save() throws IOException {
        String currentLog = fullStat.toString();
        if (currentLog.compareTo(previousLog) != 0) {
            bufferWritter.write(currentLog);
            previousLog = currentLog;
        }
    }

    /**
     * Fecha o ficheiro de log.
     *
     * @throws IOException se ocorrer algum problema ao fechar o ficheiro.
     */
    public synchronized void close() throws IOException {
        bufferWritter.close();
    }

    /**
     * Regista o número do voo.
     *
     * @param fn número do voo.
     */
    public synchronized void setFlightNumber(Integer fn) {
        fullStat.plane.fn = fn;
    }

    /**
     * Regista a quantidade de bagagem no voo.
     *
     * @param bn quantidade de bagagem de todos os passageiros.
     */
    public synchronized void setLuggageOnThePlane(Integer bn) {
        fullStat.plane.bn = bn;
    }

    /**
     * O bagageiro retira uma peça de bagagem do avião.
     */
    @Override
    public synchronized void restoreAPieceOfLuggageFromThePlane() {
        fullStat.plane.bn--;
    }

    /**
     * Muda o estado do ciclo de vida do bagageiro, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do bagageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    @Override
    public synchronized void setPorterState(Porter_State stat)
            throws IOException {
        fullStat.porter.stat = stat;
        save();
    }

    /**
     * O bagageiro adiciona uma peça de bagagem à correia de transporte.
     */
    @Override
    public synchronized void addLuggageToTheConveyorBelt() {
        fullStat.porter.cb++;
    }

    /**
     * O passageiro recolhe uma peça de bagagem da correia de transporte.
     *
     * @param id número de identificação do passageiro.
     */
    @Override
    public synchronized void restoreLugageFromTheConveyorBelt(Integer id) {
        fullStat.passengers[id].na++;
        fullStat.porter.cb--;
    }

    /**
     * Retira toda a bagagem da correia de transporte.
     */
    public synchronized void emptyConveyorBelt() {
        fullStat.porter.cb = 0;
    }

    /**
     * O bagageiro adiciona uma peça de bagagem no armazém.
     */
    @Override
    public synchronized void addLuggageToTheStoreroom() {
        fullStat.porter.sr++;
    }

    /**
     * Retira toda a bagagem do armazém.
     */
    public synchronized void emptyStoreroom() {
        fullStat.porter.sr = 0;
    }

    /**
     * Muda o estado do ciclo de vida do motorista, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do motorista.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    @Override
    public synchronized void setDriverState(Driver_State stat) throws IOException {
        fullStat.driver.stat = stat;
        save();
    }

    /**
     * Um passageiro entra para a última posição livre da fila de espera do
     * autocarro.
     *
     * @param id número de identificação do passageiro que entrou na fila de
     * espera.
     */
    @Override
    public synchronized void addPassengerToTheWaitingQueue(Integer id) {
        for (int i = 0; i < N; i++) {
            if (fullStat.driver.q[i] == -1) {
                fullStat.driver.q[i] = id;
                break;
            }
        }
    }

    /**
     * Um passageiro embarca no autocarro.
     *
     * @param id número de identificação do passageiro que entrou no autocarro.
     */
    @Override
    public synchronized void attributePassengerHerBusSeat(Integer id) {
        for (int i = 0; i < T; i++) {
            if (fullStat.driver.s[i] == -1) {
                fullStat.driver.s[i] = id;
                break;
            }
        }
    }

    /**
     * O primeiro passageiro na fila de espera dai desta para entrar no
     * autocarro.
     */
    @Override
    public synchronized void removePassengerFromTheWaitingQueue() {
        for (int i = 0; i < N - 1; i++) {
            fullStat.driver.q[i] = fullStat.driver.q[i + 1];
        }
        fullStat.driver.q[N - 1] = -1;
    }

    /**
     * Um passageiro sai do autocarro, libertando o seu lugar.
     *
     * @param id número de identificação do passageiro que saíu no autocarro.
     */
    @Override
    public synchronized void revokePassengerHerBusSeat(Integer id) {
        for (int i = 0; i < T; i++) {
            if (fullStat.driver.s[i] == id) {
                fullStat.driver.s[i] = -1;
                break;
            }
        }
    }

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     * @throws IOException se ocorrer algum problema a escrever no ficheiro.
     */
    @Override
    public synchronized void setPassengerState(Passenger_State st, Integer id)
            throws IOException {
        fullStat.passengers[id].st = st;
        save();
    }

    /**
     * Atribui uma situação a um passageiro, se está em trânsito ou este
     * aeroporto é o seu voo final.
     *
     * @param si situação do passageiro.
     * @param id número de identificação do passageiro.
     */
    public synchronized void setPassengerStatus(Passenger_Status si, Integer id) {
        fullStat.passengers[id].si = si;
    }

    /**
     * Define a quantidade de bagagem de um passageiro no início do voo.
     *
     * @param nr quantidade de bagagem.
     * @param id número de identificação do passageiro.
     */
    public synchronized void setLuggageCarriedAtTheStartOfHerJourney(Integer nr, Integer id) {
        fullStat.passengers[id].nr = nr;
    }

}
