package mutex;

import java.io.IOException;
import logging.ATE_Logger;
import protocol.InitialSetup;
import proxy.Logger_Proxy;
import struct.Passenger_State;

/**
 * Monitor que implementa as acções do passageiro no terminal de desembarque,
 * antes de sair do aeroporto.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Arrival_Terminal_Exit implements Passenger_ATE {

    /**
     * O repositório geral de informação.
     */
    private final ATE_Logger logger;

    /**
     * Inicializa o monitor, recebendo a referência do repositório geral de
     * informação.
     *
     * @param logger Repositório geral de informação.
     */
    public Arrival_Terminal_Exit(ATE_Logger logger) {
        this.logger = logger;
    }
    
    /**
     * Inicializa o monitor, recebendo a mensagem de configuraçao inicial.
     * @param setup Mensagem de configuração inicial.
     */
    public Arrival_Terminal_Exit(InitialSetup setup){
        this.logger=new Logger_Proxy(setup.getLoggerAddress());
    }

    /**
     * Se o passageiro perder malas, reporta o sucedido.
     *
     * @param id Identificação do passageiro que vai reportar malas perdidas.
     */
    @Override
    public synchronized void reportMissingBags(Integer id) {
        try {
            logger.setPassengerState(Passenger_State.AT_THE_BAGGAGE_RECLAIM_OFFICE, id);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("Passenger " + id + " is reporting missing bags. HELP!"); //apenas para debug
    }

    /**
     * O passageiro vai para casa e abandona o aeroporto, se for o seu destino
     * final e se se encontrar na situação de possuir malas e já ter recolhido
     * todas, possuir malas e ter reclamado de malas perdidas ou não possuir
     * malas.
     *
     * @param id Identificação do passageiro a abandonar o aeroporto.
     */
    @Override
    public synchronized void goHome(Integer id) {
        try {
            logger.setPassengerState(Passenger_State.EXITING_THE_ARRIVAL_TERMINAL, id);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("Passenger " + id + " is leaving the airport and going home."); //apenas para debug
    }

}
