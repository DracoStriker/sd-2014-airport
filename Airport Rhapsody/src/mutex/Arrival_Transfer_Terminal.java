package mutex;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import logging.ATT_Logger;
import protocol.InitialSetup;
import proxy.Logger_Proxy;
import struct.Driver_State;
import struct.Passenger_State;

/**
 * Zona de transferência para o terminal de desembarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 28-03-2014
 */
public class Arrival_Transfer_Terminal implements Passenger_ATT, Driver_ATT {

    /**
     * Fila de espera do autocarro.
     */
    private final LinkedList<Integer> busWaitingQueue;

    /**
     * Condição que indica que a hora de partida foi atingida.
     */
    private boolean timeToGo;

    /**
     * Condição que indica que um passageiro entrou no autocarro e o motorista
     * pode convidar o próximo a entrar.
     */
    private boolean passengerEntered;

    /**
     * Condição que indica que o próximo passageiro foi chamado pelo motorista.
     */
    private boolean passengerCalled;

    /**
     * Número de lugares do autocarro.
     */
    private final int nTotalSeats;

    /**
     * Número de passageiros em trânsito para todos os voos de hoje.
     */
    private final int nFlightsForToday;

    /**
     * Número de passageiros que entraram no autocarro.
     */
    private int nPassengersOnTheBus;

    /**
     * Gerador de tempos aleatórios.
     */
    private final Random r;

    /**
     * O repositório geral de informação.
     */
    private final ATT_Logger logger;

    /**
     * Número do voo actual.
     */
    private int nFlight;

    /**
     * Número de passageiros que estão à espera de apanhar o autocarro.
     */
    private int nPassengersWaitingForADrive;

    private final ReentrantLock lock;

    private final Condition driver;

    private final HashMap<Integer, Condition> passengers;

    /**
     * Inicializa este monitor passando o número de lugares do autocarro, o
     * número de passageiros em trânsito para todos os voos de hoje e a
     * referência do repositório geral de informação.
     *
     * @param nTotalSeats número de lugares do autocarro.
     * @param nFlightsForToday número de voos hoje.
     * @param logger o repositório geral de informação.
     */
    public Arrival_Transfer_Terminal(Integer nTotalSeats, Integer nFlightsForToday,
            ATT_Logger logger) {
        busWaitingQueue = new LinkedList<>();
        timeToGo = false;
        passengerEntered = false;
        passengerCalled = false;
        this.nTotalSeats = nTotalSeats;
        this.nFlightsForToday = nFlightsForToday;
        this.logger = logger;
        r = new Random();
        nPassengersOnTheBus = 0;
        nFlight = 1;
        lock = new ReentrantLock(true);
        driver = lock.newCondition();
        passengers = new HashMap<>();
    }
    
    /**
     * Inicializa este monitor passando a mensagem de configuração inicial.
     * @param setup Mensagem de configuração inicial.
     */
    public Arrival_Transfer_Terminal(InitialSetup setup){
        busWaitingQueue = new LinkedList<>();
        timeToGo = false;
        passengerEntered = false;
        passengerCalled = false;
        this.nTotalSeats = setup.getBusSeats();
        this.nFlightsForToday = setup.getFlights();
        this.logger = new Logger_Proxy(setup.getLoggerAddress());
        r = new Random();
        nPassengersOnTheBus = 0;
        nFlight = 1;
        lock = new ReentrantLock(true);
        driver = lock.newCondition();
        passengers = new HashMap<>();
    }

    /**
     * Inicializa o estado do voo actual.
     *
     * @param nFlight número do voo actual.
     * @param nPassengersWaitingForADrive número de passageiros em trânsito do
     * voo actual.
     */
    public void startFlight(Integer nFlight,
            Integer nPassengersWaitingForADrive) {
        lock.lock();
        try {
            this.nFlight = nFlight;
            this.nPassengersWaitingForADrive += nPassengersWaitingForADrive;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Os passageiros colocam-se na fila de espera do autocarro pela sua ordem
     * de chegada e caso o número de pessageiros na fila de espera chegue para
     * lotar o autocarro ou tenha chegada a hora de partida o passageiro avisa o
     * motorista que pode mandar chamar os passageiros.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     */
    @Override
    public void takeABus(Integer id) {
        lock.lock();
        try {
            busWaitingQueue.offer(id); // o passageiro coloca-se na primeira posição livre da fila de espera
            passengers.put(id, lock.newCondition());
            System.out.println("Passenger " + id + " arrived at the bus "
                    + "waiting queue.");
            if (nTotalSeats == busWaitingQueue.size() || timeToGo) { // caso o número de passageiros à espera seja igual ao núemro de lugares do autocarro ou está na hroa de partir
                driver.signal(); // o passageiro avisa o motorista que pode chamar os passageiros para antrarem
            }
            try {
                logger.addPassengerToTheWaitingQueue(id);
                logger.setPassengerState(
                        Passenger_State.AT_THE_ARRIVAL_TRANSFER_TERMINAL, id);
            } catch (IOException ex) {
                System.err.println(ex);
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Após o motorista anunciar o embarque dos passageiros, cada passageiro
     * embarca no autocarro conforme a ordem na fila de espera.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     */
    @Override
    public void enterTheBus(Integer id) {
        lock.lock();
        try {
            while (!(Objects.equals(busWaitingQueue.peek(), id) && passengerCalled)) { // o passageiro espera que seja a sua vez de entrar no autocarro
                try {
                    passengers.get(id).await();
                } catch (InterruptedException ex) {
                }
            }
            busWaitingQueue.remove(); // o passageiro sai da fila de espera
            passengers.remove(id);
            passengerCalled = false;
            passengerEntered = true;
            System.out.println("Passenger " + id + " entered the bus.");
            try {
                logger.removePassengerFromTheWaitingQueue();
                logger.attributePassengerHerBusSeat(id);
                logger.setPassengerState(Passenger_State.TERMINAL_TRANSFER, id);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            driver.signal(); // o passageiro avisa o motorista que pode chamar o próximo passageiro
        } finally {
            lock.unlock();
        }
    }

    /**
     * O motorista espera até que haja passageiros suficientes para encher o
     * autocarro na fila de espera ou chegue a hora de partida e há pelo menos
     * um passageiro para entrar ou se já transportou todos os passageiros em
     * trânsito e caso sim termina o seu dia de trabalho.
     *
     * @return se todos os passageiros em trânsito já foram transportados para o
     * terminal de embarque.
     */
    @Override
    public boolean hasDaysWorkEnded() {
        lock.lock();
        try {
            System.out.println("Driver is ready and waiting for enough passengers to fill the bus.");
            try {
                logger.setDriverState(Driver_State.PARKING_AT_THE_ARRIVAL_TERMINAL);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            boolean daysWorkEnded = false;
            while (!(nTotalSeats == busWaitingQueue.size()) && !(timeToGo && !busWaitingQueue.isEmpty()) && !(daysWorkEnded = (nFlightsForToday == nFlight) && (nPassengersWaitingForADrive == 0))) { // o motorista espera que a fila de espera encha ou que chegue a hora de partida e há pelo menos um passageiro à espera
                Timer timer = new Timer();
                DriverTimerTask task = new DriverTimerTask();
                timer.schedule(task, r.nextInt(100)); // a hora de partida é anunciada
                try {
                    driver.await();
                } catch (InterruptedException ex) {
                }
                task.cancel();
                timer.cancel();
            }
            timeToGo = false;
            if (daysWorkEnded) {
                System.out.println("Has driver work ended? Yes there are no more "
                        + "passenger to transport.");
            } else {
                System.out.println("Has driver work ended? No, this is the flight "
                        + nFlight + " and I'm still waiting for more "
                        + nPassengersWaitingForADrive + " passengers.");
            }
            return daysWorkEnded;
        } finally {
            lock.unlock();
        }
    }

    /**
     * O motorista anuncia que os passageiros podem entrar no autocarro
     * controlando a entrada dos passageiros um a um até que não existam mais
     * lugares livres ou enquanto ainda há passageiros em fila desde que existam
     * lugares livres.
     */
    @Override
    public void announcingBusBoarding() {
        lock.lock();
        try {
            System.out.println("Announcing bus boarding!");
            while (nPassengersOnTheBus < nTotalSeats && !busWaitingQueue.isEmpty()) { // o motorista verifica se ainda há lugares livres e se ainda há passageiros à espera
                passengerCalled = true;
                passengers.get(busWaitingQueue.peek()).signal(); // o motorista chama o primeiro passageiro da fila
                while (!passengerEntered) { // e espera que entre
                    try {
                        driver.await();
                    } catch (InterruptedException ex) {
                    }
                }
                passengerEntered = false;
                nPassengersOnTheBus++;
            }
            passengerCalled = false; // o motorista para de chamar passageiros pois está na hora de partir
            System.out.println("The bus left to the Departure Transfer Terminal.");
        } finally {
            lock.unlock();
        }
    }

    /**
     * Após os passageiros terem embarcado no autocarro, o motorista parte para
     * o terminal de embarque.
     *
     * @return o número de passageiros que embarcaram no autocarro.
     */
    @Override
    public int goToDepartureTerminal() {
        lock.lock();
        try {
            nPassengersWaitingForADrive -= nPassengersOnTheBus;
            System.out.println("The bus is travelling to the Departure Transfer Terminal.");
            try {
                logger.setDriverState(Driver_State.DRIVING_FORWARD);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            return nPassengersOnTheBus;
        } finally {
            lock.unlock();
        }
    }

    /**
     * O motorista estaciona o autocarro na zona de transferência do terminal de
     * desembarque após ter regressado do terminal de embarque.
     */
    @Override
    public void parkTheBus() {
        lock.lock();
        try {
            nPassengersOnTheBus = 0;
            System.out.println("The bus parked at the Arrival Transfer Terminal.");
            try {
                logger.setDriverState(Driver_State.PARKING_AT_THE_ARRIVAL_TERMINAL);
            } catch (IOException ex) {
                System.err.println(ex);
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Horário de partida do autocarro.
     *
     * @author Simão Reis <simao.paulo@ua.pt>
     * @version 1.0
     * @since 15-03-2014
     */
    private class DriverTimerTask extends TimerTask {

        @Override
        public void run() {
            lock.lock();
            try {
                System.out.println("The driver checked that it's time to leave!");
                timeToGo = true;
                driver.signal(); // chegou a hora de partida
            } finally {
                lock.unlock();
            }
        }

    }
}
