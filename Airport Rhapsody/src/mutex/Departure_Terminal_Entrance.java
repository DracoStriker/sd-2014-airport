package mutex;

import java.io.IOException;
import logging.DTE_Logger;
import protocol.InitialSetup;
import proxy.Logger_Proxy;
import struct.Passenger_State;

/**
 * Terminal de embarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Departure_Terminal_Entrance implements Passenger_DTE {

    /**
     * O repositório geral de informação.
     */
    private final DTE_Logger logger;

    /**
     * Inicializa este monitor passando a referência do repositório geral de
     * informação.
     *
     * @param logger o repositório geral de informação.
     */
    public Departure_Terminal_Entrance(DTE_Logger logger) {
        this.logger = logger;
    }

    /**
     * Inicializa este monitor passando a mensagem de configuração inicial.
     * @param setup Mensagem de configuração inicial.
     */
    public Departure_Terminal_Entrance(InitialSetup setup){
        this.logger = new Logger_Proxy(setup.getLoggerAddress());
    }
    
    /**
     * O passageiro embarca no avião.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * avião.
     */
    @Override
    public synchronized void prepareNextLeg(Integer id) {
        System.out.println("Passenger " + id + " boarded the plane.");
        try {
            logger.setPassengerState(
                    Passenger_State.ENTERING_THE_DEPARTURE_TERMINAL, id);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

}
