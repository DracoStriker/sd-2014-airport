package mutex;

import java.io.IOException;
import logging.DTT_Logger;
import protocol.InitialSetup;
import proxy.Logger_Proxy;
import struct.Driver_State;
import struct.Passenger_State;

/**
 * Zona de transferência para o terminal de embarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Departure_Transfer_Terminal implements Passenger_DTT, Driver_DTT {

    /**
     * Condição para indicar que o autocarro chegou e estacionou.
     */
    private boolean theBusHasArrived;

    /**
     * Número de passageiros que ainda estão dentro do autocarro.
     */
    private int nPassengersOnTheBus;

    /**
     * O repositório geral de informação.
     */
    private final DTT_Logger logger;

    /**
     * Ponto de sincronização do monitor.
     */
    private final Object lock;

    /**
     * Inicializa este monitor passando a referência do repositório geral de
     * informação.
     *
     * @param logger O repositório geral de informação.
     */
    public Departure_Transfer_Terminal(DTT_Logger logger) {
        theBusHasArrived = false;
        this.logger = logger;
        lock = new Object();
    }
    
    /**
     * Inicializa este monitor passando a mensagem de configuração inicial.
     * @param setup Mensagem de configuração inicial.
     */
    public Departure_Transfer_Terminal(InitialSetup setup){
        theBusHasArrived = false;
        this.logger = new Logger_Proxy(setup.getLoggerAddress());
        lock = new Object();
    }

    /**
     * O passageiro sai do autocarro.
     *
     * @param id número de identificação do passageiro que vai sair do
     * autocarro.
     */
    @Override
    public void leaveTheBus(Integer id) {
        synchronized (lock) {
            while (!theBusHasArrived) { // o passageiro espera que o autocarro estacione
                try {
                    lock.wait();
                } catch (InterruptedException ex) {
                }
            }
            if (--nPassengersOnTheBus == 0) { // o último passageiro avisa o motorista que já sairam todos
                lock.notify();
            }
            System.out.println("Passenger " + id + " left the bus.");
            try {
                logger.revokePassengerHerBusSeat(id);
                logger.setPassengerState(
                        Passenger_State.AT_THE_DEPARTURE_TRANSFER_TERMINAL, id);
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }

    /**
     * O autocarro estaciona à entrada do terminal de embarque do aeroporto e
     * deixa os passageiros sairem.
     *
     * @param nPassengersOnTheBus número de passageiros no autocarro quando este
     * estacionou.
     */
    @Override
    public void parkTheBusAndLetPassOff(Integer nPassengersOnTheBus) {
        synchronized (lock) {
            this.nPassengersOnTheBus = nPassengersOnTheBus;
            theBusHasArrived = true;
            System.out.println("The bus parked at the Departure Transfer "
                    + "Terminal with " + nPassengersOnTheBus
                    + " passenger(s).");
            try {
                logger.setDriverState(
                        Driver_State.PARKING_AT_THE_DEPARTURE_TERMINAL);
            } catch (IOException ex) {
                System.err.println(ex);
            }
            lock.notifyAll(); // o motorista avisa os passageiros que já podem sair
            while (this.nPassengersOnTheBus > 0) { // o motorista espera que todos os passageiros saiam
                try {
                    lock.wait();
                } catch (InterruptedException ex) {
                }
            }
            System.out.println("All passengers have left the bus.");
        }
    }

    /**
     * O motorista do autocarro regressa ao terminal de desembarque.
     */
    @Override
    public void goToArrivalTerminal() {
        synchronized (lock) {
            theBusHasArrived = false;
            System.out.println(
                    "The bus left to the Arrival Transfer Terminal.");
            try {
                logger.setDriverState(Driver_State.DRIVING_BACKWARD);
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }

}
