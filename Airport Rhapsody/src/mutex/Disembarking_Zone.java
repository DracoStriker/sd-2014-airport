package mutex;

import java.io.IOException;
import java.util.ArrayList;
import logging.DZ_Logger;
import protocol.InitialSetup;
import proxy.Logger_Proxy;
import struct.Bag;
import struct.Passenger_State;
import struct.Passenger_Status;
import struct.Porter_State;

/**
 * Monitor que implementa as acções do passageiro e do bagageiro na zona de
 * desembarque, onde os passageiros saem do avião.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Disembarking_Zone implements Passenger_DZ, Porter_DZ {

    /**
     * Lista de estados internos do passageiro.
     */
    private ArrayList<Passenger_Status> passengerStatus;
    
    /**
     * Lista de malas no avião.
     */
    private ArrayList<Bag> planeBags;
    
    /**
     * Número de pessoas que já sairam do avião.
     */
    private int counter = 0;
    
    /**
     * Condição para acordar o bagageiro.
     */
    private boolean wakePorter = false;
    
    /**
     * Número total de passageiros de cada voo.
     */
    private final int N;
    
    /**
     * Repositório geral de informação.
     */
    private final DZ_Logger logger;

    /**
     * Inicializa o monitor, recebendo o número total de passageiros de cada
     * voo, a referência da lista de estados internos do passageiro, a
     * referência da lista de malas no avião e a referência do repositório geral
     * de informação.
     *
     * @param N Número total de passageiros de cada voo.
     * @param logger Repositório geral de informação.
     */
    public Disembarking_Zone(Integer N, DZ_Logger logger) {
        this.N = N;
        this.logger = logger;
    }

    /**
     * Inicializa o monitor, recebendo a mensagem de configuração inicial.
     * 
     * @param setup Mensagem de configuração inicial.
     */
    public Disembarking_Zone(InitialSetup setup){
        this.N = setup.getPassengersPerFlight();
        this.logger = new Logger_Proxy(setup.getLoggerAddress());
    }
    
    /**
     * Actualiza a informação das listas a utilizar durante a simulação.
     * @param passengerStatus Lista de estados dos passageiros.
     * @param planeBags Lista de malas do avião.
     */
    public synchronized void setLists(ArrayList<Passenger_Status> passengerStatus, ArrayList<Bag> planeBags){
        this.passengerStatus=passengerStatus;
        this.planeBags=planeBags;
    }
    
    /**
     * Indica qual a próxima acção do passageiro com base no seu estado interno.
     * Acordará o bagageiro caso seja o último a sair do avião.
     *
     * @param id Identificação do passageiro.
     * @return Estado interno do passageiro.
     */
    @Override
    public synchronized Passenger_Status whatShouldIDo(Integer id) {
        try {
            logger.setPassengerState(Passenger_State.AT_THE_DISEMBARKING_ZONE, id); //escrever no log
        } catch (IOException ex) {
            System.err.println(ex);
        }
        counter++; //incrementa o número de passageiros que saiu do avião
        System.out.println("Passenger " + id + " left the plane."); //print apenas para debug
        if (counter == N) { //se o contador == número total de passageiros, então este é o último a sair do avião
            System.out.println("Passenger " + id + " was the last exiting the airplane. Let's go and wake the porter :D"); //print apenas para debug
            wakePorter = true; //condição que permite acordar o bagageiro sem que ele volte a esperar no ciclo
            counter = 0; //reset do contador para o próximo voo
            notifyAll(); //acorda o bagageiro
        }
        return passengerStatus.get(id);
    }

    /**
     * O bagageiro espera até que outro passageiro de outro voo o acorde.
     */
    @Override
    public synchronized void takeARest() {
        try {
            logger.setPorterState(Porter_State.WAITING_FOR_A_PLANE_TO_LAND); //escreve no log a mudança de estado do passageiro
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("Porter is resting."); //print apenas para debug
        while (!wakePorter) { //enquanto o último passageiro não saiu do avião e não activou a condição, o bagageiro não pode prosseguir
            try {
                wait();
            } catch (InterruptedException ex) {
            }
        }
        wakePorter = false; //o bagageiro já acordou, reset da condição para o próximo voo
        System.out.println("Porter is awake!"); //print apenas para debug
    }

    /**
     * O bagageiro tenta recolher uma mala do porão do avião, caso ela exista.
     *
     * @return Mala recolhida ou null (caso não exista).
     */
    @Override
    public synchronized Bag tryToCollectABag() {
        if (planeBags.isEmpty()) { //se não existirem malas no avião, apenas muda de estado
            try {
                logger.setPorterState(Porter_State.AT_THE_PLANES_HOLD); //escreve no log a mudança de estado do bagageiro
            } catch (IOException ex) {
                System.err.println(ex);
            }
            System.out.println("Unable to retrieve bags from plane"); //print apenas para debug
            return null;
        } else { //se existirem malas no avião, muda de estado e retira a mala
            logger.restoreAPieceOfLuggageFromThePlane(); //escreve no log o número de malas no avião
            try {
                logger.setPorterState(Porter_State.AT_THE_PLANES_HOLD); //escreve no log a mudança de estado do bagageiro
            } catch (IOException ex) {
                System.err.println(ex);
            }
            return planeBags.remove(0);
        }
    }

}
