package mutex;

/**
 * Acções do motorista na zona de transferência para o terminal de embarque do
 * aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Driver_ATT {

    /**
     * O motorista verifica se já transportou todos os passageiros em trânsito e
     * caso sim termina o seu dia de trabalho.
     *
     * @return se todos os passageiros em trânsito já foram transportados para o
     * terminal de embarque.
     */
    public boolean hasDaysWorkEnded();

    /**
     * O motorista anuncia que os passageiros podem entrar no autocarro, e
     * controla a entrada dos passageiros um a um até que não existam mais
     * lugares livres mas caso chegue a hora de partida e há pelo menos um
     * passageiro no autocarro este parte ainda com lugares livres.
     */
    public void announcingBusBoarding();

    /**
     * Após os passageiros terem embarcado no autocarro, o motorista parte para
     * o terminal de embarque.
     *
     * @return o número de passageiros que embarcaram no autocarro.
     */
    public int goToDepartureTerminal();

    /**
     * O motorista estaciona o autocarro na zona de transferência do terminal de
     * desembarque após ter regressado do terminal de embarque.
     */
    public void parkTheBus();

}
