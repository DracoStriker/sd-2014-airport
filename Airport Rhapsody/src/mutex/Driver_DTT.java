package mutex;

/**
 * Acções do motorista na zona de transferência para o terminal de desembarque
 * do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Driver_DTT {
    
    /**
     * O autocarro estaciona à entrada do terminal de embarque do aeroporto e
     * deixa os passageiros sairem.
     *
     * @param nPassengersOnTheBus número de passageiros no autocarro quando este
     * estacionou.
     */
    public void parkTheBusAndLetPassOff(Integer nPassengersOnTheBus);
    
    /**
     * O motorista do autocarro regressa ao terminal de desembarque.
     */
    public void goToArrivalTerminal();
}
