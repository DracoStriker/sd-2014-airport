package mutex;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import logging.LCP_Logger;
import protocol.InitialSetup;
import proxy.Logger_Proxy;
import struct.Bag;
import struct.Passenger_State;
import struct.Porter_State;

/**
 * Monitor que implementa as acções do passageiro e bagageiro na zona de recolha
 * de bagagens.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Luggage_Collection_Point implements Passenger_LCP, Porter_LCP {

    /**
     * Ponto de bloqueio para garantir regime de exclusão mútua
     */
    private final ReentrantLock lock = new ReentrantLock(true);
    /**
     * Ponto de sincronização dos passageiros
     */
    private final Condition[] conditions;
    /**
     * Lista de malas de cada passageiro na passadeira rolante de recolha.
     */
    private final HashMap<Integer, LinkedList<Bag>> conveyorBelt = new HashMap<>();
    /**
     * Condição de espera para recolha de bagagens por parte dos passageiros.
     */
    private boolean hasMoreBags = true;
    /**
     * Repositório geral de informação.
     */
    private final LCP_Logger logger;

    /**
     * Inicializa o monitor recebendo o número total de passageiros de cada voo
     * e a referência do repositório geral de informação.
     *
     * @param N Número total de passageiros de cada voo.
     * @param logger Repositório geral de informação.
     */
    public Luggage_Collection_Point(Integer N, LCP_Logger logger) {
        conditions = new Condition[N];
        for (int i = 0; i < N; i++) {
            conveyorBelt.put(i, new LinkedList<Bag>()); //inicialização da lista de malas para cada passageiro
            conditions[i] = lock.newCondition(); //inicialização das condições para cada passageiro
        }
        this.logger = logger;
    }
    
    /**
     * Inicializa o monitor, recebendo a mensagem de configuração inicial.
     * @param setup Mensagem de configuração inicial.
     */
    public Luggage_Collection_Point(InitialSetup setup){
        int N = setup.getPassengersPerFlight();
        conditions = new Condition[N];
        for (int i = 0; i < N; i++) {
            conveyorBelt.put(i, new LinkedList<Bag>()); //inicialização da lista de malas para cada passageiro
            conditions[i] = lock.newCondition(); //inicialização das condições para cada passageiro
        }
        this.logger = new Logger_Proxy(setup.getLoggerAddress());
    }

    /**
     * Redefine o valor da condição de espera para recolha de bagagens por parte
     * dos passageiros.
     *
     * @param hasMoreBags Condição de espera para recolha de bagagens por parte
     * dos passageiros, indicando se há mais sacos para recolher.
     */
    public void setHasMoreBags(Boolean hasMoreBags) {
        lock.lock();
        try {
            this.hasMoreBags = hasMoreBags;
        } finally {
            lock.unlock();
        }
    }

    /**
     * O passageiro recolhe uma mala da passadeira rolante de recolha.
     *
     * @param id Identificação do passageiro a recolher a mala.
     * @return Mala recolhida ou null (caso não haja nenhuma mala a recolher).
     */
    @Override
    public Bag goCollectABag(Integer id) {
        lock.lock(); //bloqueio para exclusão mútua
        try {
            Bag collectedBag;
            while (conveyorBelt.get(id).isEmpty() && hasMoreBags) { //condiçao de espera do passageiro:  
                try {                                               //se não há malas para si e o bagageiro ainda
                    conditions[id].await(); //tem mais malas para colocar, o passageiro espera
                } catch (InterruptedException ex) {
                }
            }
            if (!conveyorBelt.get(id).isEmpty()) { //uma vez que já não está a espera, se houver malas para si, muda de estado e recolhe-as
                logger.restoreLugageFromTheConveyorBelt(id); //escreve no log o número de malas que o passageiro leva
                try {
                    logger.setPassengerState(Passenger_State.AT_THE_LUGGAGE_COLLECTION_POINT, id); //escreve no log a mudança de estado do passageiro
                } catch (IOException ex) {
                    System.err.println(ex);
                }
                collectedBag = conveyorBelt.get(id).remove();
                System.out.println("Passenger " + id + " collected bag :)"); //print apenas para debug
                return collectedBag;
            } else { //se não houver malas para si, muda de estado e vai embora
                try {
                    logger.setPassengerState(Passenger_State.AT_THE_LUGGAGE_COLLECTION_POINT, id); //escreve no log a mudança de estado do passageiro
                } catch (IOException ex) {
                    System.err.println(ex);
                }
                return null;
            }
        } finally {
            lock.unlock(); //desbloqueio para exclusão mútua
        }

    }

    /**
     * O bagageiro desloca a mala recolhida do porão do avião para o destino
     * apropriado conforme o destino do passageiro seja o destino final ou não.
     *
     * @param b Mala recolhida do porão do avião.
     */
    @Override
    public void carryItToAppropriateStore(Bag b) {
        lock.lock(); //bloqueio para exclusão mútua
        try {
            int id = b.getId(); //obter a identificação do proprietário da mala
            switch (b.getStatus()) { //verifica o estado interno do passageiro proprietario da mala
                case IN_TRANSIT: //se o destino não é o final, muda de estado e simula a colocação da mala no armazém
                    logger.addLuggageToTheStoreroom();
                    try {
                        logger.setPorterState(Porter_State.AT_THE_STOREROOM); //escreve no log a mudança de estado do bagageiro
                    } catch (IOException ex) {
                        System.err.println(ex);
                    }
                    System.out.println("Bag sent to storage room from passenger " + id); //print apenas para debug
                    break;
                default: //caso o destino seja final, muda de estado e coloca a mala na passadeira rolante de recolha e acorda todos os passageiros
                    logger.addLuggageToTheConveyorBelt(); //escreve no log o numero de malas na passadeira rolante de recolha
                    try {
                        logger.setPorterState(Porter_State.AT_THE_LUGGAGE_BELT_CONVEYOR); //escreve no log a mudança de estado do bagageiro
                    } catch (IOException ex) {
                        System.err.println(ex);
                    }
                    System.out.println("Bag sent to conveyor belt from passenger " + id); //print apenas para debug
                    conveyorBelt.get(id).add(b);
                    conditions[id].signal(); //acorda o proprietario da mala
                    break;
            }
        } finally {
            lock.unlock(); //desbloqueio para exclusão mútua
        }
    }

    /**
     * O bagageiro não tem mais malas para recolher e acorda todos os
     * passageiros que estão ainda à espera de alguma mala.
     */
    @Override
    public void noMoreBagsToCollect() {
        lock.lock(); //bloqueio para exclusão mútua
        try {
            try {
                logger.setPorterState(Porter_State.WAITING_FOR_A_PLANE_TO_LAND); //escreve no log a mudança de estado do bagageiro
            } catch (IOException ex) {
                System.err.println(ex);
            }
            System.out.println("Porter has no more bags to collect :'("); //print apenas para debug
            hasMoreBags = false; //já não há malas para colocar na passadeira rolante de recolha
            for (Condition c : conditions) {
                c.signal();
            } //acorda todos os passageiros que ainda estão à espera
        } finally {
            lock.unlock(); //desbloqueio para exclusão mútua
        }

    }
}
