package mutex;

/**
 * Interface das acções do passageiro no terminal de desembarque, antes de sair
 * do aeroporto.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_ATE {

    /**
     * Se o passageiro perder malas, reporta o sucedido.
     *
     * @param id Identificação do passageiro que vai reportar malas perdidas.
     */
    public void reportMissingBags(Integer id);

    /**
     * Se for o destino final do passageiro, o passageiro vai para casa e
     * abandona o aeroporto, se se encontrar nas seguintes situações:
     * <li>Possui malas e recolheu-as todas;
     * <li>Possui malas e reclamou de malas perdidas;
     * <li>Não possui malas.
     *
     * @param id Identificação do passageiro a abandonar o aeroporto.
     */
    public void goHome(Integer id);

}
