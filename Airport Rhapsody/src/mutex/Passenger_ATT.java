package mutex;

/**
 * Acções do passageiro na zona de transferência para o terminal de embarque do
 * aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_ATT {

    /**
     * Os passageiros colocam-se na fila de espera do autocarro pela sua ordem
     * de chegada.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     */
    public void takeABus(Integer id);

    /**
     * Após o motorista anunciar o embarque dos passageiros, cada passageiro
     * embarca no autocarro conforme a ordem na fila de espera.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     */
    public void enterTheBus(Integer id);

}
