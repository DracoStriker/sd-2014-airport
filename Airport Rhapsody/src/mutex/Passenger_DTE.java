package mutex;

/**
 * Acções do passageiro no terminal de desembarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_DTE {

    /**
     * O passageiro embarca no avião.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * avião.
     */
    public void prepareNextLeg(Integer id);

}
