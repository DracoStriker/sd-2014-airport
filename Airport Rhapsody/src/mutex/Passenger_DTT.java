package mutex;

/**
 * Acções do passageiro na zona de transferência para o terminal de desembarque
 * do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_DTT {

    /**
     * O passageiro sai do autocarro.
     *
     * @param id número de identificação do passageiro que vai sair do
     * autocarro.
     */
    public void leaveTheBus(Integer id);

}
