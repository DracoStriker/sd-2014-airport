package mutex;

import struct.Passenger_Status;

/**
 * Interface das acções do passageiro na zona de desembarque, onde o mesmo sai
 * do avião.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_DZ {

    /**
     * Indica qual a próxima acção do passageiro com base no seu estado interno.
     * Acordará o bagageiro caso seja o último a sair do avião.
     *
     * @param id Identificação do passageiro.
     * @return Estado interno do passageiro.
     */
    public Passenger_Status whatShouldIDo(Integer id);

}
