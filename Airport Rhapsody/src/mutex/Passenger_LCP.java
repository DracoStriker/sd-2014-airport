package mutex;

import struct.Bag;

/**
 * Interface das acções do bagageiro na zona de recolha de bagagens.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Passenger_LCP {

    /**
     * O passageiro recolhe uma mala da passadeira rolante de recolha.
     *
     * @param id Identificação do passageiro a recolher a mala.
     * @return Mala recolhida ou null (caso não haja nenhuma mala a recolher).
     */
    public Bag goCollectABag(Integer id);

}
