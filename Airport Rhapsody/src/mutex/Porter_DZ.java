package mutex;

import struct.Bag;

/**
 * Interface das acções do bagageiro na zona de desembarque, onde os passageiros
 * saem do avião.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Porter_DZ {

    /**
     * O bagageiro espera até que outro passageiro de outro voo o acorde.
     */
    public void takeARest();

    /**
     * O bagageiro tenta recolher uma mala do porão do avião, caso ela exista.
     *
     * @return Mala recolhida ou null (caso não exista).
     */
    public Bag tryToCollectABag();

}
