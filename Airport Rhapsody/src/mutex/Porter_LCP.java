package mutex;

import struct.Bag;

/**
 * Interface das acções do bagageiro na zona de recolha de bagagens.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public interface Porter_LCP {

    /**
     * O bagageiro desloca a mala recolhida do porão do avião para o destino
     * apropriado conforme o destino do passageiro seja o destino final ou não.
     *
     * @param b Mala recolhida do porão do avião.
     */
    public void carryItToAppropriateStore(Bag b);

    /**
     * O bagageiro não tem mais malas para recolher e acorda todos os
     * passageiros que estão ainda à espera de alguma mala.
     */
    public void noMoreBagsToCollect();

}
