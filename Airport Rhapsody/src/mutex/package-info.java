/**
 * Contém as regiões de memória partilhada para a simulação do Airport Rhapsody 
 * oferecendo exclusão mútua aos seus métodos.
 */
package mutex;
