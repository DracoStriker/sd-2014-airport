package protocol;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import struct.Simulation;

/**
 * Mensagem de configuração inicial a enviar para todos os monitores e entidades.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class InitialSetup implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Endereço completo do monitor do terminal de desembarque, antes dos passageiros sairem do aeroporto.
     */
    private final InetSocketAddress ateAddress;

    /**
     * Endereço completo do monitor da zona de transferência para o terminal de desembarque.
     */
    private final InetSocketAddress attAddress;

    /**
     * Endereço completo do monitor de terminal de embarque.
     */
    private final InetSocketAddress dteAddress;

    /**
     * Endereço completo do monitor da zona de transferência para o terminal de embarque.
     */
    private final InetSocketAddress dttAddress;

    /**
     * Endereço completo do monitor da zona de desembarque, antes dos passageiros sairem do avião.
     */
    private final InetSocketAddress dzAddress;

    /**
     * Endereço completo do monitor da zona de recolha de bagagens.
     */
    private final InetSocketAddress lcpAddress;

    /**
     * Endereço completo do repositório geral de informação.
     */
    private final InetSocketAddress loggerAddress;

    /**
     * Lista de simulações, contendo toda a informação necessária para cada simulação.
     */
    private final ArrayList<Simulation> simulations;

    /**
     * Número de voos.
     */
    private final int flights;

    /**
     * Número de passageiros por cada voo.
     */
    private final int passengersPerFlight;

    /**
     * Número de lugares do autocarro de transferência.
     */
    private final int busSeats;

    /**
     * Número máximo de peças de bagagem.
     */
    private final int luggage;

    /**
     * Inicializa a mensagem recebendo todos os endereços dos monitores, as listas e as variáveis de simulação necessárias a todos os monitores e entidades.
     * @param ateAddress Endereço completo do monitor do terminal de desembarque, antes dos passageiros sairem do aeroporto.
     * @param attAddress Endereço completo do monitor da zona de transferência para o terminal de desembarque.
     * @param dteAddress Endereço completo do monitor de terminal de embarque.
     * @param dttAddress Endereço completo do monitor da zona de transferência para o terminal de embarque.
     * @param dzAddress Endereço completo do monitor da zona de desembarque, antes dos passageiros sairem do avião.
     * @param lcpAddress Endereço completo do monitor da zona de recolha de bagagens.
     * @param loggerAddress Endereço completo do repositório geral de informação.
     * @param simulations Lista de simulações, contendo toda a informação necessária para cada simulação.
     * @param flights Número de voos.
     * @param passengersPerFlight Número de passageiros por cada voo.
     * @param busSeats Número de lugares do autocarro de transferência.
     * @param luggage Número máximo de peças de bagagem.
     */
    public InitialSetup(InetSocketAddress ateAddress, InetSocketAddress attAddress, InetSocketAddress dteAddress, InetSocketAddress dttAddress, InetSocketAddress dzAddress, InetSocketAddress lcpAddress, InetSocketAddress loggerAddress, ArrayList<Simulation> simulations, int flights, int passengersPerFlight, int busSeats, int luggage) {
        this.ateAddress = ateAddress;
        this.attAddress = attAddress;
        this.dteAddress = dteAddress;
        this.dttAddress = dttAddress;
        this.dzAddress = dzAddress;
        this.lcpAddress = lcpAddress;
        this.loggerAddress = loggerAddress;
        this.simulations = simulations;
        this.flights = flights;
        this.passengersPerFlight = passengersPerFlight;
        this.busSeats = busSeats;
        this.luggage = luggage;
    }

    /**
     * Obtém o endereço completo do monitor do terminal de desembarque, antes dos passageiros sairem do aeroporto.
     * @return Endereço completo do monitor do terminal de desembarque, antes dos passageiros sairem do aeroporto.
     */
    public InetSocketAddress getAteAddress() {
        return ateAddress;
    }

    /**
     * Obtém o endereço completo do monitor da zona de transferência para o terminal de desembarque.
     * @return Endereço completo do monitor da zona de transferência para o terminal de desembarque.
     */
    public InetSocketAddress getAttAddress() {
        return attAddress;
    }

    /**
     * Obtém o endereço completo do monitor de terminal de embarque.
     * @return Endereço completo do monitor de terminal de embarque.
     */
    public InetSocketAddress getDteAddress() {
        return dteAddress;
    }

    /**
     * Obtém o endereço completo do monitor da zona de transferência para o terminal de embarque.
     * @return Endereço completo do monitor da zona de transferência para o terminal de embarque.
     */
    public InetSocketAddress getDttAddress() {
        return dttAddress;
    }

    /**
     * Obtém o endereço completo do monitor da zona de desembarque, antes dos passageiros sairem do avião.
     * @return Endereço completo do monitor da zona de desembarque, antes dos passageiros sairem do avião.
     */
    public InetSocketAddress getDzAddress() {
        return dzAddress;
    }

    /**
     * Obtém o endereço completo do monitor da zona de recolha de bagagens.
     * @return Endereço completo do monitor da zona de recolha de bagagens.
     */
    public InetSocketAddress getLcpAddress() {
        return lcpAddress;
    }

    /**
     * Obtém o endereço completo do repositório geral de informação.
     * @return Endereço completo do repositório geral de informação.
     */
    public InetSocketAddress getLoggerAddress() {
        return loggerAddress;
    }

    /**
     * Obtém a lista de simulações, contendo toda a informação necessária para cada simulação.
     * @return Lista de simulações, contendo toda a informação necessária para cada simulação.
     */
    public ArrayList<Simulation> getSimulations() {
        return simulations;
    }

    /**
     * Obtém o número de voos.
     * @return Número de voos.
     */
    public int getFlights() {
        return flights;
    }

    /**
     * Obtém o número de passageiros por voo.
     * @return Número de passageiros por voo.
     */
    public int getPassengersPerFlight() {
        return passengersPerFlight;
    }

    /**
     * Obtém o número de lugares do autocarro de transferência.
     * @return Número de lugares do autocarro de transferência.
     */
    public int getBusSeats() {
        return busSeats;
    }

    /**
     * Obtém o número máximo de malas de cada passageiro.
     * @return Número máximo de malas de cada passageiro.
     */
    public int getLuggage() {
        return luggage;
    }

    @Override
    public String toString() {
        return "Message InitialSetup\n"
                + "ATE Address = " + ateAddress + "\nATT Address = "
                + attAddress + "\nDTE Address = " + dteAddress
                + "\nDTT Address = " + dttAddress + "\nDZ  Address = "
                + dzAddress + "\nLCP Address = " + lcpAddress
                + "\n Logger Address = " + loggerAddress + "\n Simulations = "
                + simulations + "\nFlights = " + flights
                + "\nPassengers per Flight = " + passengersPerFlight
                + "\nBus Seats = " + busSeats + "\nLuggage = " + luggage;
    }

}
