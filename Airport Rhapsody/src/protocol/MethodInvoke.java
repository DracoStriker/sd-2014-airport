package protocol;

import java.io.Serializable;

/**
 * Mensagem de requisito de um método remoto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
public class MethodInvoke implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * Nome do método remoto.
     */
    private final String method;
    
    /**
     * Lista de argumentos do método remoto.
     */
    private final Object[] args;

    /**
     * Inicializa esta mensagem passando o nome do método remoto e os argumentos
     * que vão ser passados.
     * 
     * @param method nome do método remoto.
     * @param args argumentos do método remoto.
     */
    public MethodInvoke(String method, Object... args) {
        this.method = method;
        this.args = args;
    }

    /**
     * Obter o nome do método a invocar.
     * 
     * @return nome do método a invocar.
     */
    public String getMethodName() {
        return method;
    }

    /**
     * Obter a lista dos argumentos do método a invocar.
     * 
     * @return a lista dos argumentos do método a invocar.
     */
    public Object[] getArgs() {
        return args;
    }
    
    @Override
    public String toString() {
        String message = "Message Method Invoke\nMethod = " + method;
        for (int i = 0; i < args.length; i++) {
            message += "\nArgs[" + i + "] = " + args[i];
        }
        return message;
    }
}
