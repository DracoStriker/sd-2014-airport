package protocol;

import java.io.Serializable;

/**
 * Valor de retorno de um método remoto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
public class MethodReturn implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * Valor de retorno de um método remoto.
     */
    private final Object output;

    /**
     * Inicializa uma mensagem de retorno com o valor retornado do método.
     * 
     * @param output valor retornado do método.
     */
    public MethodReturn(Object output) {
        this.output = output;
    }

    /**
     * Obter o valro de retorno do método remoto.
     * 
     * @return valor retornado do método.
     */
    public Object getReturn() {
        return output;
    }
    
    @Override
    public String toString() {
        return "Message Method Return\nReturn = " + output;
    }
}
