package protocol;

import java.io.Serializable;
import struct.Client;

/**
 * Mensagem de encerramento para os servidores.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class Terminate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Cliente que envia a mensagem.
     */
    private final Client sender;

    /**
     * Inicializa a mensagem, indicando o remetente.
     * @param sender Cliente que envia a mensagem.
     */
    public Terminate(Client sender) {
        this.sender = sender;
    }

    /**
     * Obtém o cliente que enviou a mensagem.
     * @return Cliente que enviou a mensagem.
     */
    public Client getSender() {
        return sender;
    }

}
