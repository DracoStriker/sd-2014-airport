package proxy;

import java.net.InetSocketAddress;
import mutex.Passenger_ATE;

/**
 * Proxy do monitor que implementa as acções do passageiro no terminal de 
 * desembarque, antes de sair do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
public class Arrival_Terminal_Exit_Proxy extends ServerProxy
        implements Passenger_ATE {

    /**
     * Construtor de um proxy definindo o endpoint remoto.
     * 
     * @param endPoint par endereço IP e porto TCP do servidor para a ligação.
     */
    public Arrival_Terminal_Exit_Proxy(InetSocketAddress endPoint) {
        super(endPoint);
    }

    /**
     * Se o passageiro perder malas, reporta o sucedido.
     *
     * @param id Identificação do passageiro que vai reportar malas perdidas.
     */
    @Override
    public void reportMissingBags(Integer id) {
        invokeRemote("reportMissingBags", id);
    }

    /**
     * O passageiro vai para casa e abandona o aeroporto, se for o seu destino
     * final e se se encontrar na situação de possuir malas e já ter recolhido
     * todas, possuir malas e ter reclamado de malas perdidas ou não possuir
     * malas.
     *
     * @param id Identificação do passageiro a abandonar o aeroporto.
     */
    @Override
    public void goHome(Integer id) {
        invokeRemote("goHome", id);
    }

}
