package proxy;

import java.net.InetSocketAddress;
import mutex.Driver_ATT;
import mutex.Passenger_ATT;

/**
 * Proxy da zona de transferência para o terminal de desembarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
public class Arrival_Transfer_Terminal_Proxy extends ServerProxy implements
        Passenger_ATT, Driver_ATT {

    /**
     * Construtor de um proxy definindo o endpoint remoto.
     * 
     * @param endPoint par endereço IP e porto TCP do servidor para a ligação.
     */
    public Arrival_Transfer_Terminal_Proxy(InetSocketAddress endPoint) {
        super(endPoint);
    }
    
    /**
     * Inicializa o estado do voo actual.
     *
     * @param nFlight número do voo actual.
     * @param nPassengersWaitingForADrive número de passageiros em trânsito do
     * voo actual.
     */
    public void startFlight(int nFlight, int nPassengersWaitingForADrive) {
        invokeRemote("startFlight", nFlight, nPassengersWaitingForADrive);
    }

    /**
     * Os passageiros colocam-se na fila de espera do autocarro pela sua ordem
     * de chegada e caso o número de pessageiros na fila de espera chegue para
     * lotar o autocarro ou tenha chegada a hora de partida o passageiro avisa o
     * motorista que pode mandar chamar os passageiros.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     */
    @Override
    public void takeABus(Integer id) {
        invokeRemote("takeABus", id);
    }

    /**
     * Após o motorista anunciar o embarque dos passageiros, cada passageiro
     * embarca no autocarro conforme a ordem na fila de espera.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * autocarro.
     */
    @Override
    public void enterTheBus(Integer id) {
        invokeRemote("enterTheBus", id);
    }

    /**
     * O motorista espera até que haja passageiros suficientes para encher o
     * autocarro na fila de espera ou chegue a hora de partida e há pelo menos
     * um passageiro para entrar ou se já transportou todos os passageiros em
     * trânsito e caso sim termina o seu dia de trabalho.
     *
     * @return se todos os passageiros em trânsito já foram transportados para o
     * terminal de embarque.
     */
    @Override
    public boolean hasDaysWorkEnded() {
        return (boolean) invokeRemote(Boolean.class, "hasDaysWorkEnded");
    }

    /**
     * O motorista anuncia que os passageiros podem entrar no autocarro
     * controlando a entrada dos passageiros um a um até que não existam mais
     * lugares livres ou enquanto ainda há passageiros em fila desde que existam
     * lugares livres.
     */
    @Override
    public void announcingBusBoarding() {
        invokeRemote("announcingBusBoarding");
    }

    /**
     * Após os passageiros terem embarcado no autocarro, o motorista parte para
     * o terminal de embarque.
     *
     * @return o número de passageiros que embarcaram no autocarro.
     */
    @Override
    public int goToDepartureTerminal() {
        return (int) invokeRemote(Integer.class, "goToDepartureTerminal");
    }

    /**
     * O motorista estaciona o autocarro na zona de transferência do terminal de
     * desembarque após ter regressado do terminal de embarque.
     */
    @Override
    public void parkTheBus() {
        invokeRemote("parkTheBus");
    }

}
