package proxy;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import protocol.InvalidReturnException;
import protocol.MethodInvoke;
import protocol.MethodReturn;
import protocol.Terminate;
import run.Logger_Process;
import run.ServerProcess;
import struct.Client;

/**
 * Proxy de um cliente, residente num servidor, que trata das mensagem recebidas num dado servidor.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @param <T> Tipo de monitor.
 * @since 22-04-2014
 */
public class ClientProxy<T> extends Thread {

    /**
     * Socket para comunicação bidireccional.
     */
    private final Socket socket;

    /**
     * Monitor que executa as funções requisitadas.
     */
    private final T monitor;

    /**
     * Instância do servidor que origina esta thread de Proxy.
     */
    private ServerProcess serverProcess;

    /**
     * Instância do logger que origina esta thread de Proxy.
     */
    private Logger_Process loggerProcess;

    /**
     * Inicializa esta proxy, recebendo a socket a utilizar para comunicação, o monitor que vai executar os métodos e a instância do servidor que origina a thread de proxy.
     * @param socket Socket a utilizar para a comunicação.
     * @param monitor Monitor que vai executar os métodos da mensagem.
     * @param serverProcess Instância do servidor que origina a thread de proxy.
     */
    public ClientProxy(Socket socket, T monitor, ServerProcess serverProcess) {
        this.socket = socket;
        this.monitor = monitor;
        this.serverProcess = serverProcess;
    }

    /**
     * Inicializa esta proxy, recebendo a socket a utilizar para comunicação, o monitor que vai executar os métodos e a instância do logger que origina a thread de proxy.
     * @param socket Socket a utilizar para a comunicação.
     * @param monitor Monitor que vai executar os métodos da mensagem.
     * @param loggerProcess Instância do logger que origina a thread de proxy.
     */
    public ClientProxy(Socket socket, T monitor, Logger_Process loggerProcess) {
        this.socket = socket;
        this.monitor = monitor;
        this.loggerProcess = loggerProcess;
    }

    @Override
    public void run() {
        try (ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())) {
            Object msg = in.readObject();
            if (msg instanceof Terminate) {
                terminateClientConnection(((Terminate) msg).getSender());
                out.writeObject(new MethodReturn(null));
            } else if (msg instanceof MethodInvoke) {
                MethodInvoke method = (MethodInvoke) msg;
//                System.out.println("\nReceived: " + method + "\nFrom " + socket.getRemoteSocketAddress() + "\n");
                MethodReturn ret = new MethodReturn(runMethod(monitor, method.getMethodName(), method.getArgs()));
                out.writeObject(ret);
//                System.out.println("\nSent: " + ret + "\n");
            } else {
                throw new InvalidReturnException("Expected "
                        + Terminate.class.getName() + " or "
                        + MethodInvoke.class.getName() + ", at method "
                        + "run but returned "
                        + msg.getClass().getName() + ".");
            }
        } catch (IOException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InvalidReturnException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Corre o método especificado no monitor especificado.
     * @param monitor Monitor que vai correr o método.
     * @param methodName Nome do método a correr.
     * @param args Argumentos para o método (se houverem).
     * @return Valor de retorno do método, ou null se não houver.
     */
    private Object runMethod(T monitor, String methodName, Object... args) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method method;
        if (args.length != 0) {
            Class<?>[] argsClass = new Class<?>[args.length];
            for (int i = 0; i < args.length; i++) {
                argsClass[i] = args[i].getClass();
            }
            method = monitor.getClass().getMethod(methodName, argsClass);
            return method.invoke(monitor, args);
        } else {
            method = monitor.getClass().getMethod(methodName);
            return method.invoke(monitor);
        }
    }

    /**
     * Indica à instância que originou este proxy que o cliente remetente desta mensagem terminou a sua execução.
     * @param client Cliente remetente da mensagem.
     */
    private void terminateClientConnection(Client client) {
        if (serverProcess != null) {
            serverProcess.close(client);
        } else {
            loggerProcess.close(client);
        }
    }
}
