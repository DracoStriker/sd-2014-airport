package proxy;

import java.net.InetSocketAddress;
import mutex.Passenger_DTE;

/**
 * Proxy do terminal de embarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
public class Departure_Terminal_Entrance_Proxy extends ServerProxy
        implements Passenger_DTE {

    /**
     * Construtor de um proxy definindo o endpoint remoto.
     * 
     * @param endPoint par endereço IP e porto TCP do servidor para a ligação.
     */
    public Departure_Terminal_Entrance_Proxy(InetSocketAddress endPoint) {
        super(endPoint);
    }

    /**
     * O passageiro embarca no avião.
     *
     * @param id número de identificação do passageiro que vai embarcar no
     * avião.
     */
    @Override
    public void prepareNextLeg(Integer id) {
        invokeRemote("prepareNextLeg", id);
    }

}
