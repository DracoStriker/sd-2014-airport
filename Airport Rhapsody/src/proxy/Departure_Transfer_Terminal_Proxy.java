package proxy;

import java.net.InetSocketAddress;
import mutex.Driver_DTT;
import mutex.Passenger_DTT;

/**
 * Proxy da zona de transferência para o terminal de embarque do aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
public class Departure_Transfer_Terminal_Proxy extends ServerProxy implements
        Passenger_DTT, Driver_DTT {

    /**
     * Construtor de um proxy definindo o endpoint remoto.
     * 
     * @param endPoint par endereço IP e porto TCP do servidor para a ligação.
     */
    public Departure_Transfer_Terminal_Proxy(InetSocketAddress endPoint) {
        super(endPoint);
    }

    /**
     * O passageiro sai do autocarro.
     *
     * @param id número de identificação do passageiro que vai sair do
     * autocarro.
     */
    @Override
    public void leaveTheBus(Integer id) {
        invokeRemote("leaveTheBus", id);
    }

    /**
     * O autocarro estaciona à entrada do terminal de embarque do aeroporto e
     * deixa os passageiros sairem.
     *
     * @param nPassengersOnTheBus número de passageiros no autocarro quando este
     * estacionou.
     */
    @Override
    public void parkTheBusAndLetPassOff(Integer nPassengersOnTheBus) {
        invokeRemote("parkTheBusAndLetPassOff", nPassengersOnTheBus);
    }

    /**
     * O motorista do autocarro regressa ao terminal de desembarque.
     */
    @Override
    public void goToArrivalTerminal() {
        invokeRemote("goToArrivalTerminal");
    }

}
