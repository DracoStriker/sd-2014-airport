package proxy;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import mutex.Passenger_DZ;
import mutex.Porter_DZ;
import struct.Bag;
import struct.Passenger_Status;

/**
 * Proxy do monitor que implementa as acções do passageiro e do bagageiro na 
 * zona de desembarque, onde os passageiros saem do avião.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
public class Disembarking_Zone_Proxy extends ServerProxy implements
        Passenger_DZ, Porter_DZ {

    /**
     * Construtor de um proxy definindo o endpoint remoto.
     * 
     * @param endPoint par endereço IP e porto TCP do servidor para a ligação.
     */
    public Disembarking_Zone_Proxy(InetSocketAddress endPoint) {
        super(endPoint);
    }
    
    /**
     * 
     * @param passengerStatus
     * @param planeBags 
     */
    public void setLists(ArrayList<Passenger_Status> passengerStatus, ArrayList<Bag> planeBags) {
        invokeRemote("setLists", passengerStatus, planeBags);
    }

    /**
     * Indica qual a próxima acção do passageiro com base no seu estado interno.
     * Acordará o bagageiro caso seja o último a sair do avião.
     *
     * @param id Identificação do passageiro.
     * @return Estado interno do passageiro.
     */
    @Override
    public Passenger_Status whatShouldIDo(Integer id) {
        return (Passenger_Status) invokeRemote(Passenger_Status.class, "whatShouldIDo",
                id);
    }

    /**
     * O bagageiro espera até que outro passageiro de outro voo o acorde.
     */
    @Override
    public void takeARest() {
        invokeRemote("takeARest");
    }

    /**
     * O bagageiro tenta recolher uma mala do porão do avião, caso ela exista.
     *
     * @return Mala recolhida ou null (caso não exista).
     */
    @Override
    public Bag tryToCollectABag() {
        return (Bag) invokeRemote(Bag.class, "tryToCollectABag");
    }
}
