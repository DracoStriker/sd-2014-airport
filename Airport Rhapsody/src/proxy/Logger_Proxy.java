package proxy;

import java.net.InetSocketAddress;
import logging.ATE_Logger;
import logging.ATT_Logger;
import logging.DTE_Logger;
import logging.DTT_Logger;
import logging.DZ_Logger;
import logging.LCP_Logger;
import struct.Driver_State;
import struct.Passenger_State;
import struct.Passenger_Status;
import struct.Porter_State;

/**
 * Monitor de reportagem dos eventos dos voos durante um dia de trabalho.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 02-04-2014
 */
public class Logger_Proxy extends ServerProxy implements ATE_Logger, ATT_Logger,
        DTE_Logger, DTT_Logger, DZ_Logger, LCP_Logger {

    /**
     * Construtor de um proxy definindo o endpoint remoto.
     *
     * @param endPoint par endereço IP e porto TCP do servidor para a ligação.
     */
    public Logger_Proxy(InetSocketAddress endPoint) {
        super(endPoint);
    }

    /**
     * Começa um novo voo limpando toda a estrutura de dados.
     */
    public void startFlight() {
        invokeRemote("startFlight");
    }

    /**
     * Grava o estado actual no ficheiro de log caso este seja diferente do
     * anterior.
     *
     */
    public void save() {
        invokeRemote("save");
    }

    /**
     * Regista o número do voo.
     *
     * @param fn número do voo.
     */
    public void setFlightNumber(Integer fn) {
        invokeRemote("setFlightNumber", fn);
    }

    /**
     * Muda o estado do ciclo de vida do passageiro, e grava em ficheiro o
     * estado actual do voo.
     *
     * @param st estado do passageiro.
     * @param id número de identificação do passageiro.
     */
    @Override
    public void setPassengerState(Passenger_State st, Integer id) {
        invokeRemote("setPassengerState", st, id);
    }

    /**
     * Muda o estado do ciclo de vida do motorista, e grava em ficheiro o estado
     * actual do voo.
     *
     * @param stat estado do motorista.
     */
    @Override
    public void setDriverState(Driver_State stat) {
        invokeRemote("setDriverState", stat);
    }

    /**
     * Um passageiro entra para a última posição livre da fila de espera do
     * autocarro.
     *
     * @param id número de identificação do passageiro que entrou na fila de
     * espera.
     */
    @Override
    public void addPassengerToTheWaitingQueue(Integer id) {
        invokeRemote("addPassengerToTheWaitingQueue", id);
    }

    /**
     * Um passageiro embarca no autocarro.
     *
     * @param id número de identificação do passageiro que entrou no autocarro.
     */
    @Override
    public void attributePassengerHerBusSeat(Integer id) {
        invokeRemote("attributePassengerHerBusSeat", id);
    }

    /**
     * O primeiro passageiro na fila de espera dai desta para entrar no
     * autocarro.
     */
    @Override
    public void removePassengerFromTheWaitingQueue() {
        invokeRemote("removePassengerFromTheWaitingQueue");
    }

    /**
     * Um passageiro sai do autocarro, libertando o seu lugar.
     *
     * @param id número de identificação do passageiro que saíu no autocarro.
     */
    @Override
    public void revokePassengerHerBusSeat(Integer id) {
        invokeRemote("revokePassengerHerBusSeat", id);
    }

    /**
     * Regista a quantidade de bagagem no voo.
     *
     * @param bn quantidade de bagagem de todos os passageiros.
     */
    public void setLuggageOnThePlane(Integer bn) {
        invokeRemote("setLuggageOnThePlane", bn);
    }

    /**
     * O bagageiro retira uma peça de bagagem do avião.
     */
    @Override
    public void restoreAPieceOfLuggageFromThePlane() {
        invokeRemote("restoreAPieceOfLuggageFromThePlane");
    }

    /**
     * 
     * @param stat 
     */
    @Override
    public void setPorterState(Porter_State stat) {
        invokeRemote("setPorterState", stat);
    }

    /**
     * O bagageiro adiciona uma peça de bagagem à correia de transporte.
     */
    @Override
    public void addLuggageToTheConveyorBelt() {
        invokeRemote("addLuggageToTheConveyorBelt");
    }

    /**
     * O passageiro recolhe uma peça de bagagem da correia de transporte.
     *
     * @param id número de identificação do passageiro.
     */
    @Override
    public void restoreLugageFromTheConveyorBelt(Integer id) {
        invokeRemote("restoreLugageFromTheConveyorBelt", id);
    }

    /**
     * O bagageiro adiciona uma peça de bagagem no armazém.
     */
    @Override
    public void addLuggageToTheStoreroom() {
        invokeRemote("addLuggageToTheStoreroom");
    }

    /**
     * Retira toda a bagagem do armazém.
     */
    public synchronized void emptyStoreroom() {
        invokeRemote("emptyStoreroom");
    }

    /**
     * Atribui uma situação a um passageiro, se está em trânsito ou este
     * aeroporto é o seu voo final.
     *
     * @param si situação do passageiro.
     * @param id número de identificação do passageiro.
     */
    public void setPassengerStatus(Passenger_Status si, Integer id) {
        invokeRemote("setPassengerStatus", si, id);
    }

    /**
     * Define a quantidade de bagagem de um passageiro no início do voo.
     *
     * @param nr quantidade de bagagem.
     * @param id número de identificação do passageiro.
     */
    public void setLuggageCarriedAtTheStartOfHerJourney(Integer nr, Integer id) {
        invokeRemote("setLuggageCarriedAtTheStartOfHerJourney", nr, id);
    }
}
