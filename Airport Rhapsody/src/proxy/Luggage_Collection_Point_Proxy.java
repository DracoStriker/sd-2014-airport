package proxy;

import java.net.InetSocketAddress;
import mutex.Passenger_LCP;
import mutex.Porter_LCP;
import struct.Bag;

/**
 * Proxy do monitor que implementa as acções do passageiro e bagageiro na zona 
 * de recolha de bagagens.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
public class Luggage_Collection_Point_Proxy extends ServerProxy implements
        Passenger_LCP, Porter_LCP {

    /**
     * Construtor de um proxy definindo o endpoint remoto.
     * 
     * @param endPoint par endereço IP e porto TCP do servidor para a ligação.
     */
    public Luggage_Collection_Point_Proxy(InetSocketAddress endPoint) {
        super(endPoint);
    }

    /**
     * Redefine o valor da condição de espera para recolha de bagagens por parte
     * dos passageiros.
     *
     * @param hasMoreBags Condição de espera para recolha de bagagens por parte
     * dos passageiros, indicando se há mais sacos para recolher.
     */
    public void setHasMoreBags(boolean hasMoreBags) {
        invokeRemote("setHasMoreBags", hasMoreBags);
    }

    /**
     * O passageiro recolhe uma mala da passadeira rolante de recolha.
     *
     * @param id Identificação do passageiro a recolher a mala.
     * @return Mala recolhida ou null (caso não haja nenhuma mala a recolher).
     */
    @Override
    public Bag goCollectABag(Integer id) {
        return (Bag) invokeRemote(Bag.class, "goCollectABag", id);
    }

    /**
     * O bagageiro desloca a mala recolhida do porão do avião para o destino
     * apropriado conforme o destino do passageiro seja o destino final ou não.
     *
     * @param b Mala recolhida do porão do avião.
     */
    @Override
    public void carryItToAppropriateStore(Bag b) {
        invokeRemote("carryItToAppropriateStore", b);
    }

    /**
     * O bagageiro não tem mais malas para recolher e acorda todos os
     * passageiros que estão ainda à espera de alguma mala.
     */
    @Override
    public void noMoreBagsToCollect() {
        invokeRemote("noMoreBagsToCollect");
    }
}
