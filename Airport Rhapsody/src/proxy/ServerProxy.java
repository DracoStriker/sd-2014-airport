package proxy;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import protocol.InvalidReturnException;
import protocol.MethodInvoke;
import protocol.MethodReturn;
import protocol.NotExpectedReturnException;

/**
 * Proxy de um Servidor abstracto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 01-04-2014
 */
abstract class ServerProxy {

    /**
     * Par endereço IP e porto TCP do servidor para a ligação.
     */
    private final InetSocketAddress endPoint;

    /**
     * Construtor de um proxy definindo o endpoint remoto.
     *
     * @param endPoint par endereço IP e porto TCP do servidor para a ligação.
     */
    protected ServerProxy(InetSocketAddress endPoint) {
        this.endPoint = endPoint;
    }

    /**
     * Invocação de um método remoto.
     *
     * @param method nome to método remoto.
     * @param args argumentos do método remoto.
     * @return valor de retorno do método remoto.
     */
    protected Object invokeRemote(String method, Object... args) {
        MethodInvoke methodInvocation = new MethodInvoke(method, args);
        Socket socket = new Socket();
        try {
            socket.connect(endPoint);
            try (ObjectOutputStream socketObjectOutputStream
                    = new ObjectOutputStream(socket.getOutputStream());
                    ObjectInputStream socketObjectInputStream
                    = new ObjectInputStream(socket.getInputStream())) {
                socketObjectOutputStream.writeObject(methodInvocation);
//                System.out.println("\nSent: " + methodInvocation + "\nTo "
//                        + endPoint + "\n");
                Object returned = socketObjectInputStream.readObject();
//                System.out.println("\nReceived: " + returned + "\n");
                if (!(returned instanceof MethodReturn)) {
                    throw new InvalidReturnException("Expected "
                            + MethodReturn.class.getName() + ", at method "
                            + "invokeRemote but returned "
                            + returned.getClass().getName() + ".");
                }
                return ((MethodReturn) returned).getReturn();
            }
        } catch (IOException | ClassNotFoundException | InvalidReturnException ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                socket.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    /**
     * Invocação de um método remoto.
     *
     * @param expected tipo de valor esperado no retorno deste método.
     * @param method nome to método remoto.
     * @param args argumentos do método remoto.
     * @return valor de retorno do método remoto.
     */
    @SuppressWarnings({"ThrowableInstanceNotThrown", "ThrowableInstanceNeverThrown"})
    protected Object invokeRemote(Class expected, String method,
            Object... args) {
        Object returned = invokeRemote(method, args);
        if (returned != null) {
            if (!(expected.isInstance(returned))) {
                throw new RuntimeException(new NotExpectedReturnException(
                        "Expected " + expected.getName() + " at method "
                        + method + ", but returned value is "
                        + returned.getClass().getName() + "."));
            }
        }
        return returned;
    }
}
