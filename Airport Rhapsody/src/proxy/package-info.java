/**
 * Conjunto de proxies para representar as entidades activas e entidades 
 * passivas numa simulação distribuída que comunicam entre si usando o protocolo
 * TCP.
 */
package proxy;
