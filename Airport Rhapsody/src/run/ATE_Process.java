package run;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import mutex.Arrival_Terminal_Exit;

/**
 * Processo de simulação do monitor do terminal de desembarque, antes dos passageiros sairem do aeroporto.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class ATE_Process {

    /**
     *  Porta de escuta do monitor.
     */
    private static final int port = 22445;

    public static void main(String... args) {
        System.out.println("Arrival Terminal Exit Process");
        try {
            new ServerProcess<>(Arrival_Terminal_Exit.class).runProcess(port);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException | IOException ex) {
            System.err.println(ex);
        }
    }
}
