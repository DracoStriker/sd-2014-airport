package run;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import logging.Logger;
import mutex.Arrival_Terminal_Exit;
import mutex.Arrival_Transfer_Terminal;
import mutex.Departure_Terminal_Entrance;
import mutex.Departure_Transfer_Terminal;
import mutex.Disembarking_Zone;
import mutex.Luggage_Collection_Point;
import struct.Bag;
import struct.Passenger_Status;
import thread.Driver;
import thread.Passenger;
import thread.Porter;

/**
 * Simulação da Rapsódia no Aeroporto
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Airport_Rhapsody {

    /**
     * Número total de voos.
     */
    public static final int K = 5;

    /**
     * Número total de passageiro em cada voo.
     */
    public static final int N = 6;

    /**
     * Número de lugares no autocarro de transferência.
     */
    public static final int T = 3;

    /**
     * Número máximo de malas para o passageiro.
     */
    public static final int M = 2;

    public static void main(String... args) {

        String fileName = "airport_rhapsody_log.txt";

        if (args.length > 0) {
            fileName = args[0];
        }

        //variaveis iniciais necessarias à execução
        Passenger_Status[] possibleStatus = Passenger_Status.values(); //valores de estado interno possiveis para os passageiros
        Random r = new Random(); //randomizer para gerar numeros aleatorios
        ArrayList<Bag> planeBags = new ArrayList<>(); //lista de malas do avião
        ArrayList<Passenger_Status> passengers = new ArrayList<>(); //lista de estados internos dos passageiros
        ArrayList<Integer> passengersBagNumber = new ArrayList<>(); //lista do numero de malas de cada passageiro
        int passengersInTransit = 0; //numero total de passageiros cujo destino não é final

        //criar logger
        Logger logger = new Logger(K, N, T, M);
        try {
            logger.open(fileName);
        } catch (IOException ex) {
            System.err.println("Cannot create file!\nExiting");
            return;
        }

        //gerar os estados internos dos passageiros para todas as simulações
//        for (int k = 0; k < K; k++) {
//            for (int i = 0; i < N; i++) {
//                passengers.add(possibleStatus[r.nextInt(possibleStatus.length)]); //adicionar passageiro com estado aleatorio
//                if (passengers.get(passengers.size() - 1) == Passenger_Status.IN_TRANSIT) { //se o destino do passageiro nao for o final
//                    passengersInTransit++; //incrementar o numero total de passageiros cujo destino não é o final
//                }
//            }
//        }
        //criar monitores
        Arrival_Terminal_Exit ate = new Arrival_Terminal_Exit(logger);
//        Arrival_Transfer_Terminal att = new Arrival_Transfer_Terminal(T, passengersInTransit, logger);
//        Arrival_Transfer_Terminal_V2 att = new Arrival_Transfer_Terminal_V2(T, K, logger);
        Arrival_Transfer_Terminal att = new Arrival_Transfer_Terminal(T, K, logger);

        Departure_Terminal_Entrance dte = new Departure_Terminal_Entrance(logger);
        Departure_Transfer_Terminal dtt = new Departure_Transfer_Terminal(logger);
        Disembarking_Zone dz = new Disembarking_Zone(N, logger);
        Luggage_Collection_Point lcp = new Luggage_Collection_Point(N, logger);

        //criar threads
        Thread[] passengerThreads = new Thread[N];
        Thread porterThread = new Thread(new Porter(K, dz, lcp));
        Thread driverThread = new Thread(new Driver(att, dtt));

        //ciclo de simulações
        for (int k = 0; k < K; k++) {

            //inicializar o log para o inicio do voo
            logger.startFlight();

            /* simulation start */
            System.out.println("\nFlight " + (k + 1) + "\n"); //print apenas para debug
            logger.setFlightNumber(k + 1); //escrever no log o número do voo

            //criar lista de malas do avião e lista do número de malas para cada passageiro
            for (int i = 0; i < N; i++) {
                int bagsPerPassenger = 0; //numero de malas do passageiro
                double isGoingToMissBags = r.nextDouble(); //gerar valor de perda de malas
                int bagsToAdd; //numero de malas do passageiro a adicionar ao avião
                
                //gerar passageiros para o voo em simulação
                passengers.add(possibleStatus[r.nextInt(possibleStatus.length)]); //adicionar passageiro com estado aleatorio
                if (passengers.get(passengers.size() - 1) == Passenger_Status.IN_TRANSIT) { //se o destino do passageiro nao for o final
                    passengersInTransit++; //incrementar o numero total de passageiros cujo destino não é o final
                }

                if (passengers.get(i) == Passenger_Status.FINAL_DESTINATION_NO_BAGS) { //se o passageiro não tiver malas
                    passengersBagNumber.add(0);                                        //adiciona 0 a lista
                    logger.setPassengerStatus(passengers.get(i), i); // escreve no log o estado interno do passageiro
                    continue; //passa ao próximo passageiro
                }
                //gerar o número de malas para o passageiro (sem ser 0 no caso do estado interno ser destino final com malas, porque o passageiro tem de ter pelo menos 1 mala neste estado interno)
                while (bagsPerPassenger == 0 && passengers.get(i) == Passenger_Status.FINAL_DESTINATION_WITH_BAGS) {
                    bagsPerPassenger = r.nextInt(M + 1);
                }
                passengersBagNumber.add(bagsPerPassenger); //adiciona o numero de bagagens a lista
                logger.setLuggageCarriedAtTheStartOfHerJourney(bagsPerPassenger, i); //escreve no log o numero de malas que o passageiro carrega
                bagsToAdd = bagsPerPassenger;
                if (isGoingToMissBags > 0.8) { //se o valor de perda de malas e superior a 0.8, entao perde uma mala (probabilidade de ~20%)
                    bagsToAdd--;
                }
                //adicionar o numero de malas decidido para o aviao
                for (int j = 0; j < bagsToAdd; j++) {
                    planeBags.add(new Bag(i, passengers.get(i)));
                }

                logger.setPassengerStatus(passengers.get(i), i); //escrever o estado interno do passageiro

            }

            logger.setLuggageOnThePlane(planeBags.size()); //escrever no log o número total de malas no avião

            att.startFlight(k + 1, passengersInTransit);

            try {
                logger.save();
            } catch (IOException ex) {
                System.err.println(ex);
            }

            //colocar malas de modo aleatório
            Collections.shuffle(planeBags);

            dz.setLists(passengers, planeBags);
            //iniciar threads do bagageiro e motorista se for o primeiro voo
            if (k == 0) {
                porterThread.start();
                driverThread.start();
            }

            //inicializar threads dos passageiros
            for (int i = 0; i < N; i++) {
                passengerThreads[i] = new Thread(new Passenger(i, passengersBagNumber.get(i), ate, att, dte, dtt, dz, lcp));
            }

            //iniciar threads dos passageiros (correr)
            for (int i = 0; i < N; i++) {
                passengerThreads[i].start();
            }

            //esperar que todos os passageiros terminem a sua execução antes da proxima simulaçao
            for (int i = 0; i < N; i++) {
                try {
                    passengerThreads[i].join();
                } catch (InterruptedException ex) {
                }
            }

            /* ***fazer reset às condiçoes de simulação antes do proximo voo*** */
            //remover os estados internos dos passageiros anteriores
            for (int i = 0; i < N; i++) {
                passengers.remove(0);
            }

            //reset do número de passageiros em trânsito no voo em simulação
            passengersInTransit=0;
            
            //remover o numero de malas dos passageiros anteriores
            while (!passengersBagNumber.isEmpty()) {
                passengersBagNumber.remove(0);
            }

            //reset da condiçao de espera dos passageiros: em que o bagageiro tem mais malas a colocar na passadeira rolante de recolha
            lcp.setHasMoreBags(true);

            //esvaziar o armazem de malas no log
            logger.emptyStoreroom();

            /* ***fim de reset às condiçoes de simulação antes do proximo voo*** */
        }

        //apos todos os voos, esperar que o bagageiro e o motorista acabem a sua execução
        try {
            porterThread.join();
            driverThread.join();
        } catch (InterruptedException ex) {
        }
        try {
            logger.close();
        } catch (IOException ex) {
            System.err.println("Cannot close file!\nExiting");
        }
    }

}
