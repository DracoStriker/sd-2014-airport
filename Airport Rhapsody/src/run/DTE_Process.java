package run;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import mutex.Departure_Terminal_Entrance;

/**
 * Processo de simulação do monitor de terminal de embarque.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class DTE_Process {

    /**
     * Porta de escuta do monitor.
     */
    private static final int port = 22448;

    public static void main(String... args) {
        System.out.println("Departure Terminal Exit Process");
        try {
            new ServerProcess<>(Departure_Terminal_Entrance.class).runProcess(port);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException | IOException ex) {
            System.err.println(ex);
        }
    }
}
