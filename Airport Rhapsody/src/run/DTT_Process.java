package run;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import mutex.Departure_Transfer_Terminal;

/**
 * Processo de simulação do monitor da zona de transferência para o terminal de embarque.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class DTT_Process {

    /**
     * Porta de escuta do monitor.
     */
    private static final int port = 22447;

    public static void main(String... args) {
        System.out.println("Departure Transfer Terminal Process");
        try {
            new ServerProcess<>(Departure_Transfer_Terminal.class).runProcess(port);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException | IOException ex) {
            System.err.println(ex);
        }
    }
}
