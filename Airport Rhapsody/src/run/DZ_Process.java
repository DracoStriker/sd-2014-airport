package run;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import mutex.Disembarking_Zone;

/**
 * Processo de simulação do monitor da zona de desembarque, antes dos passageiros sairem do avião.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class DZ_Process {

    /**
     * Porta de escuta do monitor.
     */
    private static final int port = 22443;

    public static void main(String... args) {
        System.out.println("Disembarking Zone Process");
        try {
            new ServerProcess<>(Disembarking_Zone.class).runProcess(port);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException | IOException ex) {
            System.err.println(ex);
        }
    }
}
