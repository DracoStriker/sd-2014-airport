package run;

import java.io.IOException;
import static java.lang.System.err;
import static java.lang.System.out;
import protocol.InitialSetup;
import proxy.Arrival_Transfer_Terminal_Proxy;
import proxy.Departure_Transfer_Terminal_Proxy;
import struct.Client;
import thread.Driver;
import static util.ProcessConfiguration.getInitialSetup;
import static util.ProcessConfiguration.terminateServers;

/**
 * Simulação do bagageiro da Rapsódia no Aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 17-04-2014
 */
public class Driver_Process {

    /**
     * Porto de escuta TCP deste processo.
     */
    private static final int port = 22442;

    public static void main(String... args) {
        out.println("Driver Process");
        InitialSetup initialSetup;
        try {
            initialSetup = getInitialSetup(port);
        } catch (IOException ex) {
            err.println("Couldn't receive initial setup data.");
            return;
        } catch (ClassNotFoundException ex) {
            err.println("Received malformed initial data.");
            return;
        }
        Arrival_Transfer_Terminal_Proxy att
                = new Arrival_Transfer_Terminal_Proxy(
                        initialSetup.getAttAddress());
        Departure_Transfer_Terminal_Proxy dtt
                = new Departure_Transfer_Terminal_Proxy(
                        initialSetup.getDttAddress());
        Driver driver = new Driver(att, dtt);
        Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler() {
            @Override
            @SuppressWarnings("CallToPrintStackTrace")
            public void uncaughtException(Thread th, Throwable ex) {
                err.println("Uncaught Exception @Thread " + th.getId());
                ex.printStackTrace();
                System.exit(1);
            }
        };
        Thread driverThread = new Thread(driver);
        driverThread.setUncaughtExceptionHandler(h);
        driverThread.start();
        try {
            driverThread.join();
        } catch (InterruptedException ex) {
        }
        try {
            terminateServers(Client.DRIVER, initialSetup);
        } catch (ClassNotFoundException | IOException ex) {
            err.println(ex);
        }
    }
}
