package run;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import parser.XMLReader;
import protocol.InitialSetup;
import struct.Bag;
import struct.Passenger_Status;
import struct.Simulation;

/**
 * Processo que gera toda a informação da simulação e a envia para os monitores e entidades cliente.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class GIR_Process {

    /**
     * Endereço completo do monitor do terminal de desembarque, antes dos passageiros sairem do aeroporto.
     */
    private static InetSocketAddress ateAddress;

    /**
     * Endereço completo do monitor da zona de transferência para o terminal de desembarque.
     */
    private static InetSocketAddress attAddress;

    /**
     * Endereço completo do monitor de terminal de embarque.
     */
    private static InetSocketAddress dteAddress;

    /**
     * Endereço completo do monitor da zona de transferência para o terminal de embarque.
     */
    private static InetSocketAddress dttAddress;

    /**
     * Endereço completo do monitor da zona de desembarque, antes dos passageiros sairem do avião.
     */
    private static InetSocketAddress dzAddress;

    /**
     * Endereço completo do monitor da zona de recolha de bagagens.
     */
    private static InetSocketAddress lcpAddress;

    /**
     * Endereço completo do repositório geral de informação.
     */
    private static InetSocketAddress loggerAddress;

    /**
     * Endereço completo da entidade passageiro.
     */
    private static InetSocketAddress passengersAddress;

    /**
     * Endereço completo da entidade bagageiro.
     */
    private static InetSocketAddress porterAddress;

    /**
     * Endereço completo da entidade motorista.
     */
    private static InetSocketAddress driverAddress;

    /**
     * Nº de voos da simulação.
     */
    private static int K;

    /**
     * Número de passageiros por voo.
     */
    private static int N;

    /**
     * Nº de lugares do autocarro de transferência.
     */
    private static int T;

    /**
     * Nº máximo de malas do passageiro.
     */
    private static int M;

    /**
     * Gerador de números aleatórios
     */
    private static Random r;

    /**
     * Valores de estado interno possiveis para os passageiros.
     */
    private static Passenger_Status[] possibleStatus;

    /**
     * Lista de malas do avião.
     */
    private static ArrayList<Bag> planeBags;

    /**
     * Lista de estados internos dos passageiros.
     */
    private static ArrayList<Passenger_Status> passengers;

    /**
     * Lista do numero de malas de cada passageiro.
     */
    private static ArrayList<Integer> passengersBagNumber;

    /**
     * Número total de passageiros cujo destino não é final.
     */
    private static int passengersInTransit;

    /**
     * Lista de simulações com respectivas variáveis.
     */
    private static ArrayList<Simulation> simulations;

    public static void main(String... args) {
        System.out.println("General Information Repository Process");

        //verificar se foi fornecido o nome do ficheiro de configuração
        String filename;
        if (args.length != 0) {
            filename = args[0];
        } else {
            filename = "default.xml";
        }

        //read and parse the file
        XMLReader config = new XMLReader(filename);

        //inicializaçao das variaveis necessarias à execução
        try {
            ateAddress = config.getSocketAddress("ATE");
            attAddress = config.getSocketAddress("ATT");
            dteAddress = config.getSocketAddress("DTE");
            dttAddress = config.getSocketAddress("DTT");
            dzAddress = config.getSocketAddress("DZ");
            lcpAddress = config.getSocketAddress("LCP");
            loggerAddress = config.getSocketAddress("Logger");
            passengersAddress = config.getSocketAddress("Passengers");
            porterAddress = config.getSocketAddress("Porter");
            driverAddress = config.getSocketAddress("Driver");
        } catch (UnknownHostException ex) {
            System.err.println(ex);
            return;
        }
        K = config.getFlights();
        N = config.getPassengersPerFlight();
        T = config.getBusSeats();
        M = config.getBags();
        r = new Random();
        simulations = new ArrayList<>();

        for (int k = 0; k < K; k++) {

            possibleStatus = Passenger_Status.values(); //valores de estado interno possiveis para os passageiros
            planeBags = new ArrayList<>(); //lista de malas do avião
            passengers = new ArrayList<>(); //lista de estados internos dos passageiros
            passengersBagNumber = new ArrayList<>(); //lista do numero de malas de cada passageiro
            passengersInTransit = 0; //numero total de passageiros cujo destino não é final

            //criar lista de malas do avião e lista do número de malas para cada passageiro
            for (int i = 0; i < N; i++) {
                int bagsPerPassenger = 0; //numero de malas do passageiro
                double isGoingToMissBags = r.nextDouble(); //gerar valor de perda de malas
                int bagsToAdd; //numero de malas do passageiro a adicionar ao avião

                //gerar passageiros para o voo em simulação
                passengers.add(possibleStatus[r.nextInt(possibleStatus.length)]); //adicionar passageiro com estado aleatorio
                if (passengers.get(passengers.size() - 1) == Passenger_Status.IN_TRANSIT) { //se o destino do passageiro nao for o final
                    passengersInTransit++; //incrementar o numero total de passageiros cujo destino não é o final
                }

                if (passengers.get(i) == Passenger_Status.FINAL_DESTINATION_NO_BAGS) { //se o passageiro não tiver malas
                    passengersBagNumber.add(0);                                        //adiciona 0 a lista
                    continue; //passa ao próximo passageiro
                }
                //gerar o número de malas para o passageiro (sem ser 0 no caso do estado interno ser destino final com malas, porque o passageiro tem de ter pelo menos 1 mala neste estado interno)
                while (bagsPerPassenger == 0 && passengers.get(i) == Passenger_Status.FINAL_DESTINATION_WITH_BAGS) {
                    bagsPerPassenger = r.nextInt(M + 1);
                }
                passengersBagNumber.add(bagsPerPassenger); //adiciona o numero de bagagens a lista

                bagsToAdd = bagsPerPassenger;
                if (isGoingToMissBags > 0.8) { //se o valor de perda de malas e superior a 0.8, entao perde uma mala (probabilidade de ~20%)
                    bagsToAdd--;
                }
                //adicionar o numero de malas decidido para o aviao
                for (int j = 0; j < bagsToAdd; j++) {
                    planeBags.add(new Bag(i, passengers.get(i)));
                }
            }
            //colocar malas de modo aleatório
            Collections.shuffle(planeBags);

            //adicionar os dados calculados à lista de simulações
            simulations.add(new Simulation(planeBags, passengers, passengersBagNumber, passengersInTransit));

        }
        try {
            //enviar configurações geradas para todos os monitores
            sendConfigs();
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * Envia as configurações da simulação para todos os monitores e entidades.
     */
    private static void sendConfigs() throws IOException {
        InitialSetup setup
                = new InitialSetup(ateAddress, attAddress, dteAddress,
                        dttAddress, dzAddress, lcpAddress, loggerAddress,
                        simulations, K, N, T, M);
        InetSocketAddress[] addresses = {loggerAddress, ateAddress,
            attAddress, dteAddress, dttAddress, dzAddress, lcpAddress,
            passengersAddress, porterAddress, driverAddress};
        for (InetSocketAddress address : addresses) {
            Socket socket = new Socket();
            try {
                socket.connect(new InetSocketAddress(address.getAddress(), address.getPort()));
                try (ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream())) {
                    System.out.println("Connected to " + socket.getPort());
                    out.writeObject(setup);
                }
            } catch (IOException ex) {
                socket.close();
            }
        }
    }
}
