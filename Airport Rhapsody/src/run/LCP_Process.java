package run;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import mutex.Luggage_Collection_Point;

/**
 * Processo de simulação do monitor da zona de recolha de bagagens.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class LCP_Process {
    
    /**
     * Porta de escuta do monitor.
     */
    private static final int port = 22444;

    public static void main(String... args) {
        System.out.println("Luggage Collection Point Process");
        try {
            new ServerProcess<Luggage_Collection_Point>(Luggage_Collection_Point.class).runProcess(port);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException | IOException ex) {
            System.err.println(ex);
        }
    }
}
