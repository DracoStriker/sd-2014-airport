package run;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import logging.Logger;
import protocol.InitialSetup;
import protocol.InvalidReturnException;
import proxy.ClientProxy;
import struct.Client;

/**
 * Processo de simulação do monitor de repositório geral de informação (logger).
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class Logger_Process {

    /**
     * Porta de escuta do monitor.
     */
    private static final int port = 22449;

    /**
     * Indica se ainda há passageiros em execução.
     */
    private volatile boolean passengerKeepAlive;

    /**
     * Indica se o bagageiro ainda está em execução.
     */
    private volatile boolean porterKeepAlive;

    /**
     * Indica se o motorista ainda está em execução.
     */
    private volatile boolean driverKeepAlive;

    /**
     * Socket de escuta.
     */
    private ServerSocket serverSocket;

    /**
     * Inicializa as variáveis necessárias a este processo, nomeadamente os indicadores de actividade das entidades cliente.
     */
    private Logger_Process() {
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
    }

    public static void main(String... args) {
        System.out.println("Logger Process");
        String fileName = "airport_rhapsody_log.txt";
        if (args.length > 0) {
            fileName = args[0];
        }
        new Logger_Process().runProcess(fileName);
    }

    /**
     * Inicia todo o processo servidor, recebendo as configurações iniciais
     * primeiro e, de seguida, recebendo e processando mensagens num modo
     * cíclico.
     *
     * @param filename Nome do ficheiro de log a criar.
     */
    public void runProcess(String filename) {
        InitialSetup setup;
        Socket socket;
        Logger logger;
        /*Recebe as configurações iniciais*/
        try {
            serverSocket = new ServerSocket(port);
            socket = serverSocket.accept();
            try (ObjectInputStream socketObjectInputStream = new ObjectInputStream(socket.getInputStream())) {
                Object msg = socketObjectInputStream.readObject();
                if (!(msg instanceof InitialSetup)) {
                    throw new InvalidReturnException("Expected "
                            + InitialSetup.class.getName() + ", at method "
                            + "runProcess but received "
                            + msg.getClass().getName() + ".");
                }
                setup = (InitialSetup) msg;
            } catch (InvalidReturnException ex) {
                System.err.println(ex);
                try {
                    serverSocket.close();
                } catch (IOException ex1) {
                    System.err.println(ex1);
                }
                return;
            }
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex);
            try {
                serverSocket.close();
            } catch (IOException ex1) {
                System.err.println(ex1);
            }
            return;
        }
        /*Abre o ficheiro de log*/
        logger = new Logger(setup);
        try {
            logger.open(filename);
        } catch (IOException ex) {
            System.err.println("Cannot create file!\nExiting");
            return;
        }
        Thread.UncaughtExceptionHandler h = new UncaughtExceptionHandler() {
            @Override
            @SuppressWarnings("CallToPrintStackTrace")
            public void uncaughtException(Thread th, Throwable ex) {
                System.err.println("Uncaught exception @Thread " + th.getId());
                ex.printStackTrace();
                System.exit(1);
            }
        };
        /*Inicia o serviço de recepção e processamento de mensagens*/
        while (passengerKeepAlive || porterKeepAlive || driverKeepAlive) {
            try {
                socket = serverSocket.accept();
            } catch (SocketException ex) {
                if (passengerKeepAlive || porterKeepAlive || driverKeepAlive) {
                    System.err.println(ex + " @Logger_Process");
                }
                continue;
            } catch (IOException ex) {
                System.err.println(ex);
                return;
            }
            ClientProxy<Logger> clientProxy = new ClientProxy<>(socket, logger, this);
            clientProxy.setUncaughtExceptionHandler(h);
            clientProxy.start();
        }
        /*Fecha o ficheiro de log*/
        try {
            System.out.println("Terminating logger");
            logger.close();
            System.out.println("Logger Monitor Terminated.");
        } catch (IOException ex) {
            System.err.println("Cannot close file!\nExiting");
        }
    }

    /**
     * Actualiza o indicador de actividade da entidade cliente correspondente ao
     * cliente que terminou a actividade, terminando a execução do processo
     * deste monitor se não houver mais entidades cliente a correr.
     *
     * @param client Cliente que terminou a actividade.
     */
    public void close(Client client) {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }
}
