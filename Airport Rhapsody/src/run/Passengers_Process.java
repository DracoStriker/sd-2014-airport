package run;

import java.io.IOException;
import static java.lang.System.err;
import static java.lang.System.out;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import protocol.InitialSetup;
import proxy.Arrival_Terminal_Exit_Proxy;
import proxy.Arrival_Transfer_Terminal_Proxy;
import proxy.Departure_Terminal_Entrance_Proxy;
import proxy.Departure_Transfer_Terminal_Proxy;
import proxy.Disembarking_Zone_Proxy;
import proxy.Logger_Proxy;
import proxy.Luggage_Collection_Point_Proxy;
import struct.Bag;
import struct.Client;
import struct.Passenger_Status;
import struct.Simulation;
import thread.Passenger;
import util.ProcessConfiguration;
import static util.ProcessConfiguration.terminateServers;

/**
 * Simulação dos vários passageiros da Rapsódia no Aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 17-04-2014
 */
public class Passengers_Process {

    /**
     * Porto de escuta TCP deste processo.
     */
    private static final int port = 22440;

    public static void main(String... args) {
        out.println("Passengers Process");
        InitialSetup initialSetup;
        try {
            initialSetup = ProcessConfiguration.getInitialSetup(port);
        } catch (IOException ex) {
            out.println("Couldn't receive initial setup data.");
            return;
        } catch (ClassNotFoundException ex) {
            out.println("Received malformed initial data.");
            return;
        }
        int K = initialSetup.getFlights();
        int N = initialSetup.getPassengersPerFlight();
        Disembarking_Zone_Proxy dz
                = new Disembarking_Zone_Proxy(initialSetup.getDzAddress());
        Luggage_Collection_Point_Proxy lcp
                = new Luggage_Collection_Point_Proxy(
                        initialSetup.getLcpAddress());
        Arrival_Terminal_Exit_Proxy ate
                = new Arrival_Terminal_Exit_Proxy(initialSetup.getAteAddress());
        Arrival_Transfer_Terminal_Proxy att
                = new Arrival_Transfer_Terminal_Proxy(
                        initialSetup.getAttAddress());
        Departure_Transfer_Terminal_Proxy dtt
                = new Departure_Transfer_Terminal_Proxy(
                        initialSetup.getDttAddress());
        Departure_Terminal_Entrance_Proxy dte
                = new Departure_Terminal_Entrance_Proxy(
                        initialSetup.getDteAddress());
        Logger_Proxy logger = new Logger_Proxy(initialSetup.getLoggerAddress());
        ArrayList<Simulation> simulations = initialSetup.getSimulations();
        Thread.UncaughtExceptionHandler h = new UncaughtExceptionHandler() {
            @Override
            @SuppressWarnings("CallToPrintStackTrace")
            public void uncaughtException(Thread th, Throwable ex) {
                err.println("Uncaught Exception @Thread " + th.getId());
                ex.printStackTrace();
                System.exit(1);
            }
        };
        for (int k = 0; k < K; k++) {
            Simulation simulation = simulations.get(k);
            logger.startFlight();
            logger.setFlightNumber(k + 1);
            int nPassengersWaitingForADrive
                    = simulation.getPassengersInTransit();
            att.startFlight(k + 1, nPassengersWaitingForADrive);
            ArrayList<Integer> passengerBags
                    = simulation.getPassengersBagNumber();
            ArrayList<Passenger_Status> passengersStatus
                    = simulation.getPassengers();
            ArrayList<Bag> planeBags = simulation.getPlaneBags();
            for (int i = 0; i < N; i++) {
                if (passengersStatus.get(i)
                        == Passenger_Status.FINAL_DESTINATION_NO_BAGS) {
                    logger.setPassengerStatus(passengersStatus.get(i), i);
                    continue;
                }
                logger.setLuggageCarriedAtTheStartOfHerJourney(passengerBags.get(i), i);
                logger.setPassengerStatus(passengersStatus.get(i), i);
            }
            logger.setLuggageOnThePlane(planeBags.size());
            logger.save();
            Passenger[] passengers = new Passenger[N];
            for (int i = 0; i < N; i++) {
                passengers[i] = new Passenger(i, passengerBags.get(i), ate, att,
                        dte, dtt, dz, lcp);
            }
            dz.setLists(passengersStatus, planeBags);
            Thread[] passengerThreads = new Thread[N];
            for (int i = 0; i < N; i++) {
                passengerThreads[i] = new Thread(passengers[i]);
            }
            for (int i = 0; i < N; i++) {
                passengerThreads[i].setUncaughtExceptionHandler(h);
                passengerThreads[i].start();
            }
            for (int i = 0; i < N; i++) {
                try {
                    passengerThreads[i].join();
                } catch (InterruptedException ex) {
                }
            }
            lcp.setHasMoreBags(true);
            logger.emptyStoreroom();
        }
        try {
            terminateServers(Client.PASSENGER, initialSetup);
        } catch (ClassNotFoundException | IOException ex) {
            err.println(ex);
        }
    }
}
