package run;

import java.io.IOException;
import static java.lang.System.err;
import static java.lang.System.out;
import protocol.InitialSetup;
import proxy.Disembarking_Zone_Proxy;
import proxy.Luggage_Collection_Point_Proxy;
import struct.Client;
import thread.Porter;
import util.ProcessConfiguration;
import static util.ProcessConfiguration.terminateServers;

/**
 * Simulação do motorista da Rapsódia no Aeroporto.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @version 1.0
 * @since 17-04-2014
 */
public class Porter_Process {
    
    /**
     * Porto de escuta TCP deste processo.
     */
    private static final int port = 22441;

    public static void main(String... args) {
        out.println("Porter Process");
        InitialSetup initialSetup;
        try {
            initialSetup = ProcessConfiguration.getInitialSetup(port);
        } catch (IOException ex) {
            err.println("Couldn't receive initial setup data.");
            return;
        } catch (ClassNotFoundException ex) {
            err.println("Received malformed initial data.");
            return;
        }
        int K = initialSetup.getFlights();
        Disembarking_Zone_Proxy dz
                = new Disembarking_Zone_Proxy(initialSetup.getDzAddress());
        Luggage_Collection_Point_Proxy lcp
                = new Luggage_Collection_Point_Proxy(
                        initialSetup.getLcpAddress());
        Porter porter = new Porter(K, dz, lcp);
        Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler() {
            @Override
            @SuppressWarnings("CallToPrintStackTrace")
            public void uncaughtException(Thread th, Throwable ex) {
                err.println("Uncaught Exception @Thread " + th.getId());
                ex.printStackTrace();
                System.exit(1);
            }
        };
        Thread porterThread = new Thread(porter);
        porterThread.setUncaughtExceptionHandler(h);
        porterThread.start();
        try {
            porterThread.join();
        } catch (InterruptedException ex) {
        }
        try {
            terminateServers(Client.PORTER, initialSetup);
        } catch (ClassNotFoundException | IOException ex) {
            err.println(ex);
        }
    }
}
