package run;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import protocol.InitialSetup;
import protocol.InvalidReturnException;
import proxy.ClientProxy;
import struct.Client;

/**
 * Processo de simulação de um monitor genérico.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 * @param <T> Tipo de monitor
 */
public class ServerProcess<T> {

    /**
     * Tipo de classe do monitor que instanciou este processo.
     */
    private final Class<T> type;

    /**
     * Indica se ainda há passageiros em execução.
     */
    private volatile boolean passengerKeepAlive;

    /**
     * Indica se o bagageiro ainda está em execução.
     */
    private volatile boolean porterKeepAlive;

    /**
     * Indica se o motorista ainda está em execução.
     */
    private volatile boolean driverKeepAlive;

    /**
     * Socket de escuta.
     */
    private ServerSocket serverSocket;

    /**
     * Inicializa as variáveis necessárias a este processo, nomeadamente os
     * indicadores de actividade das entidades cliente e o tipo de classe que
     * instanciou este processo.
     *
     * @param type Tipo da classe que instanciou este processo.
     */
    public ServerProcess(Class<T> type) {
        passengerKeepAlive = true;
        porterKeepAlive = true;
        driverKeepAlive = true;
        this.type = type;
    }

    /**
     * Inicia todo o processo deste servidor, recebendo as configurações
     * iniciais primeiro, instanciando o monitor específico de seguida e
     * finalmente iniciando o serviço de recepção e processamento de mensagens.
     *
     * @param port Porta de escuta a colocar no servidor.
     * @throws java.lang.InstantiationException Se não for possível instanciar o monitor.
     * @throws java.lang.IllegalAccessException Se ocorreu um erro de acesso.
     * @throws java.lang.reflect.InvocationTargetException Se não conseguiu invocar com sucesso o método pretendido.
     * @throws java.lang.NoSuchMethodException Se o método pretendido não existe ou não foi encontrado.
     * @throws java.lang.ClassNotFoundException Se a classe pretendida não foi encontrada.
     * @throws java.io.IOException Se ocorreu um erro ao nível interno.
     */
    public void runProcess(int port) throws InstantiationException,
            IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, NoSuchMethodException,
            ClassNotFoundException, IOException {
        InitialSetup setup;
        Socket socket;
        /*Receber configurações iniciais*/
        try {
            serverSocket = new ServerSocket(port);
            socket = serverSocket.accept();
            try (ObjectInputStream socketObjectInputStream
                    = new ObjectInputStream(socket.getInputStream())) {
                Object msg = socketObjectInputStream.readObject();
                if (!(msg instanceof InitialSetup)) {
                    throw new InvalidReturnException("Expected "
                            + InitialSetup.class.getName() + ", at method "
                            + "runProcess but received "
                            + msg.getClass().getName() + ".");
                }
                setup = (InitialSetup) msg;
            } catch (InvalidReturnException ex) {
                System.err.println(ex);
                serverSocket.close();
                return;
            }
        } catch (IOException ex) {
            System.err.println(ex);
            serverSocket.close();
            return;
        }
        System.out.println("Configs received.");

        /*Invocar nova instancia do monitor*/
        Constructor<T> constructor; //referencia do construtor a utilizar
        constructor = type.getConstructor(InitialSetup.class); //obter o construtor da classe específica que recebe como argumento uma mensagem de configuração inicial
        T monitor; //referencia do monitor a instanciar
        monitor = constructor.newInstance(setup); //nova instancia do monitor atraves do construtor obtido
        System.out.println("Monitor instanciated.");
        System.out.println("Socket is ready.");
        Thread.UncaughtExceptionHandler h = new UncaughtExceptionHandler() {
            @Override
            @SuppressWarnings("CallToPrintStackTrace")
            public void uncaughtException(Thread th, Throwable ex) {
                System.err.println("Uncaught exception @Thread " + th.getId());
                ex.printStackTrace();
                System.exit(1);
            }
        };

        /*Inicia o serviço de recepção e processamento de mensagens*/
        while (passengerKeepAlive || porterKeepAlive || driverKeepAlive) {
            try {
                socket = serverSocket.accept();
            } catch (SocketException ex) {
                if (passengerKeepAlive || porterKeepAlive || driverKeepAlive) {
                    System.err.println(ex + " @ServerProcess: " + type.getSimpleName());
                }
                continue;
            }
            ClientProxy<T> clientProxy = new ClientProxy<>(socket, monitor, this);
            clientProxy.setUncaughtExceptionHandler(h);
            clientProxy.start();
        }
        System.out.println(type.getSimpleName() + " Monitor Terminated.");
    }

    /**
     * Actualiza o indicador de actividade da entidade cliente correspondente ao
     * cliente que terminou a actividade, terminando a execução do processo
     * deste monitor se não houver mais entidades cliente a correr.
     * 
     * @param client Cliente que terminou a actividade.
     */
    public void close(Client client) {
        switch (client) {
            case PASSENGER:
                passengerKeepAlive = false;
                break;
            case PORTER:
                porterKeepAlive = false;
                break;
            case DRIVER:
                driverKeepAlive = false;
                break;
        }
        if (!passengerKeepAlive && !porterKeepAlive && !driverKeepAlive) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }
}
