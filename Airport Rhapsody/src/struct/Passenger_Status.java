package struct;

import java.io.Serializable;

/**
 * Estados internos dos passageiros.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public enum Passenger_Status implements Serializable {

    FINAL_DESTINATION_WITH_BAGS("FDT"), FINAL_DESTINATION_NO_BAGS("FDT"),
    IN_TRANSIT("TRT");

    private static final long serialVersionUID = 1L;

    /**
     * String de representação deste tipo de dados enumerado.
     */
    private final String status;

    /**
     * Construtor deste tipo enuemrado fornecendo a sua representação em String.
     * 
     * @param status representação em String deste tipo de dados enumerado.
     */
    private Passenger_Status(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
