package struct;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Estrutura que representa os dados de uma simulação/voo.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 22-04-2014
 */
public class Simulation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Lista de malas do avião.
     */
    private final ArrayList<Bag> planeBags;

    /**
     * Lista de estados internos dos passageiros.
     */
    private final ArrayList<Passenger_Status> passengers;

    /**
     * Lista de número de malas de cada passageiro.
     */
    private final ArrayList<Integer> passengersBagNumber;

    /**
     * Número de passageiros cujo destino não é final.
     */
    private final int passengersInTransit;

    /**
     * Inicializa os dados desta estrutura.
     * @param planeBags Lista de malas do avião.
     * @param passengers Lista de estados internos dos passageiros.
     * @param passengersBagNumber Lista de número de malas de cada passageiro.
     * @param passengersInTransit Número de passageiros cujo destino não é final.
     */
    public Simulation(ArrayList<Bag> planeBags, ArrayList<Passenger_Status> passengers, ArrayList<Integer> passengersBagNumber, int passengersInTransit) {
        this.planeBags = planeBags;
        this.passengers = passengers;
        this.passengersBagNumber = passengersBagNumber;
        this.passengersInTransit = passengersInTransit;
    }

    /**
     * Obtém a lista de malas do avião.
     * @return
     */
    public ArrayList<Bag> getPlaneBags() {
        return planeBags;
    }

    /**
     * Obtém a lista de estados internos dos passageiros.
     * @return
     */
    public ArrayList<Passenger_Status> getPassengers() {
        return passengers;
    }

    /**
     * Obtém a lista de número de malas de cada passageiro.
     * @return
     */
    public ArrayList<Integer> getPassengersBagNumber() {
        return passengersBagNumber;
    }

    /**
     * Obtém o número de passageiros cujo destino não é final.
     * @return
     */
    public int getPassengersInTransit() {
        return passengersInTransit;
    }

    @Override
    public String toString() {
        return "Simulation\nPlane Bags = " + planeBags + "\nPassengers = "
                + passengers + "\nPassengers Bag Number = "
                + passengersBagNumber + "\nPassengers In Transit = "
                + passengersInTransit;
    }
}
