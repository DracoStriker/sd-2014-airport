package thread;

import java.util.Random;
import mutex.Driver_ATT;
import mutex.Driver_DTT;

/**
 * Thread que representa e a entidade Motorista.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Driver implements Runnable {

    /**
     * Monitor da zona de transferência para o terminal de desembarque do
     * aeroporto.
     */
    private final Driver_ATT attMon;
    /**
     * Monitor da zona de transferência para o terminal de embarque do
     * aeroporto.
     */
    private final Driver_DTT dttMon;

    /**
     * Inicializa a thread do motorista, recebendo a referência do monitor da
     * zona de transferência para o terminal de desembarque e a referência do
     * monitor da zona de transferência para o terminal de embarque.
     *
     * @param attMon Monitor da zona de transferência para o terminal de
     * desembarque do aeroporto.
     * @param dttMon Monitor da zona de transferência para o terminal de
     * embarque do aeroporto.
     */
    public Driver(Driver_ATT attMon, Driver_DTT dttMon) {
        this.attMon = attMon;
        this.dttMon = dttMon;
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        try {
            int nPassengersOnTheBus; //numero de passageiros presentes no autocarro
            Random r = new Random(); //gerador de numeros aleatorios
            //ciclo de vida do motorista
            Thread.sleep(r.nextInt(100));
            while (!attMon.hasDaysWorkEnded()) {
                Thread.sleep(r.nextInt(100));
                attMon.announcingBusBoarding();
                Thread.sleep(r.nextInt(100));
                nPassengersOnTheBus = attMon.goToDepartureTerminal();
                Thread.sleep(r.nextInt(100));
                dttMon.parkTheBusAndLetPassOff(nPassengersOnTheBus);
                Thread.sleep(r.nextInt(100));
                dttMon.goToArrivalTerminal();
                Thread.sleep(r.nextInt(100));
                attMon.parkTheBus();
                Thread.sleep(r.nextInt(100));
            }
            System.out.println("Driver work has ended."); //print apenas para debug
        } catch (InterruptedException ex) {
        }
    }
}
