package thread;

import java.util.Random;
import mutex.Passenger_ATE;
import mutex.Passenger_ATT;
import mutex.Passenger_DTE;
import mutex.Passenger_DTT;
import mutex.Passenger_DZ;
import mutex.Passenger_LCP;
import static struct.Passenger_Status.IN_TRANSIT;

/**
 * Thread que representa a entidade Passageiro.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Passenger implements Runnable {

    /**
     * Identificação do passageiro.
     */
    private final int id;

    /**
     * Número de malas que o passageiro possui, no início da simulação.
     */
    private final int nBags;

    /**
     * Número de malas que o passageiro possui actualmente.
     */
    private int nBagsReal = 0;

    /**
     * Monitor do terminal de desembarque do aeroporto.
     */
    private final Passenger_ATE ateMon;

    /**
     * Monitor da zona de transferência para o terminal de desembarque do
     * aeroporto.
     */
    private final Passenger_ATT attMon;

    /**
     * Terminal de embarque do aeroporto.
     */
    private final Passenger_DTE dteMon;

    /**
     * * Monitor da zona de transferência para o terminal de embarque do
     * aeroporto.
     */
    private final Passenger_DTT dttMon;

    /**
     * Monitor da zona de desembarque, onde os passageiros saem do avião.
     */
    private final Passenger_DZ dzMon;

    /**
     * Monitor da zona de recolha de bagagens.
     */
    private final Passenger_LCP lcpMon;

    /**
     * Inicializa a thread do passageiro, recebendo a identificação do mesmo, o
     * número de malas que ele possui no início da simulação e a referência dos
     * monitores em que ele participa.
     *
     * @param id Identificação do passageiro
     * @param nBags Número de malas que o passageiro possui no início da
     * simulação.
     * @param ateMon Monitor do terminal de desembarque do aeroporto.
     * @param attMon Monitor da zona de transferência para o terminal de
     * desembarque do aeroporto.
     * @param dteMon Terminal de embarque do aeroporto.
     * @param dttMon Monitor da zona de transferência para o terminal de
     * embarque do aeroporto.
     * @param dzMon Monitor da zona de desembarque, onde os passageiros saem do
     * avião.
     * @param lcpMon Monitor da zona de recolha de bagagens.
     */
    public Passenger(int id, int nBags, Passenger_ATE ateMon, Passenger_ATT attMon, Passenger_DTE dteMon, Passenger_DTT dttMon, Passenger_DZ dzMon, Passenger_LCP lcpMon) {
        this.id = id;
        this.nBags = nBags;
        this.ateMon = ateMon;
        this.attMon = attMon;
        this.dteMon = dteMon;
        this.dttMon = dttMon;
        this.dzMon = dzMon;
        this.lcpMon = lcpMon;
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        try {
            Random r = new Random(); //gerador de números aleatórios
            //ciclo de vida do passageiro
            Thread.sleep(r.nextInt(100));
            switch (dzMon.whatShouldIDo(id)) { //dependendo do estado interno, o passageiro executará varios métodos
                case IN_TRANSIT: //destino do passageiro não é o final
                    Thread.sleep(r.nextInt(100));
                    attMon.takeABus(id);
                    Thread.sleep(r.nextInt(100));
                    attMon.enterTheBus(id);
                    Thread.sleep(r.nextInt(100));
                    dttMon.leaveTheBus(id);
                    Thread.sleep(r.nextInt(100));
                    dteMon.prepareNextLeg(id);
                    break;
                case FINAL_DESTINATION_WITH_BAGS: //destino do passageiro é o final e tem malas
                    Thread.sleep(r.nextInt(100));
                    while (lcpMon.goCollectABag(id) != null) {
                        nBagsReal++;
                        Thread.sleep(r.nextInt(100));
                    }
                    if (nBags != nBagsReal) {
                        Thread.sleep(r.nextInt(100));
                        ateMon.reportMissingBags(id);
                    }
                    Thread.sleep(r.nextInt(100));
                    ateMon.goHome(id);
                    break;
                case FINAL_DESTINATION_NO_BAGS: //destino do passageiro é o final e não tem malas
                    Thread.sleep(r.nextInt(100));
                    ateMon.goHome(id);
                    break;
            }
        } catch (InterruptedException ex) {
        }
    }
}
