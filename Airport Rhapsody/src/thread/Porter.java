package thread;

import java.util.Random;
import mutex.Porter_DZ;
import mutex.Porter_LCP;
import struct.Bag;

/**
 * Thread que representa a entidade Bagageiro.
 *
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 11-03-2014
 */
public class Porter implements Runnable {

    /**
     * Monitor da zona de desembarque, onde os passageiros saem do avião.
     */
    private final Porter_DZ dzMon;
    
    /**
     * Monitor da zona de recolha de bagagens.
     */
    private final Porter_LCP lcpMon;
    
    /**
     * Número total de voos.
     */
    private final int K;

    /**
     * Inicializa a thread do motorista, recebendo o número total de voos, a
     * referência do monitor da zona de desembarque e a referência do monitor da
     * zona de recolha de bagagens.
     *
     * @param K Número total de voos.
     * @param dzMon Monitor da zona de desembarque, onde os passageiros saem do
     * avião.
     * @param lcpMon Monitor da zona de recolha de bagagens.
     */
    public Porter(int K, Porter_DZ dzMon, Porter_LCP lcpMon) {
        this.K = K;
        this.dzMon = dzMon;
        this.lcpMon = lcpMon;
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        Bag bag; //mala que recolhe do aviao para o local apropriado
        Random r = new Random(); //gerador de números aleatórios
        //ciclo de vida do bagageiro
        for (int k = 0; k < K; k++) {
            try {
                Thread.sleep(r.nextInt(100));
                dzMon.takeARest();
                Thread.sleep(r.nextInt(100));
                while ((bag = dzMon.tryToCollectABag()) != null) {
                    Thread.sleep(r.nextInt(100));
                    lcpMon.carryItToAppropriateStore(bag);
                }
                Thread.sleep(r.nextInt(100));
                lcpMon.noMoreBagsToCollect();
            } catch (InterruptedException ex) {
            }
        }
        System.out.println("Porter work has ended."); //print apenas para debug
    }
}
