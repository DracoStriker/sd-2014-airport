/**
 * Contém os fios de execução das entidades Passageiro, Bagageiro e Motorista
 * do Airport Rhapsody.
 */
package thread;
