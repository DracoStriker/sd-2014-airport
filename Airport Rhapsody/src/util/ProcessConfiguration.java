package util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import protocol.InitialSetup;
import protocol.Terminate;
import struct.Client;

/**
 * Funções auxiliares para a execução da Rapsódia no Aeroporto num ambiente
 * distribuído.
 *
 * @author Simão Reis <simao.paulo@ua.pt>
 * @author Pedro Piçarra <pedropicarra@ua.pt>
 * @version 1.0
 * @since 17-04-2014
 */
public final class ProcessConfiguration {

    /**
     * Método auxiliar para receber as configurações iniciais para uma simulação
     * distribuída.
     *
     * @param port porto TCP de escuta das configurações iniciais.
     * @return configurações iniciais para uma simulação.
     * @throws IOException quando ocorrer algum erro na ligação TCP.
     * @throws ClassNotFoundException quando recebe um objecto com uma definição
     * desconhecida.
     */
    public static InitialSetup getInitialSetup(int port) throws IOException,
            ClassNotFoundException {
        ServerSocket server;
        Socket socket;
        InitialSetup initialSetup;
        server = new ServerSocket(port);
        socket = server.accept();
        try (ObjectInputStream socketObjectInputStream
                = new ObjectInputStream(socket.getInputStream())) {
            initialSetup = (InitialSetup) socketObjectInputStream.readObject();
        }
        server.close();
        return initialSetup;
    }

    /**
     * Método auxiliar para enviar uma mensagem para todos os servidores a pedir
     * o seu fim de execução, por parte de um cliente.
     *
     * @param client Cliente que envia a mensagem.
     * @param setup Mensagem de configuração inicial.
     * @throws java.lang.ClassNotFoundException A classe que foi lida não foi encontrada.
     * @throws java.io.IOException Ocorreu um erro ao nível interno.
     */
    public static void terminateServers(Client client, InitialSetup setup)
            throws ClassNotFoundException, IOException {
        Terminate msg = new Terminate(client);
        Socket socket;
        InetSocketAddress[] addresses = {setup.getAteAddress(),
            setup.getAttAddress(), setup.getDteAddress(), setup.getDttAddress(),
            setup.getDzAddress(), setup.getLcpAddress(),
            setup.getLoggerAddress()};
        for (InetSocketAddress address : addresses) {
            socket = new Socket();
            try {
                socket.connect(address);
                try (ObjectOutputStream out
                        = new ObjectOutputStream(socket.getOutputStream());
                        ObjectInputStream in
                        = new ObjectInputStream(socket.getInputStream())) {
                    out.writeObject(msg);
                    in.readObject();
                }
            } catch (IOException ex) {
                socket.close();
            }
        }
    }
}
