package simplermiappregistryserver;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import simplermilib.Register;

/**
 *
 * @author Simon
 */
public class EntryRegister implements Register {
    
    private static final long serialVersionUID = 1L;

    private String hostName;
    private int portNumb = 1099;

    public EntryRegister(String hostName, int portNumb) throws RemoteException {
        if ((hostName == null) || ("".equals(hostName))) {
            throw new NullPointerException();
        }
        this.hostName = hostName;
        if ((portNumb >= 4000) && (portNumb <= 65535)) {
            this.portNumb = portNumb;
        }
    }

    @Override
    public void bind(String name, Remote obj) throws RemoteException, AlreadyBoundException {
        Registry registry;
        if ((name == null) || (obj == null)) {
            throw new NullPointerException();
        }
        registry = LocateRegistry.getRegistry(hostName, portNumb);
        registry.bind(name, obj);
        System.out.println("binding object " +obj.getClass().getCanonicalName() + " as " + name);
    }

    @Override
    public void unbind(String name) throws RemoteException, NotBoundException {
        Registry registry;
        if ((name == null)) {
            throw new NullPointerException();
        }
        registry = LocateRegistry.getRegistry(hostName, portNumb);
        registry.unbind(name);
        System.out.println("unbinding " + name);
    }

    @Override
    public void rebind(String name, Remote obj) throws RemoteException {
        Registry registry;
        if ((name == null) || (obj == null)) {
            throw new NullPointerException();
        }
        registry = LocateRegistry.getRegistry(hostName, portNumb);
        registry.rebind(name, obj);
        System.out.println("rebinding object " +obj.getClass().getCanonicalName() + " as " + name);
    }

}
