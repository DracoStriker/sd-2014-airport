package simplermiappregistryserver;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import simplermilib.Register;

/**
 *
 * @author Simon
 */
public class RegistryServerMain {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    private static String registryServerHostName;
    private static int registryServerPort;
    private static int port;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        RegistryServerMain registryServer = new RegistryServerMain();
        registryServer.init(args);
        registryServer.startServer();
    }

    private void startServer() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.createRegistry(registryServerPort);
            EntryRegister entry = new EntryRegister(registryServerHostName, registryServerPort);
            Register register = (Register) UnicastRemoteObject.exportObject(entry, port);
            registry.rebind("Register", register);
        } catch (RemoteException e) {
            System.err.println(e);
        }
        System.out.println("registry server is running...");
    }

    private void init(String[] args) {
        if (args.length < 3) {
            System.out.println("Usage:\njava -jar RegistryServerMain <registry host name> <registry port> <registry server port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
            port = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("Argument 2 and 3 must be a number!");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nServer is closing...");
            }
        });
    }
}
