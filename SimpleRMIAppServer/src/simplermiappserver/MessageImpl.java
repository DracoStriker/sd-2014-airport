package simplermiappserver;

import java.rmi.RemoteException;
import simplermilib.Message;

/**
 *
 * @author Simon
 */
public class MessageImpl implements Message {

    private static final long serialVersionUID = 1L;

    public MessageImpl() throws RemoteException {
    }

    @Override
    public void sayHello(String name) throws RemoteException {
        System.out.println("hello " + name);
    }
}
