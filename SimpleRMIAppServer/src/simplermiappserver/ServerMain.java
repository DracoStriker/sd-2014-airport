package simplermiappserver;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import simplermilib.Message;
import simplermilib.Register;

/**
 *
 * @author Simon
 */
public class ServerMain {

    static {
        System.setProperty("java.security.policy", "java.policy");
    }

    private static String registryServerHostName;
    private static int registryServerPort;
    private static int port;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ServerMain server = new ServerMain();
        server.init(args);
        server.startServer();
    }

    private void startServer() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
        try {
            Registry registry = LocateRegistry.getRegistry(registryServerHostName, registryServerPort);
            Register register = (Register) registry.lookup("Register");
            MessageImpl messageImpl = new MessageImpl();
            Message message = (Message) UnicastRemoteObject.exportObject(messageImpl, port);
            register.bind("Message", message);
        } catch (RemoteException | NotBoundException | AlreadyBoundException e) {
            System.err.println(e);
        }
        System.out.println("server is running...");
    }

    private void init(String[] args) {
        if (args.length < 3) {
            System.out.println("Usage:\njava -jar RegistryServerMain <registry host name> <registry port> <registry server port>");
            System.exit(0);
        }
        registryServerHostName = args[0];
        try {
            registryServerPort = Integer.parseInt(args[1]);
            port = Integer.parseInt(args[2]);
        } catch (NumberFormatException ex) {
            System.out.println("Argument 2 and 3 must be a number!");
            System.exit(0);
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("\nServer is closing...");
            }
        });
    }
}
