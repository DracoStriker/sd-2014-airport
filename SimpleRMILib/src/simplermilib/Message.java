package simplermilib;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Simon
 */
public interface Message extends Remote {

    void sayHello(String name) throws RemoteException;
}
