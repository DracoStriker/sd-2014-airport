package simplermilib;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Simon
 */
public interface Register extends Remote {

    public void bind(String name, Remote obj) throws RemoteException, AlreadyBoundException;

    public void unbind(String name) throws RemoteException, NotBoundException;

    public void rebind(String name, Remote obj) throws RemoteException;
}
